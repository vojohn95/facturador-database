<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PensionPagosAplicados implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    use Exportable;

    public function __construct(string $mes, string $año)
    {
        $this->mes = $mes;
        $this->año = $año;
    }

    public function array(): array
    {
        $reporte = "select est.nombre_proyecto as proyecto, grt.name as gerente, pd.id as \"id pensión\", pd.noTarjeta as tarjeta,
        pg.fechaPago as \"Fecha Pago\", tipoPago as \"Forma de Pago\", pg.total as \"Total de Pago\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as cliente
        from pensionesDet pd inner join pagos pg
        on pg.idPension = pd.id inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id = cl.id_pension
        where pg.mes = 'Mayo'
        and pg.año = '2020';";
        $result1 = DB::connection('mysql2')->SELECT($reporte);

        return $result1;
    }

    public function headings(): array
    {
        return [
            'Proyecto',
            'Gerente',
            'Pensión',
            'Tarjeta',
            'Fecha de pago',
            'Forma de pago',
            'Total de pago',
            'Cliente',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
