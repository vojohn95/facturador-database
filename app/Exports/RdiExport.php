<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Models\RDI;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithProperties;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Jenssegers\Date\Date;

class RdiExport implements FromView, WithStyles, WithDrawings, ShouldAutoSize, WithCustomStartCell, WithProperties
{
    use Exportable;

    public function __construct(string $est, string $fecha)
    {
        $this->est = $est;
        $this->fecha = $fecha;
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Central logo');
        $drawing->setPath(public_path('logo/logo/login-logo.png'));
        $drawing->setHeight(90);
        $drawing->setWidth(90);
        $drawing->setCoordinates('D4');

        return $drawing;
    }

    public function view(): View
    {
        $reporte = "SELECT  ingd.fecha_contable as fecha_contable, est.nombre_proyecto as proyecto, ccon.DESCRIPCION as concepto, ingd.monto as monto_iva, truncate(ingd.monto / 1.16, 2) as monto_sin_iva,
        ingd.cantidad as numero_autos #este campo es donde lleva la cantidad de coches ingresados al estacionamiento, va ligado a la clave 79 autos
        FROM ingreso_diario ingd
        inner join estacionamientos est on est.no_estacionamiento = ingd.id_estacionamiento
        inner join cat_concepto ccon on ccon.id =ingd.id_concepto
        where ingd.id_estacionamiento = $this->est #numero proyecto
        and date_format(ingd.fecha_contable, '%Y-%m-%d') = '$this->fecha'
        #and ingd.id_concepto = 1
        group by concepto, fecha_contable, proyecto, monto, numero_autos
        order by fecha_contable, concepto  asc";
        $result1 = DB::connection('mysql2')->SELECT($reporte);
        $date = new Date($result1[0]->fecha_contable);
        $fecha = \Carbon\Carbon::parse($date)->locale('es')->format('l jS \\de F Y');

        return view('rdi.rdi', [
            'invoices' => $result1,
            'fecha_contable' => $fecha,
            'nombre_proyecto' => $result1[0]->proyecto,
        ]);
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 55,
            'B' => 45,
        ];
    }

    public function startCell(): string
    {
        return 'B2';
    }

    public function properties(): array
    {
        return [
            'creator'        => 'John',
            'lastModifiedBy' => 'OCE',
            'title'          => 'RDI',
            'description'    => 'RDI',
            'subject'        => 'RDI',
            'keywords'       => 'rdi',
            'category'       => 'rdi',
            'manager'        => 'OCE',
            'company'        => 'OCE',
        ];
    }
}
