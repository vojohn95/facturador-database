<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PensionInActivasExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    use Exportable;

    //public function __construct(string $mes, string $año, int $id_pen)
    public function __construct()
    {
        /*$this->mes = $mes;
        $this->año = $año;
        $this->id_pen = $id_pen;*/
    }

    public function array(): array
    {
        #Reporte de pensiones inactivas por ESTACIONAMIENTO
        $reporte = "
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correogerente\",
        est.no_estacionamiento as \"Número de Proyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"Número de Pensión\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombre de pensionado\",
        'Inactiva' as \"Estado\",
        pd.fechaInactivacion as \"fecha baja o cancelación\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Contrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\")  as \"Solicitud contrato\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\")  as \"Comprobante Domicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\")  as \"Ine\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Licencia\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\")  as \"Rfc\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"TarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id = cl.id_pension inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = 8 #id de estacionamiento a consultar
        and pd.status = 0 #cambiar a 0 cuando inactivas
        and pd.fechaInactivacion is not null
        and date_format(pd.created_at, '%Y-%m-%d') between '2021-01-05' and '2021-06-01'
        UNION
        select dis.clave_distrito as distrito,
        dis.nombre as distrital,
        grt.name as gerente,
        grt.email as \"correo de gerente\",
        est.no_estacionamiento as \"Número de Proyecto\",
        est.nombre_proyecto as \"Proyecto\",
        pd.id as \"Número de Pensión\",
        concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"nombre de pensionado\",
        'Cancelada' as \"Estado\",
        pd.fechaCancelacion as \"fecha baja o cancelación\",
        if(pd.contrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Contrato\",
        if(pd.solicitudContrato is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"SolicitudContrato\",
        if(pd.comprobanteDomicilio is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"ComprobanteDomicilio\",
        if(pd.ine is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Ine\",
        if(pd.licencia is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Licenciado\",
        if(pd.rfc is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"Rfc\",
        if(pd.tarjetaCirculacion is null, \"No Tiene Documento Asociado\", \"Tiene Documento Asociado\") as \"TarjetaCirculacion\"
        from pensionesDet pd inner join estacionamientos est on est.id = pd.id_estacionamiento
        inner join gerentes grt on  est.id_gerentes = grt.id inner join Clientes cl on
        pd.id = cl.id_pension inner join distritales dis on dis.id = grt.id_distritales
        where  est.id = 8 #id de estacionamiento a consultar
        and pd.status = 0 #cambiar a 0 cuando inactivas
        and fechaCancelacion is not null
        and date_format(pd.created_at, '%Y-%m-%d') between '2021-01-01' and '2021-06-01';";
        $result1 = DB::connection('mysql2')->SELECT($reporte);
        //dd($result1);

        return $result1;
    }

    public function headings(): array {
        return [
            'Distrito',
            'Distrital',
            'Gerente',
            'Correo gerente',
            'Numero de proyecto',
            'Proyecto',
            'Numero de pensión',
            'Nombre de pensión',
            'Estado',
            'Fecha baja o cancelación',
            'Contrato',
            'Solicitud contrato',
            'Comprobante domicilio',
            'Ine',
            'Licencia',
            'RFC',
            'Tarjeta Circulacion',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }


}
