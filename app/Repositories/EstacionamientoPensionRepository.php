<?php

namespace App\Repositories;

use App\Models\EstacionamientoPension;
use App\Repositories\BaseRepository;

/**
 * Class EstacionamientoPensionRepository
 * @package App\Repositories
 * @version February 24, 2021, 3:14 am CST
*/

class EstacionamientoPensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no_estacionamiento',
        'nombre_proyecto',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'pais',
        'cp',
        'tipo_estacionamiento',
        'id_gerentes',
        'estatus'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EstacionamientoPension::class;
    }
}
