<?php

namespace App\Repositories;

use App\Models\AsignacionTarjeta;
use App\Repositories\BaseRepository;

/**
 * Class AsignacionTarjetaRepository
 * @package App\Repositories
 * @version February 24, 2021, 8:12 pm CST
*/

class AsignacionTarjetaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'folio_inicial',
        'folio_final',
        'id_proyecto',
        'fecha_asignacion',
        'fecha_finalizacion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AsignacionTarjeta::class;
    }
}
