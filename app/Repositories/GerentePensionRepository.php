<?php

namespace App\Repositories;

use App\Models\GerentePension;
use App\Repositories\BaseRepository;

/**
 * Class GerentePensionRepository
 * @package App\Repositories
 * @version February 24, 2021, 3:15 am CST
*/

class GerentePensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GerentePension::class;
    }
}
