<?php

namespace App\Repositories;

use App\Models\Estacionamiento;
use App\Repositories\BaseRepository;

/**
 * Class EstacionamientoRepository
 * @package App\Repositories
 * @version September 23, 2019, 5:43 pm UTC
*/

class EstacionamientoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_marca',
        'id_org',
        'nombre',
        'dist_regio',
        'direccion',
        'calle',
        'no_ext',
        'colonia',
        'municipio',
        'estado',
        'cp',
        'latitud',
        'longitud',
        'Facturable',
        'Automatico',
        'cajones',
        'folio',
        'serie',
        'correo',
        'pensiones',
        'cortesias',
        'cobro',
        'observaciones',
        'escuela'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estacionamiento::class;
    }
}
