<?php

namespace App\Repositories;

use App\Models\EstatusTicket;
use App\Repositories\BaseRepository;

/**
 * Class EstatusTicketRepository
 * @package App\Repositories
 * @version March 29, 2022, 1:17 am CST
*/

class EstatusTicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_estatus'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EstatusTicket::class;
    }
}
