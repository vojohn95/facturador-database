<?php

namespace App\Repositories;

use App\Models\CatConcepto;
use App\Repositories\BaseRepository;

/**
 * Class CatConceptoRepository
 * @package App\Repositories
 * @version May 3, 2021, 6:21 pm CDT
*/

class CatConceptoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'DESCRIPCION',
        'TIPO',
        'ID_MAT_ARTICULO',
        'CLIENTE_ERP',
        'FECHA_ALTA',
        'FECHA_BAJA',
        'ESTATUS'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CatConcepto::class;
    }
}
