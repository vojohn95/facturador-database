<?php

namespace App\Repositories;

use App\Models\TicketsTicket;
use App\Repositories\BaseRepository;

/**
 * Class TicketsTicketRepository
 * @package App\Repositories
 * @version March 29, 2022, 1:18 am CST
*/

class TicketsTicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_ticket',
        'descripcion',
        'id_proyecto',
        'id_tipo',
        'id_asignacion',
        'id_categoria',
        'id_nivel',
        'id_estatus',
        'id_usuario'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketsTicket::class;
    }
}
