<?php

namespace App\Repositories;

use App\Models\Org;
use App\Repositories\BaseRepository;

/**
 * Class OrgRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:13 pm UTC
*/

class OrgRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'RFC',
        'Razon_social',
        'Regimen_fiscal',
        'URL_dev',
        'URL_prod',
        'token_dev',
        'token_prod',
        'Prod'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Org::class;
    }
}
