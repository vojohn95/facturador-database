<?php

namespace App\Repositories;

use App\Models\OperacionDiaria;
use App\Repositories\BaseRepository;

/**
 * Class OperacionDiariaRepository
 * @package App\Repositories
 * @version February 24, 2021, 2:42 am CST
*/

class OperacionDiariaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tarjeta_fisica',
        'transaccion',
        'tipo_transaccion',
        'monto',
        'estancia'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OperacionDiaria::class;
    }
}
