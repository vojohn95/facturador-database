<?php

namespace App\Repositories;

use App\Models\catMetodoPago;
use App\Repositories\BaseRepository;

/**
 * Class catMetodoPagoRepository
 * @package App\Repositories
 * @version February 24, 2021, 3:01 am CST
*/

class catMetodoPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'claveSat',
        'descricion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return catMetodoPago::class;
    }
}
