<?php

namespace App\Repositories;

use App\Models\catBancos;
use App\Repositories\BaseRepository;

/**
 * Class catBancosRepository
 * @package App\Repositories
 * @version February 24, 2021, 3:04 am CST
*/

class catBancosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'banco'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return catBancos::class;
    }
}
