<?php

namespace App\Repositories;

use App\Models\ImgIngreso;
use App\Repositories\BaseRepository;

/**
 * Class ImgIngresoRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:27 pm UTC
*/

class ImgIngresoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ingreso',
        'foto',
        'firma'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ImgIngreso::class;
    }
}
