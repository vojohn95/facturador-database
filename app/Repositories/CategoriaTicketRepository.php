<?php

namespace App\Repositories;

use App\Models\CategoriaTicket;
use App\Repositories\BaseRepository;

/**
 * Class CategoriaTicketRepository
 * @package App\Repositories
 * @version March 29, 2022, 1:16 am CST
*/

class CategoriaTicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_categoria'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoriaTicket::class;
    }
}
