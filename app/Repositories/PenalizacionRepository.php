<?php

namespace App\Repositories;

use App\Models\Penalizacion;
use App\Repositories\BaseRepository;

/**
 * Class PenalizacionRepository
 * @package App\Repositories
 * @version February 24, 2021, 3:11 am CST
*/

class PenalizacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'monto',
        'concepto',
        'id_estacionamiento'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Penalizacion::class;
    }
}
