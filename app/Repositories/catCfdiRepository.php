<?php

namespace App\Repositories;

use App\Models\catCfdi;
use App\Repositories\BaseRepository;

/**
 * Class catCfdiRepository
 * @package App\Repositories
 * @version February 24, 2021, 2:59 am CST
*/

class catCfdiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'claveSat',
        'descricion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return catCfdi::class;
    }
}
