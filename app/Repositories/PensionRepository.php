<?php

namespace App\Repositories;

use App\Models\Pension;
use App\Repositories\BaseRepository;

/**
 * Class PensionRepository
 * @package App\Repositories
 * @version February 24, 2021, 3:09 am CST
*/

class PensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'montoPension',
        'tipoPension',
        'contrato',
        'solicitudContrato',
        'comprobanteDomicilio',
        'ine',
        'licencia',
        'rfc',
        'tarjetaCirculacion',
        'noTarjeta',
        'status',
        'fecha_limite'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pension::class;
    }
}
