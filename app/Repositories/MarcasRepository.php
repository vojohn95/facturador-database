<?php

namespace App\Repositories;

use App\Models\Marcas;
use App\Repositories\BaseRepository;

/**
 * Class MarcasRepository
 * @package App\Repositories
 * @version September 19, 2019, 5:13 pm UTC
*/

class MarcasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'marca'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Marcas::class;
    }
}
