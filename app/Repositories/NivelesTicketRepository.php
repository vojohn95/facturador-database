<?php

namespace App\Repositories;

use App\Models\NivelesTicket;
use App\Repositories\BaseRepository;

/**
 * Class NivelesTicketRepository
 * @package App\Repositories
 * @version March 29, 2022, 1:17 am CST
*/

class NivelesTicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_nivel',
        'tiempo_respuesta'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NivelesTicket::class;
    }
}
