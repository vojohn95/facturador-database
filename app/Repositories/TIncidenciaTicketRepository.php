<?php

namespace App\Repositories;

use App\Models\TIncidenciaTicket;
use App\Repositories\BaseRepository;

/**
 * Class TIncidenciaTicketRepository
 * @package App\Repositories
 * @version March 29, 2022, 1:18 am CST
*/

class TIncidenciaTicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_incidencia'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TIncidenciaTicket::class;
    }
}
