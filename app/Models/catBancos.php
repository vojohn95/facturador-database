<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class catBancos
 * @package App\Models
 * @version February 24, 2021, 3:04 am CST
 *
 * @property integer $id
 * @property string $banco
 */
class catBancos extends Model
{
    public $table = 'catBancos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $timestamps = false;

    public $connection = "mysql2";

    public $fillable = [
        'id',
        'banco'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'banco' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'banco' => 'nullable|string|max:45'
    ];


}
