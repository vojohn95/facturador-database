<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Org
 * @package App\Models
 * @version September 19, 2019, 5:13 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection marcas
 * @property string RFC
 * @property string Razon_social
 * @property string Regimen_fiscal
 * @property string URL_dev
 * @property string URL_prod
 * @property string token_dev
 * @property string token_prod
 * @property boolean Prod
 */
class Org extends Model
{
    use SoftDeletes;

    public $table = 'orgs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'RFC',
        'Razon_social',
        'Regimen_fiscal',
        'URL_dev',
        'URL_prod',
        'token_dev',
        'token_prod',
        'Prod'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'RFC' => 'string',
        'Razon_social' => 'string',
        'Regimen_fiscal' => 'string',
        'URL_dev' => 'string',
        'URL_prod' => 'string',
        'token_dev' => 'string',
        'token_prod' => 'string',
        'Prod' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'RFC' => 'required',
        'Razon_social' => 'required',
        'Regimen_fiscal' => 'required',
        'URL_dev' => 'required',
        'URL_prod' => 'required',
        'token_dev' => 'required',
        'token_prod' => 'required',
        'Prod' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function marcas()
    {
        return $this->belongsToMany(\App\Models\Marca::class, 'parks');
    }
}
