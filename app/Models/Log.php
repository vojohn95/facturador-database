<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Log
 * @package App\Models
 * @version September 19, 2019, 5:10 pm UTC
 *
 * @property \App\Models\CentralUser idCentralUser
 * @property integer id_central_user
 * @property string mensaje_sis
 * @property string url
 * @property string metodo
 * @property string tipo
 */
class Log extends Model
{
    use SoftDeletes;

    public $table = 'logs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_central_user',
        'mensaje_sis',
        'url',
        'metodo',
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_central_user' => 'integer',
        'mensaje_sis' => 'string',
        'url' => 'string',
        'metodo' => 'string',
        'tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_central_user' => 'required',
        'mensaje_sis' => 'required',
        'url' => 'required',
        'metodo' => 'required',
        'tipo' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCentralUser()
    {
        return $this->belongsTo(\App\Models\CentralUser::class, 'id_central_user');
    }
}
