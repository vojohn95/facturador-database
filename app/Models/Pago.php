<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pago
 * @package App\Models
 * @version February 24, 2021, 2:43 am CST
 *
 * @property \App\Models\PensionesDet $idpension
 * @property number $total
 * @property number $subtotal
 * @property number $iva
 * @property string $tipoPago
 * @property integer $idPension
 * @property string $fechaPago
 * @property integer $id_bancos
 * @property string $mes
 * @property integer $año
 * @property string $comprobante_pago
 */
class Pago extends Model
{
    public $table = 'pagos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'total',
        'subtotal',
        'iva',
        'tipoPago',
        'idPension',
        'fechaPago',
        'id_bancos',
        'mes',
        'año',
        'comprobante_pago',
        'estatus_factura',
        'formaPago',
        'id_metodoPago',
        'id_formaPago',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total' => 'float',
        'subtotal' => 'float',
        'iva' => 'float',
        'tipoPago' => 'string',
        'idPension' => 'integer',
        'fechaPago' => 'date',
        'id_bancos' => 'integer',
        'id_metodoPago' => 'integer',
        'id_formaPago' => 'integer',
        'mes' => 'string',
        'año' => 'integer',
        //'comprobante_pago' => 'string',
        'estatus_factura' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'total' => 'nullable|numeric',
        'subtotal' => 'nullable|numeric',
        'iva' => 'nullable|numeric',
        'tipoPago' => 'nullable|string|max:25',
        'idPension' => 'nullable|integer',
        'fechaPago' => 'nullable',
        'id_bancos' => 'nullable|integer',
        'id_metodoPago' => 'nullable|integer',
        'id_formaPago' => 'nullable|integer',
        'mes' => 'nullable|string|max:20',
        'año' => 'nullable|integer',
        'comprobante_pago' => 'nullable',
        'estatus_factura' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idpension()
    {
        return $this->belongsTo(\App\Models\PensionesDet::class, 'idPension');
    }
}
