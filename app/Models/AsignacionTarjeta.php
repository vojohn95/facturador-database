<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AsignacionTarjeta
 * @package App\Models
 * @version February 24, 2021, 8:12 pm CST
 *
 * @property \App\Models\Estacionamiento $idProyecto
 * @property integer $folio_inicial
 * @property integer $folio_final
 * @property integer $id_proyecto
 * @property string $fecha_asignacion
 * @property string $fecha_finalizacion
 */
class AsignacionTarjeta extends Model
{

    public $table = 'asignacionTarjetas';

    /*const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';*/


    public $connection = "mysql2";

    public $fillable = [
        'folio_inicial',
        'folio_final',
        'id_proyecto',
        'fecha_asignacion',
        'fecha_finalizacion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_asignacion' => 'integer',
        'folio_inicial' => 'integer',
        'folio_final' => 'integer',
        'id_proyecto' => 'integer',
        'fecha_asignacion' => 'date',
        'fecha_finalizacion' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'folio_inicial' => 'nullable',
        'folio_final' => 'nullable',
        'id_proyecto' => 'nullable|integer',
        'fecha_asignacion' => 'nullable',
        'fecha_finalizacion' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idProyecto()
    {
        return $this->belongsTo(\App\Models\Estacionamiento::class, 'id_proyecto');
    }
}
