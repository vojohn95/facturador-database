<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TIncidenciaTicket
 * @package App\Models
 * @version March 29, 2022, 1:18 am CST
 *
 * @property \Illuminate\Database\Eloquent\Collection $tickets
 * @property string $nombre_incidencia
 */
class TIncidenciaTicket extends Model
{
    use SoftDeletes;

    public $table = 'tipo_incidencia';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $connection = "mysql3";

    public $fillable = [
        'nombre_incidencia'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre_incidencia' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_incidencia' => 'nullable|string|max:45',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tickets()
    {
        return $this->hasMany(\App\Models\Ticket::class, 'id_tipo');
    }
}
