<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class GerentePension
 * @package App\Models
 * @version February 24, 2021, 3:15 am CST
 *
 * @property \Illuminate\Database\Eloquent\Collection $estacionamientos
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 */
class GerentePension extends Model
{
    use SoftDeletes;

    public $table = 'gerentes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $connection = "mysql2";

    public $fillable = [
        'name',
        'email',
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable|string|max:255',
        'email' => 'nullable|string|max:255',
        'password' => 'nullable|string|max:255',
        'remember_token' => 'nullable|string|max:100',
        'created_at' => 'required',
        'updated_at' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function estacionamientos()
    {
        return $this->hasMany(\App\Models\Estacionamiento::class, 'id_gerentes');
    }
}
