<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CatalogoRegiman
 * 
 * @property int $codigoRegimen
 * @property string|null $descripcion
 * @property string|null $fisica
 * @property string|null $moral
 * @property Carbon|null $inicioVigencia
 * @property Carbon|null $FinVigencia
 *
 * @package App\Models
 */
class CatalogoRegiman extends Model
{
	protected $table = 'catalogo_regimen';
	protected $primaryKey = 'codigoRegimen';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'codigoRegimen' => 'int',
		'inicioVigencia' => 'date',
		'FinVigencia' => 'date'
	];

	protected $fillable = [
		'descripcion',
		'fisica',
		'moral',
		'inicioVigencia',
		'FinVigencia'
	];
}
