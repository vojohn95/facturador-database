<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Marcas
 * @package App\Models
 * @version September 19, 2019, 5:13 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection orgs
 * @property string marca
 */
class Marcas extends Model
{
    use SoftDeletes;

    public $table = 'marcas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'marca'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'marca' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'marca' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function orgs()
    {
        return $this->belongsToMany(\App\Models\Org::class, 'parks');
    }
}
