<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class catMetodoPago
 * @package App\Models
 * @version February 24, 2021, 3:01 am CST
 *
 * @property \Illuminate\Database\Eloquent\Collection $facturaPensiones
 * @property integer $claveSat
 * @property string $descricion
 */
class catMetodoPago extends Model
{
    public $table = 'cat_Metodopago';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = false;

    public $connection = "mysql2";

    public $fillable = [
        'clave',
        'Descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'clave' => 'string',
        'Descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'clave' => 'nullable|string',
        'Descripcion' => 'nullable|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function facturaPensiones()
    {
        return $this->hasMany(\App\Models\FacturaPensione::class, 'id_metodoPago');
    }
}
