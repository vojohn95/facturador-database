<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ingreso
 * @package App\Models
 * @version October 18, 2019, 10:36 am CDT
 *
 * @property \App\Models\BancoColect idBanco
 * @property \App\Models\EmpresaColect idEmpresa
 * @property \App\Models\Gerente idGerente
 * @property \Illuminate\Database\Eloquent\Collection imgIngresos
 * @property integer id_empresa
 * @property integer id_banco
 * @property integer numero
 * @property string venta
 * @property string fecha_recoleccion
 * @property number importe
 * @property string valido
 * @property string coment
 * @property string tipo
 * @property integer id_gerente
 */
class Ingreso extends Model
{
    use SoftDeletes;

    public $table = 'ingresos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_empresa',
        'id_banco',
        'numero',
        'venta',
        'fecha_recoleccion',
        'importe',
        'valido',
        'coment',
        'tipo',
        'id_gerente'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_empresa' => 'integer',
        'id_banco' => 'integer',
        'numero' => 'integer',
        'venta' => 'date',
        'fecha_recoleccion' => 'date',
        'importe' => 'float',
        'valido' => 'string',
        'coment' => 'string',
        'tipo' => 'string',
        'id_gerente' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_empresa' => 'required',
        'id_banco' => 'required',
        'valido' => 'required',
        'tipo' => 'required',
        'id_gerente' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idBanco()
    {
        return $this->belongsTo(\App\Models\BancoColect::class, 'id_banco');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idEmpresa()
    {
        return $this->belongsTo(\App\Models\EmpresaColect::class, 'id_empresa');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idGerente()
    {
        return $this->belongsTo(\App\Models\Gerente::class, 'id_gerente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function imgIngresos()
    {
        return $this->hasMany(\App\Models\ImgIngreso::class, 'id_ingreso');
    }
}
