<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NivelesTicket
 * @package App\Models
 * @version March 29, 2022, 1:17 am CST
 *
 * @property \Illuminate\Database\Eloquent\Collection $tickets
 * @property string $nombre_nivel
 * @property string $tiempo_respuesta
 */
class NivelesTicket extends Model
{
    use SoftDeletes;

    public $table = 'niveles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $connection = "mysql3";

    public $fillable = [
        'nombre_nivel',
        'tiempo_respuesta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre_nivel' => 'string',
        'tiempo_respuesta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_nivel' => 'nullable|string|max:45',
        'tiempo_respuesta' => 'nullable|string|max:45',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tickets()
    {
        return $this->hasMany(\App\Models\Ticket::class, 'id_nivel');
    }
}
