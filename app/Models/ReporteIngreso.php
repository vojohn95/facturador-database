<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReporteIngreso
 *
 * @property int $id
 * @property int|null $id_estacionamiento
 * @property Carbon|null $fecha
 * @property float|null $ingreso_total
 * @property float|null $ingreso_th
 * @property float|null $factura_global
 *
 * @property Park|null $park
 *
 * @package App\Models
 */
class ReporteIngreso extends Model
{
	protected $table = 'reporte_ingresos';
	public $timestamps = false;

	protected $casts = [
		'id_estacionamiento' => 'int',
		'ingreso_total' => 'float',
		'ingreso_th' => 'float',
		'factura_global' => 'float'
	];

	protected $dates = [
		'fecha'
	];

	protected $fillable = [
		'id_estacionamiento',
		'fecha',
		'ingreso_total',
		'ingreso_th',
		'factura_global'
	];

	public function park()
	{
		return $this->belongsTo(Park::class, 'id_estacionamiento');
	}
}
