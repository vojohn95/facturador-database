<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TicketsTicket
 * @package App\Models
 * @version March 29, 2022, 1:18 am CST
 *
 * @property \App\Models\Categorium $idCategoria
 * @property \App\Models\Estatus $idEstatus
 * @property \App\Models\Nivele $idNivel
 * @property \App\Models\TipoIncidencium $idTipo
 * @property string $nombre_ticket
 * @property string $descripcion
 * @property integer $id_proyecto
 * @property integer $id_tipo
 * @property integer $id_asignacion
 * @property integer $id_categoria
 * @property integer $id_nivel
 * @property integer $id_estatus
 * @property integer $id_usuario
 */
class TicketsTicket extends Model
{
    use SoftDeletes;

    public $table = 'tickets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $connection = "mysql3";

    public $fillable = [
        'nombre_ticket',
        'descripcion',
        'id_proyecto',
        'id_tipo',
        'id_asignacion',
        'id_categoria',
        'id_nivel',
        'id_estatus',
        'id_usuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_ticket' => 'integer',
        'nombre_ticket' => 'string',
        'descripcion' => 'string',
        'id_proyecto' => 'integer',
        'id_tipo' => 'integer',
        'id_asignacion' => 'integer',
        'id_categoria' => 'integer',
        'id_nivel' => 'integer',
        'id_estatus' => 'integer',
        'id_usuario' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_ticket' => 'nullable|string|max:100',
        'descripcion' => 'nullable|string',
        'id_proyecto' => 'nullable|integer',
        'id_tipo' => 'nullable|integer',
        'id_asignacion' => 'nullable',
        'id_categoria' => 'nullable|integer',
        'id_nivel' => 'nullable|integer',
        'id_estatus' => 'nullable|integer',
        'id_usuario' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idCategoria()
    {
        return $this->belongsTo(\App\Models\Categorium::class, 'id_categoria');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idEstatus()
    {
        return $this->belongsTo(\App\Models\Estatus::class, 'id_estatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idNivel()
    {
        return $this->belongsTo(\App\Models\Nivele::class, 'id_nivel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idTipo()
    {
        return $this->belongsTo(\App\Models\TipoIncidencium::class, 'id_tipo');
    }
}
