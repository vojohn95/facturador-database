<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cliente
 * @package App\Models
 * @version February 24, 2021, 2:37 am CST
 *
 * @property \App\Models\PensionesDet $idPension
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $nombres
 * @property string $email
 * @property string $RFC
 * @property integer $id_pension
 * @property string $estatus
 */
class CertificadoFiscal extends Model
{
    use SoftDeletes;

    public $table = 'certificados_fiscales';

    public $fillable = [
        'id_cliente',
        'pdf',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_cliente' => 'integer',
        'pdf' => 'string',
    ];


}
