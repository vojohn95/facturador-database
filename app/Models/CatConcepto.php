<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CatConcepto
 * @package App\Models
 * @version May 3, 2021, 6:21 pm CDT
 *
 * @property string $DESCRIPCION
 * @property string $TIPO
 * @property integer $ID_MAT_ARTICULO
 * @property string $CLIENTE_ERP
 * @property string|\Carbon\Carbon $FECHA_ALTA
 * @property string|\Carbon\Carbon $FECHA_BAJA
 * @property string $ESTATUS
 */
class CatConcepto extends Model
{
    //use SoftDeletes;

    public $table = 'cat_concepto';

    /*const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';*/


    //protected $dates = ['deleted_at'];

    public $connection = "mysql2";

    public $fillable = [
        'DESCRIPCION',
        'TIPO',
        'ID_MAT_ARTICULO',
        'CLIENTE_ERP',
        'FECHA_ALTA',
        'FECHA_BAJA',
        'ESTATUS'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ID' => 'integer',
        'DESCRIPCION' => 'string',
        'TIPO' => 'string',
        'ID_MAT_ARTICULO' => 'integer',
        'CLIENTE_ERP' => 'string',
        'FECHA_ALTA' => 'datetime',
        'FECHA_BAJA' => 'datetime',
        'ESTATUS' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'DESCRIPCION' => 'nullable|string|max:50',
        'TIPO' => 'nullable|string|max:3',
        'ID_MAT_ARTICULO' => 'nullable|integer',
        'CLIENTE_ERP' => 'nullable|string|max:45',
        'FECHA_ALTA' => 'nullable',
        'FECHA_BAJA' => 'nullable',
        'ESTATUS' => 'nullable|string|max:3'
    ];


}
