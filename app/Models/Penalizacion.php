<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Penalizacion
 * @package App\Models
 * @version February 24, 2021, 3:11 am CST
 *
 * @property \App\Models\Estacionamiento $idEstacionamiento
 * @property number $monto
 * @property string $concepto
 * @property integer $id_estacionamiento
 */
class Penalizacion extends Model
{
    public $table = 'tab_penalizaciones';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    public $timestamps = false;

    public $connection = "mysql2";

    public $fillable = [
        'monto',
        'concepto',
        'id_estacionamiento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'monto' => 'float',
        'concepto' => 'string',
        'id_estacionamiento' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'monto' => 'nullable|numeric',
        'concepto' => 'nullable|string|max:45',
        'id_estacionamiento' => 'nullable|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idEstacionamiento()
    {
        return $this->belongsTo(\App\Models\Estacionamiento::class, 'id_estacionamiento');
    }
}
