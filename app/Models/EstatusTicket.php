<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class EstatusTicket
 * @package App\Models
 * @version March 29, 2022, 1:17 am CST
 *
 * @property \Illuminate\Database\Eloquent\Collection $tickets
 * @property string $nombre_estatus
 */
class EstatusTicket extends Model
{
    use SoftDeletes;

    public $table = 'estatus';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $connection = "mysql3";

    public $fillable = [
        'nombre_estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre_estatus' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre_estatus' => 'nullable|string|max:45',
        'created_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tickets()
    {
        return $this->hasMany(\App\Models\Ticket::class, 'id_estatus');
    }
}
