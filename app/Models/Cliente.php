<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cliente
 * @package App\Models
 * @version February 24, 2021, 2:37 am CST
 *
 * @property \App\Models\PensionesDet $idPension
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $nombres
 * @property string $email
 * @property string $RFC
 * @property integer $id_pension
 * @property string $estatus
 */
class Cliente extends Model
{
    public $table = 'Clientes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'apellido_paterno',
        'apellido_materno',
        'nombres',
        'email',
        'RFC',
        'id_pension',
        'estatus'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'apellido_paterno' => 'string',
        'apellido_materno' => 'string',
        'nombres' => 'string',
        'email' => 'string',
        'RFC' => 'string',
        'id_pension' => 'integer',
        'estatus' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'apellido_paterno' => 'nullable|string|max:100',
        'apellido_materno' => 'nullable|string|max:100',
        'nombres' => 'nullable|string|max:200',
        'email' => 'nullable|string|max:150',
        'RFC' => 'nullable|string|max:14',
        'id_pension' => 'nullable|integer',
        'estatus' => 'nullable|string|max:1',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idPension()
    {
        return $this->belongsTo(\App\Models\PensionesDet::class, 'id_pension');
    }
}
