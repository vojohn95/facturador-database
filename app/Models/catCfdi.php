<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class catCfdi
 * @package App\Models
 * @version February 24, 2021, 2:59 am CST
 *
 * @property \Illuminate\Database\Eloquent\Collection $facturaPensiones
 * @property string $claveSat
 * @property string $descricion
 */
class catCfdi extends Model
{
    public $table = 'cat_CfdiUso';
    public $timestamps = false;

    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';

    public $connection = "mysql2";

    public $fillable = [
        'claveSat',
        'descricion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'claveSat' => 'string',
        'descricion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'claveSat' => 'nullable|string|max:10',
        'descricion' => 'nullable|string|max:30'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function facturaPensiones()
    {
        return $this->hasMany(\App\Models\FacturaPensione::class, 'id_usoCFDI');
    }
}
