<?php

namespace App\Funciones;

use App\Models\FacturaPension;
use App\Models\Org;
use Barryvdh\DomPDF\Facade as PDF;
use CfdiUtils\CadenaOrigen\DOMBuilder;
use CfdiUtils\XmlResolver\XmlResolver;
use Endroid\QrCode\QrCode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 08/10/2019
 * Time: 10:33 AM
 */
class TimbrarPension
{
    public function Factura($id, $id_pension)
    {
        $org = Org::find(1);
        //Query timbrado
        $totales = "select  distinct(pd.id) as \"Numeropension\",
        'S' as Serie,
        (SELECT Auto_increment
      FROM information_schema.tables
      WHERE table_name='facturaPensiones') as \"Numerofactura\",
       concat(cl.nombres, \" \", cl.apellido_paterno, \" \", cl.apellido_materno) as \"razonsocial\", cl.RFC as \"rfc\",
       cl.email,
       est.no_estacionamiento as \"NumeroProyecto\", est.nombre_proyecto as \"Proyecto\", est.cp as \"CPexpedicion\",
       fp.claveSat as \"formaPago\",
       mp.clave as \"MetodoPago\",
       cuc.claveSat as \"usoCFDI\",
       concat('Pago de pensión número ',
       pd.id, ' correspondiente al mes de ',
       pg.mes, ' del ', pg.año) as \"Concepto\", pg.subtotal, pg.iva, pg.total from pensionesDet pd inner join
       estacionamientos est on est.id = pd.id_estacionamiento inner join Clientes cl on pd.id = cl.id_pension inner join
       pagos pg on pg.idPension = pd.id  inner join
      cat_formaPago fp on fp.id = pg.id_formaPago inner join cat_Metodopago mp on mp.id = pg.id_metodoPago
      inner join facturaPensiones  fpen on fpen.id_pen = pd.id
      inner join cat_CfdiUso cuc on cuc.id = fpen.id_usoCFDI
      where  pg.estatus_factura is null and pd.id = $id_pension";
        $result1 = DB::connection('mysql2')->SELECT($totales);

        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');
        $url = '';
        $token = '';
        $certificado = new \CfdiUtils\Certificado\Certificado($org->ruta_cer);
        $comprobanteAtributos = [
            'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'LugarExpedicion' => $org->cp,
            'MetodoPago' => $result1[0]->MetodoPago,
            'TipoDeComprobante' => 'I',
            'Total' => number_format($result1[0]->total, 2),
            'Moneda' => 'MXN',
            'SubTotal' => number_format(($result1[0]->total / 1.16), 2),
            'FormaPago' => $result1[0]->formaPago,
            'Fecha' => $fecha . "T" . $fecha2,
            'Folio' => $result1[0]->Numerofactura,
            'Serie' => $result1[0]->Serie,
            'Version' => '3.3',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
        $comprobante = $creator->comprobante();
        $comprobante->addEmisor([
            'RegimenFiscal' => $org['Regimen_fiscal'],
            'Nombre' => $org['Razon_social'],
            'Rfc' => $org['RFC'],
        ]);
        $comprobante->addReceptor([
            'UsoCFDI' => $result1[0]->usoCFDI,
            'Nombre' => $result1[0]->razonsocial,
            'Rfc' => trim($result1[0]->rfc),
        ]);
        $comprobante->addConcepto([
            'ClaveProdServ' => '78111807',
            'Cantidad' => '1.00',
            'ClaveUnidad' => 'E48',
            'Unidad' => 'Unidad de servicio',
            'Descripcion' => $result1[0]->Concepto,
            'ValorUnitario' => str_replace(",", "", number_format($result1[0]->total / 1.16, 2)),
            'Importe' => str_replace(",", "", number_format($result1[0]->total / 1.16, 2)),
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format(($result1[0]->total / 1.16) * .16, 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($result1[0]->total / 1.16, 2)),
        ]);

        $creator->addSumasConceptos(NULL, 2);
        $key = file_get_contents($org->ruta_pem);
        $creator->addSello($key, 'OCE94120');
        //$creator->addSello($key, Crypt::decryptString($org->pass));
        //$creator->addSello($key, 'OCE94120');
        //$creator->saveXml('text.xml');
        $xml = $creator->asXml();
        $xml2 = base64_encode($xml);

        $body = array(
            'xml' => $xml2,
        );
        /*if ($org->Prod) {
            $url = $org->URL_prod;
            $token = $org->token_prod;
        } else {*/
        $url = "https://api-stage.emite.dev";
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM";
        //}
        $client = new Client([
            'base_uri' => $url,
        ]);
        try {
            $response = $client->post('v1/stamp', [
                //'debug' => TRUE,
                'headers' => [
                    'Accept' => 'application/json; charset=UTF-8',
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ],
                \GuzzleHttp\RequestOptions::JSON => $body
            ]);
            $val2 = json_decode($response->getBody()->getContents());
        } catch (ClientException  $e) {
            //dd('Excepción capturada: ',  $e->getRequest(), $e->getResponse()->getBody()->getContents(), 'Papu ayuda a presentar errores');
            //
            $error = json_decode($e->getResponse()->getBody()->getContents());
            //dd($error, $xml);
            $errores_timb = $error->data->details;
            foreach ($errores_timb as $value) {
                $validation = $value->validation;
                flash('Message')->error($validation);
            }

            //$salida = FALSE;
            return;
        }


        if ($val2->data->status == "previously_stamped" || $val2->data->status == "stamped") {
            $xml3 = $val2->data->document_info->xml;
            $uuid = $val2->data->stamp_info->uuid;
            $xml3 = base64_decode($xml3);
            file_put_contents("Facturas/" . $uuid . ".xml", $xml3);
            $xmlContent = file_get_contents("Facturas/" . $uuid . ".xml");
            $resolver = new XmlResolver();
            $location = $resolver->resolveCadenaOrigenLocation('3.3');
            $builder = new DOMBuilder();
            $cadenaorigen = $builder->build($xmlContent, $location);
            /**/
            $xmlContents = $xml3;
            $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
            $cfdi->getVersion(); // (string) 3.3
            $cfdi->getDocument(); // clon del objeto DOMDocument
            $cfdi->getSource(); // (string) <cfdi:Comprobante...
            $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
            $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
            $iva = number_format(($result1[0]->total / 1.16) * .16, 2);
            $data = array(
                'org' => [
                    'RS' => $org['Razon_social'],
                    'RFC' => $org['RFC'],
                    'dir' => $org['dir']
                ],
                'client' => [
                    'RS' => $result1[0]->razonsocial,
                    'RFC' => trim($result1[0]->rfc),
                ],
                'fact' => [
                    'SF' => $result1[0]->Numerofactura,
                    'FF' => $uuid,
                    'CSD' => $complemento['NoCertificado'],
                    'FHE' => $complemento['Fecha'],
                    'FHC' => $tfd['FechaTimbrado'],
                    'NSAT' => $tfd['NoCertificadoSAT'],
                    'UC' => $result1[0]->usoCFDI,
                    'MP' => $result1[0]->MetodoPago,
                    'FP' => $result1[0]->formaPago,
                    'RF' => $org['Regimen_fiscal'],
                ],
                'con' => [
                    'CA' => '1.00',
                    'UN' => 'Unidad de servicio',
                    'PU' => number_format($result1[0]->total / 1.16, 2),
                    'DE' => '0.00',
                    'IM' => number_format($result1[0]->total / 1.16, 2),
                    'IVA' => $iva,
                    'IEPS' => '0.00',
                    'IMP_IEPS' => '0.00',
                ],
                'TT' => [
                    'T' => number_format($result1[0]->total, 2),
                    'ST' => number_format(($result1[0]->total / 1.16), 2),
                    'IVA' => $iva,
                ],
                'FC' => [
                    'CO' => $cadenaorigen,
                    'SSAT' => wordwrap($tfd['SelloSAT'], 120, "\n", true),
                    'SCFD' => wordwrap($tfd['SelloCFD'], 120, "\n", true),
                ]
            );

            $ochocarct = substr($uuid, -8);
            $qrCode = new QrCode('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&r' . $org['RFC'] . '&rr=' . $result1[0]->rfc . '&tt=' . number_format($result1[0]->total, 2) . '0000');
            header('Content-Type: ' . $qrCode->getContentType());
            $qrCode->writeString();
            $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
            $qrCode->writeFile($ubiQr);
            $pdf = PDF::loadView('PDF.pdf', compact('data'))->save("Facturas/" . $uuid . '.pdf');
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            /*$mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "ocefacturacion@gmail.com";
            $mail->Password = "Integrador1";
            $mail->setFrom('ocefacturacion@gmail.com', 'Central operadora de estacionamientos');*/
            $mail->Host = 'smtp.office365.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "facturacion-oce@central-mx.com";
            $mail->Password = "1t3gr4d0r2020*";
            $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
            //$mail->From = "no-reply@central-mx.com";
            //$mail->FromName = "Central operadora de estacionamientos";
            $mail->Subject = "Factura" . $result1[0]->Serie . ($result1[0]->Numerofactura);
            $mail->Body = "<html>
                <meta charset='utf-8'>
                <h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $result1[0]->Serie . " y Folio: " . ($result1[0]->Numerofactura) . ". Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $result1[0]->email . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
                <p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
                <p align='center;'>Contactanos</p>
                <p align='center;'>Teléfono: (55) 3640-3900</p>
                <p align='center;'>Correo: facturacion@central-mx.com</p>
                </html>";
            $mail->AddAddress($result1[0]->email);
            $archivo = "Facturas/" . $uuid . ".xml";
            $pdf1 = "Facturas/" . $uuid . '.pdf';
            $mail->AddAttachment($archivo);
            $mail->AddAttachment($pdf1);
            $mail->IsHTML(true);
            $mail->Send();

            $contenido = file_get_contents("Facturas/" . $uuid . '.pdf');
            $facturasPension = FacturaPension::find($id);
            $facturasPension->serie = $result1[0]->Serie;
            $facturasPension->folio = $result1[0]->Numerofactura;
            $facturasPension->fecha_timbrado = date("Y-m-d H:i:s");
            $facturasPension->uuid = $uuid;
            $facturasPension->subtotal_factura = str_replace(",", "",  number_format(($result1[0]->total / 1.16), 2));
            $facturasPension->iva_factura = str_replace(",", "", $iva);
            $facturasPension->total_factura = str_replace(",", "", number_format($result1[0]->total, 2));
            $facturasPension->XML = $xml3;
            $facturasPension->PDF = base64_encode($contenido);
            $facturasPension->estatus = 1;
            $facturasPension->save();

            unlink($archivo);
            unlink($pdf1);
            unlink($ubiQr);
            flash('Message')->success('Factura generada con éxito');

        }
    }
}
