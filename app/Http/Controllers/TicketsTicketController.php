<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTicketsTicketRequest;
use App\Http\Requests\UpdateTicketsTicketRequest;
use App\Repositories\TicketsTicketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TicketsTicketController extends AppBaseController
{
    /** @var  TicketsTicketRepository */
    private $ticketsTicketRepository;

    public function __construct(TicketsTicketRepository $ticketsTicketRepo)
    {
        $this->ticketsTicketRepository = $ticketsTicketRepo;
    }

    /**
     * Display a listing of the TicketsTicket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ticketsTickets = $this->ticketsTicketRepository->all();

        return view('tickets_tickets.index')
            ->with('ticketsTickets', $ticketsTickets);
    }

    /**
     * Show the form for creating a new TicketsTicket.
     *
     * @return Response
     */
    public function create()
    {
        return view('tickets_tickets.create');
    }

    /**
     * Store a newly created TicketsTicket in storage.
     *
     * @param CreateTicketsTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketsTicketRequest $request)
    {
        $input = $request->all();

        $ticketsTicket = $this->ticketsTicketRepository->create($input);

        flash('Message')->success('Tickets Ticket saved successfully.');

        return redirect(route('ticketsTickets.index'));
    }

    /**
     * Display the specified TicketsTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticketsTicket = $this->ticketsTicketRepository->find($id);

        if (empty($ticketsTicket)) {
            flash('Message')->error('Tickets Ticket not found');

            return redirect(route('ticketsTickets.index'));
        }

        return view('tickets_tickets.show')->with('ticketsTicket', $ticketsTicket);
    }

    /**
     * Show the form for editing the specified TicketsTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketsTicket = $this->ticketsTicketRepository->find($id);

        if (empty($ticketsTicket)) {
            flash('Message')->error('Tickets Ticket not found');

            return redirect(route('ticketsTickets.index'));
        }

        return view('tickets_tickets.edit')->with('ticketsTicket', $ticketsTicket);
    }

    /**
     * Update the specified TicketsTicket in storage.
     *
     * @param int $id
     * @param UpdateTicketsTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketsTicketRequest $request)
    {
        $ticketsTicket = $this->ticketsTicketRepository->find($id);

        if (empty($ticketsTicket)) {
            flash('Message')->error('Tickets Ticket not found');

            return redirect(route('ticketsTickets.index'));
        }

        $ticketsTicket = $this->ticketsTicketRepository->update($request->all(), $id);

        flash('Message')->success('Tickets Ticket updated successfully.');

        return redirect(route('ticketsTickets.index'));
    }

    /**
     * Remove the specified TicketsTicket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketsTicket = $this->ticketsTicketRepository->find($id);

        if (empty($ticketsTicket)) {
            flash('Message')->error('Tickets Ticket not found');

            return redirect(route('ticketsTickets.index'));
        }

        $this->ticketsTicketRepository->delete($id);

        flash('Message')->success('Tickets Ticket deleted successfully.');

        return redirect(route('ticketsTickets.index'));
    }
}
