<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\catCfdi;
use App\Funciones\TimbrarPension;
use App\Models\Pension;
use Illuminate\Http\Request;
use App\Models\catMetodoPago;
use App\Models\FacturaPension;
use App\Http\Controllers\AppBaseController;
use App\Repositories\FacturaPensionRepository;
use App\Http\Requests\CreateFacturaPensionRequest;
use App\Http\Requests\UpdateFacturaPensionRequest;
use CfdiUtils\Validate\Cfdi33\RecepcionPagos\UsoCfdi;
use Illuminate\Support\Facades\DB;

class FacturaPensionController extends AppBaseController
{
    /** @var  FacturaPensionRepository */
    private $facturaPensionRepository;

    public function __construct(FacturaPensionRepository $facturaPensionRepo)
    {
        $this->facturaPensionRepository = $facturaPensionRepo;
    }

    /**
     * Display a listing of the FacturaPension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //$facturaPensions = $this->facturaPensionRepository->all();

        $facturaPensions = DB::connection('mysql2')->table('facturaPensiones')
            ->join('pensionesDet', 'facturaPensiones.id_pen', '=', 'pensionesDet.id')
            ->select('facturaPensiones.*', 'pensionesDet.id as id_pen')
            ->get();
        //dd($facturaPensions);

        return view('factura_pensions.index')
            ->with('facturaPensions', $facturaPensions);
    }

    /**
     * Show the form for creating a new FacturaPension.
     *
     * @return Response
     */
    public function create()
    {
        $pensiones = Pension::all();
        $usocfdis = catCfdi::all();
        $metodos =  catMetodoPago::all();
        //dd($metodos);
        return view('factura_pensions.create')->with('pensiones', $pensiones)->with('cfdis', $usocfdis)->with('metodos', $metodos);
    }

    /**
     * Store a newly created FacturaPension in storage.
     *
     * @param CreateFacturaPensionRequest $request
     *
     * @return Response
     */
    public function store(CreateFacturaPensionRequest $request)
    {
        $input = $request->all();

        $xml = '';
        $pdf = '';

        $xml = request()->file('XML');
        if ($xml != null) {
            $datacont = $xml->get();
            $nombre_contrato = $xml->getBasename();
            $cont = file_get_contents($xml);
            //$contdata = base64_encode($cont);
            $input['XML'] = $cont;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pdf = request()->file('PDF');
        if ($pdf != null) {
            $datascont = $pdf->get();
            $nombre_scontrato = $pdf->getBasename();
            $scont = file_get_contents($pdf);
            $scontdata = base64_encode($scont);
            $input['PDF'] = $scontdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $facturaPension = $this->facturaPensionRepository->create($input);

        flash('Message')->success('Factura actualizada.');

        return redirect(route('facturaPensions.index'));
    }

    /**
     * Display the specified FacturaPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            flash('Message')->error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        return view('factura_pensions.show')->with('facturaPension', $facturaPension);
    }

    /**
     * Show the form for editing the specified FacturaPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            flash('Message')->error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $pensiones = Pension::all();
        $usocfdis = catCfdi::all();
        $metodos =  catMetodoPago::all();

        return view('factura_pensions.edit')->with('facturaPension', $facturaPension)->with('pensiones', $pensiones)
            ->with('cfdis', $usocfdis)->with('metodos', $metodos);
    }

    /**
     * Update the specified FacturaPension in storage.
     *
     * @param int $id
     * @param UpdateFacturaPensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacturaPensionRequest $request)
    {
        $input = request()->all();
        $facturaPension = facturaPension::find($id);

        if (empty($facturaPension)) {
            flash('Message')->error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        if ($input['id_pen']) {
            $facturaPension->id_pen = $input['id_pen'];
        }

        if ($input['serie']) {
            $facturaPension->serie = $input['serie'];
        }

        if ($input['folio']) {
            $facturaPension->folio = $input['folio'];
        }

        if ($input['fecha_timbrado']) {
            $facturaPension->fecha_timbrado = $input['fecha_timbrado'];
        }

        if ($input['uuid']) {
            $facturaPension->uuid = $input['uuid'];
        }

        if ($input['subtotal_factura']) {
            $facturaPension->subtotal_factura = $input['subtotal_factura'];
        }

        if ($input['iva_factura']) {
            $facturaPension->iva_factura = $input['iva_factura'];
        }

        if ($input['total_factura']) {
            $facturaPension->total_factura = $input['total_factura'];
        }

        if ($input['id_usoCFDI']) {
            $facturaPension->id_usoCFDI = $input['id_usoCFDI'];
        }

        if ($input['id_metodoPago']) {
            $facturaPension->id_metodoPago = $input['id_metodoPago'];
        }

        $xml = '';
        $pdf = '';

        $xml = request()->file('XML');
        if ($xml != null) {
            $datacont = $xml->get();
            $nombre_contrato = $xml->getBasename();
            $cont = file_get_contents($xml);
            //$contdata = base64_encode($cont);
            $facturaPension->XML = $cont;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pdf = request()->file('PDF');
        if ($pdf != null) {
            $datacontd = $pdf->get();
            $nombre_contratod = $pdf->getBasename();
            $contd = file_get_contents($pdf);
            $contdatad = base64_encode($pdf);
            //dd($contdatad);
            $facturaPension->PDF = $contdatad;
            //$contrato = $this->pensionRepository->create($input);
        }

        $facturaPension->save();

        flash('Message')->success('Factura actualizado.');

        return redirect(route('facturaPensions.index'));
    }

    /**
     * Remove the specified FacturaPension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facturaPension = $this->facturaPensionRepository->find($id);

        if (empty($facturaPension)) {
            flash('Message')->error('Factura Pension not found');

            return redirect(route('facturaPensions.index'));
        }

        $this->facturaPensionRepository->delete($id);

        flash('Message')->success('Factura eliminada.');

        return redirect(route('facturaPensions.index'));
    }

    public function downloadpenPdf($id)
    {
        $pdf = FacturaPension::find($id);
        $archivo = $pdf->PDF;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $pdf->id . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=contrato-fact$pdf->id.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadpenXML($id)
    {
        //$id = \request()->request;
        //dd($id);
        $xml = FacturaPension::find($id);
        $archivo = $xml->XML;
        $path = "xml_down/" . $xml->id . ".xml";
        //dd($uuid);
        file_put_contents($path, $archivo);
        header("Content-disposition: attachment; filename=fact$xml->id.xml");
        header("Content-type: application/xml");
        readfile($path);
        unlink($path);

        //flash('Message')->success('Factura descargada');

        //return redirect(route('facturas.index'));
    }

    public function rechazo($id)
    {

        $query = FacturaPension::findOrFail($id);
        $estatus = $query->estatus;

        $query->estatus = '3';
        $query->save();

        flash('Message')->success('Factura rechazada');

        return redirect(route('facturaPensions.index'));
    }

    public function valido($id)
    {
        $validar = facturaPension::find($id);
        //dd($validar);

        if ($validar->estatus == 1) {
            flash('Message')->error('Ticket timbrado previamente');
            return redirect(route('facturaPensions.index'));
        }

        $timbrar = new TimbrarPension();
        $timbrar->Factura($id, $validar->id_pen);

        return redirect(route('facturaPensions.index'));
    }
}
