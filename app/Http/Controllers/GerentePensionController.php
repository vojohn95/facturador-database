<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGerentePensionRequest;
use App\Http\Requests\UpdateGerentePensionRequest;
use App\Repositories\GerentePensionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GerentePensionController extends AppBaseController
{
    /** @var  GerentePensionRepository */
    private $gerentePensionRepository;

    public function __construct(GerentePensionRepository $gerentePensionRepo)
    {
        $this->gerentePensionRepository = $gerentePensionRepo;
    }

    /**
     * Display a listing of the GerentePension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $gerentePensions = $this->gerentePensionRepository->all();

        return view('gerente_pensions.index')
            ->with('gerentePensions', $gerentePensions);
    }

    /**
     * Show the form for creating a new GerentePension.
     *
     * @return Response
     */
    public function create()
    {
        return view('gerente_pensions.create');
    }

    /**
     * Store a newly created GerentePension in storage.
     *
     * @param CreateGerentePensionRequest $request
     *
     * @return Response
     */
    public function store(CreateGerentePensionRequest $request)
    {
        $input = $request->all();

        $gerentePension = $this->gerentePensionRepository->create($input);

        flash('Message')->success('Gerente Pension saved successfully.');

        return redirect(route('gerentePensions.index'));
    }

    /**
     * Display the specified GerentePension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gerentePension = $this->gerentePensionRepository->find($id);

        if (empty($gerentePension)) {
            flash('Message')->error('Gerente Pension not found');

            return redirect(route('gerentePensions.index'));
        }

        return view('gerente_pensions.show')->with('gerentePension', $gerentePension);
    }

    /**
     * Show the form for editing the specified GerentePension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gerentePension = $this->gerentePensionRepository->find($id);

        if (empty($gerentePension)) {
            flash('Message')->error('Gerente Pension not found');

            return redirect(route('gerentePensions.index'));
        }

        return view('gerente_pensions.edit')->with('gerentePension', $gerentePension);
    }

    /**
     * Update the specified GerentePension in storage.
     *
     * @param int $id
     * @param UpdateGerentePensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGerentePensionRequest $request)
    {
        $gerentePension = $this->gerentePensionRepository->find($id);

        if (empty($gerentePension)) {
            flash('Message')->error('Gerente Pension not found');

            return redirect(route('gerentePensions.index'));
        }

        $gerentePension = $this->gerentePensionRepository->update($request->all(), $id);

        flash('Message')->success('Gerente Pension updated successfully.');

        return redirect(route('gerentePensions.index'));
    }

    /**
     * Remove the specified GerentePension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gerentePension = $this->gerentePensionRepository->find($id);

        if (empty($gerentePension)) {
            flash('Message')->error('Gerente Pension not found');

            return redirect(route('gerentePensions.index'));
        }

        $this->gerentePensionRepository->delete($id);

        flash('Message')->success('Gerente Pension deleted successfully.');

        return redirect(route('gerentePensions.index'));
    }
}
