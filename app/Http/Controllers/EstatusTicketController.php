<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstatusTicketRequest;
use App\Http\Requests\UpdateEstatusTicketRequest;
use App\Repositories\EstatusTicketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EstatusTicketController extends AppBaseController
{
    /** @var  EstatusTicketRepository */
    private $estatusTicketRepository;

    public function __construct(EstatusTicketRepository $estatusTicketRepo)
    {
        $this->estatusTicketRepository = $estatusTicketRepo;
    }

    /**
     * Display a listing of the EstatusTicket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $estatusTickets = $this->estatusTicketRepository->all();

        return view('estatus_tickets.index')
            ->with('estatusTickets', $estatusTickets);
    }

    /**
     * Show the form for creating a new EstatusTicket.
     *
     * @return Response
     */
    public function create()
    {
        return view('estatus_tickets.create');
    }

    /**
     * Store a newly created EstatusTicket in storage.
     *
     * @param CreateEstatusTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateEstatusTicketRequest $request)
    {
        $input = $request->all();

        $estatusTicket = $this->estatusTicketRepository->create($input);

        flash('Message')->success('Estatus Ticket saved successfully.');

        return redirect(route('estatusTickets.index'));
    }

    /**
     * Display the specified EstatusTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estatusTicket = $this->estatusTicketRepository->find($id);

        if (empty($estatusTicket)) {
            flash('Message')->error('Estatus Ticket not found');

            return redirect(route('estatusTickets.index'));
        }

        return view('estatus_tickets.show')->with('estatusTicket', $estatusTicket);
    }

    /**
     * Show the form for editing the specified EstatusTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estatusTicket = $this->estatusTicketRepository->find($id);

        if (empty($estatusTicket)) {
            flash('Message')->error('Estatus Ticket not found');

            return redirect(route('estatusTickets.index'));
        }

        return view('estatus_tickets.edit')->with('estatusTicket', $estatusTicket);
    }

    /**
     * Update the specified EstatusTicket in storage.
     *
     * @param int $id
     * @param UpdateEstatusTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstatusTicketRequest $request)
    {
        $estatusTicket = $this->estatusTicketRepository->find($id);

        if (empty($estatusTicket)) {
            flash('Message')->error('Estatus Ticket not found');

            return redirect(route('estatusTickets.index'));
        }

        $estatusTicket = $this->estatusTicketRepository->update($request->all(), $id);

        flash('Message')->success('Estatus Ticket updated successfully.');

        return redirect(route('estatusTickets.index'));
    }

    /**
     * Remove the specified EstatusTicket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estatusTicket = $this->estatusTicketRepository->find($id);

        if (empty($estatusTicket)) {
            flash('Message')->error('Estatus Ticket not found');

            return redirect(route('estatusTickets.index'));
        }

        $this->estatusTicketRepository->delete($id);

        flash('Message')->success('Estatus Ticket deleted successfully.');

        return redirect(route('estatusTickets.index'));
    }
}
