<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecatCfdiRequest;
use App\Http\Requests\UpdatecatCfdiRequest;
use App\Repositories\catCfdiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class catCfdiController extends AppBaseController
{
    /** @var  catCfdiRepository */
    private $catCfdiRepository;

    public function __construct(catCfdiRepository $catCfdiRepo)
    {
        $this->catCfdiRepository = $catCfdiRepo;
    }

    /**
     * Display a listing of the catCfdi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $catCfdis = $this->catCfdiRepository->all();

        return view('cat_cfdis.index')
            ->with('catCfdis', $catCfdis);
    }

    /**
     * Show the form for creating a new catCfdi.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_cfdis.create');
    }

    /**
     * Store a newly created catCfdi in storage.
     *
     * @param CreatecatCfdiRequest $request
     *
     * @return Response
     */
    public function store(CreatecatCfdiRequest $request)
    {
        $input = $request->all();

        $catCfdi = $this->catCfdiRepository->create($input);

        flash('Message')->success('Cat Cfdi añadido satisfactoriamente.');

        return redirect(route('catCfdis.index'));
    }

    /**
     * Display the specified catCfdi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catCfdi = $this->catCfdiRepository->find($id);

        if (empty($catCfdi)) {
            flash('Message')->error('Cat Cfdi not found');

            return redirect(route('catCfdis.index'));
        }

        return view('cat_cfdis.show')->with('catCfdi', $catCfdi);
    }

    /**
     * Show the form for editing the specified catCfdi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catCfdi = $this->catCfdiRepository->find($id);

        if (empty($catCfdi)) {
            flash('Message')->error('Cat Cfdi not found');

            return redirect(route('catCfdis.index'));
        }

        return view('cat_cfdis.edit')->with('catCfdi', $catCfdi);
    }

    /**
     * Update the specified catCfdi in storage.
     *
     * @param int $id
     * @param UpdatecatCfdiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecatCfdiRequest $request)
    {
        $catCfdi = $this->catCfdiRepository->find($id);

        if (empty($catCfdi)) {
            flash('Message')->error('Cat Cfdi not found');

            return redirect(route('catCfdis.index'));
        }

        $catCfdi = $this->catCfdiRepository->update($request->all(), $id);

        flash('Message')->success('Cat Cfdi actualizado satisfactoriamente.');

        return redirect(route('catCfdis.index'));
    }

    /**
     * Remove the specified catCfdi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catCfdi = $this->catCfdiRepository->find($id);

        if (empty($catCfdi)) {
            flash('Message')->error('Cat Cfdi not found');

            return redirect(route('catCfdis.index'));
        }

        $this->catCfdiRepository->delete($id);

        flash('Message')->success('Cat Cfdi eliminado.');

        return redirect(route('catCfdis.index'));
    }
}
