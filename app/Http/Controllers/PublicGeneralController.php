<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Factura;
use Endroid\QrCode\QrCode;
use Laracasts\Flash\Flash;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Estacionamiento;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use PHPMailer\PHPMailer\PHPMailer;
use CfdiUtils\CadenaOrigen\DOMBuilder;
use CfdiUtils\XmlResolver\XmlResolver;
use GuzzleHttp\Exception\ClientException;

class PublicGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas = Factura::join('autof_tickets', 'autof_tickets.id', '=', 'auto_facts.id_ticketAF')
            ->select([
                'auto_facts.id',
                'auto_facts.serie',
                'auto_facts.folio',
                'auto_facts.fecha_timbrado',
                'auto_facts.subtotal_factura',
                'auto_facts.iva_factura',
                'auto_facts.total_factura',
                'autof_tickets.email',
                'autof_tickets.Razon_social'
            ])
            ->orderBy('auto_facts.created_at', 'desc')
            ->paginate(250);
        //            dd(DB::getQueryLog()); // Show results of log

        $estacionamiento = Estacionamiento::select('no_est', 'nombre')
            ->get();
        return view('public_general.index')
            ->with('facturas', $facturas)
            ->with('estacionamientos', $estacionamiento)
            ->with('bus', True);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $org = DB::table('orgs')->select('id', 'ruta_cer', 'cp', 'Regimen_fiscal', 'Razon_social', 'RFC', 'ruta_pem', 'URL_prod', 'URL_dev', 'token_prod', 'token_dev', 'Prod', 'dir')->first();
        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');
        $url = '';
        $token = '';

        //generate cfdi 4.0
        if ($org->Prod == '0') {
            $url = $org->URL_prod;
            $token = $org->token_prod;
        } else {
            $url = $org->URL_dev;
            $token = $org->token_dev;
        }

        try {
            $urlebs = "http://127.0.0.1:3000/ebs/publicgeneral";
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $urlebs);
            $response = $response->getBody()->getContents();
            $response = json_decode($response);
        } catch (ClientException  $e) {
            dd('Excepción capturada: ',  $e->getRequest(), $e->getResponse()->getBody()->getContents());
            //
            $error = json_decode($e->getResponse()->getBody()->getContents());

            return false;
        }

        $totalfactura = 0;
        //foreach on array response
        foreach ($response as $key => $value) {
            $totalfactura += $value->valores[0]->total_de_la_factura;
        }

        $certificado = new \CfdiUtils\Certificado\Certificado($org->ruta_cer);
        $comprobanteAtributos = [
            'Serie' => 'XXX',
            'Folio' => '0000123456',
            'LugarExpedicion' => $org->cp,
            'MetodoPago' => 'PUE',
            'TipoDeComprobante' => 'I',
            'Total' => number_format($totalfactura, 2),
            'Moneda' => 'MXN',
            'Exportacion' => '01',
            'SubTotal' => number_format(($totalfactura / 1.16), 2),
            'FormaPago' => '01',
            'Fecha' => $fecha . "T" . $fecha2,
            'Version' => '4.0',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator40($comprobanteAtributos, $certificado);

        $comprobante = $creator->comprobante();

        $comprobante->addEmisor([
            'RegimenFiscal' => '601', // General de Ley Personas Morales
            'Nombre' => "OPERADORA CENTRAL DE ESTACIONAMIENTOS",
            'Rfc' => "OCE9412073L3",
        ]);

        $comprobante->addReceptor([
            'Rfc' => 'XAXX010101000',
            'Nombre' => 'PÚBLICO EN GENERAL',
            'UsoCFDI' => 'S01',
            'DomicilioFiscalReceptor' => "03330",
            'RegimenFiscalReceptor' => '616',
        ]);

        $con = [];
        $contador = 0;
        foreach ($response as $key => $value) {
            $comprobante->addConcepto([
                'Cantidad' => '1',
                'ClaveProdServ' => '78111807',
                'NoIdentificacion' => '0505',
                'Unidad' => 'SERVICIOS',
                'ClaveUnidad' => 'E48',
                'Descripcion' => $value->valores[0]->descripcion_proyecto,
                'ValorUnitario' => str_replace(",", "", number_format($value->valores[0]->total_de_la_factura / 1.16, 2)),
                'Importe' => str_replace(",", "", number_format($value->valores[0]->total_de_la_factura / 1.16, 2)),
                'ObjetoImp' => '02'
            ])->addTraslado([
                'Base' => str_replace(",", "", number_format($value->valores[0]->total_de_la_factura / 1.16, 2)),
                'Impuesto' => '002',
                'TipoFactor' => 'Tasa',
                'TasaOCuota' => '0.160000',
                'Importe' => str_replace(",", "", number_format(($value->valores[0]->total_de_la_factura / 1.16) * .16, 2))
            ]);

            $toString = strval($contador);
            $toString = [
                'PU' => number_format($value->valores[0]->total_de_la_factura / 1.16, 2),
                'IM' => number_format($value->valores[0]->total_de_la_factura / 1.16, 2),
                'IVA' => number_format(($value->valores[0]->total_de_la_factura / 1.16) * .16, 2),
            ];

            array_push($con, $toString);

            $contador++;
        }

        $creator->addSumasConceptos(null, 2);

        $key = file_get_contents($org->ruta_pem);
        $creator->addSello($key, 'CENTRAL3L394');

        $creator->moveSatDefinitionsToComprobante();

        $asserts = $creator->validate();
        $asserts->hasErrors(); // contiene si hay o no errores

        $creator->saveXml(public_path('/facturas/factura.xml'));

        $xml = $creator->asXml();

        $xml2 = base64_encode($xml);
        $body = array(
            'xml' => $xml2,
        );

        $client = new Client([
            'base_uri' => $url,
        ]);
        try {
            $response = $client->post('v1/stamp', [
                //'debug' => TRUE,
                'headers' => [
                    'Accept' => 'application/json; charset=UTF-8',
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                ],
                \GuzzleHttp\RequestOptions::JSON => $body
            ]);
            $val2 = json_decode($response->getBody()->getContents());
        } catch (ClientException  $e) {
            dd('Excepción capturada: ',  $e->getRequest(), $e->getResponse()->getBody()->getContents());
            //
            $error = json_decode($e->getResponse()->getBody()->getContents());
            //dd($error, $xml);
            $errores_timb = $error->data->details;
            foreach ($errores_timb as $value) {
                $validation = $value->validation;
                flash('Message')->error($validation);
            }
            //dd($validation);
            //$salida = FALSE;
            return;
        }
        if ($val2->data->status == "previously_stamped" || $val2->data->status == "stamped") {
            $xml3 = $val2->data->document_info->xml;
            $uuid = $val2->data->stamp_info->uuid;
            $xml3 = base64_decode($xml3);
            file_put_contents("Facturas/" . $uuid . ".xml", $xml3);
            $xmlContent = file_get_contents("Facturas/" . $uuid . ".xml");
            $resolver = new XmlResolver();
            $location = $resolver->resolveCadenaOrigenLocation('4.0');
            $builder = new DOMBuilder();
            $cadenaorigen = $builder->build($xmlContent, $location);
            /**/
            $xmlContents = $xml3;
            $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
            $cfdi->getVersion(); // (string) 3.3
            $cfdi->getDocument(); // clon del objeto DOMDocument
            $cfdi->getSource(); // (string) <cfdi:Comprobante...
            $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
            $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
            $iva = number_format(($totalfactura / 1.16) * .16, 2);
            $data = array(
                'org' => [
                    'RS' => $org->Razon_social,
                    'RFC' => $org->RFC,
                    'dir' => $org->dir,
                ],
                'client' => [
                    'RS' => 'PÚBLICO EN GENERAL',
                    'RFC' => trim("XAXX010101000"),
                ],
                'fact' => [
                    'SF' => "XXX" . '0000123456',
                    'FF' => $uuid,
                    'CSD' => $complemento['NoCertificado'],
                    'FHE' => $complemento['Fecha'],
                    'FHC' => $tfd['FechaTimbrado'],
                    'NSAT' => $tfd['NoCertificadoSAT'],
                    'UC' => 'S01',
                    'MP' => 'PUE',
                    'FP' => '01',
                    'RF' => $org->Regimen_fiscal,
                ],
                'con' => $con,
                'TT' => [
                    'T' => number_format($totalfactura, 2),
                    'ST' => number_format(($totalfactura / 1.16), 2),
                    'IVA' => $iva,
                ],
                'FC' => [
                    'CO' => $cadenaorigen,
                    'SSAT' => wordwrap($tfd['SelloSAT'], 120, "\n", true),
                    'SCFD' => wordwrap($tfd['SelloCFD'], 120, "\n", true),
                ]
            );

            $ochocarct = substr($uuid, -8);
            $qrCode = new QrCode('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&r' . $org->RFC . '&rr=' . 'XAXX010101000' . '&tt=' . number_format($totalfactura, 2) . '0000');
            header('Content-Type: ' . $qrCode->getContentType());
            $qrCode->writeString();
            $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
            $qrCode->writeFile($ubiQr);
            $pdf = PDF::loadView('PDF.publicgeneral', compact('data'))->save("Facturas/" . $uuid . '.pdf');
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            /*$mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "ocefacturacion@gmail.com";
            $mail->Password = "Integrador1";
            $mail->setFrom('ocefacturacion@gmail.com', 'Central operadora de estacionamientos');*/
            $mail->Host = 'smtp.office365.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAuth = true;
            $mail->Username = "facturacion-oce@central-mx.com";
            $mail->Password = "1t3gr4d0r2020*";
            $mail->setFrom('facturacion-oce@central-mx.com', 'Operadora central de estacionamientos');
            //$mail->From = "no-reply@central-mx.com";
            //$mail->FromName = "Central operadora de estacionamientos";
            $mail->Subject = "Factura" . "test publico en general";
            $mail->Body = "<html>
                <meta charset='utf-8'>
                <h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " ,  Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico lfernando@integrador-technology.mx a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p>
                <p align='center;'>Estimado usuario le recomendamos realizar sus facturas en un lapso no mayor a 5 días hábiles</p>
                <p align='center;'>Contactanos</p>
                <p align='center;'>Teléfono: (55) 3640-3900</p>
                <p align='center;'>Correo: facturacion@central-mx.com</p>
                </html>";
            $mail->AddAddress("lfernando@integrador-technology.mx");
            $archivo = "Facturas/" . $uuid . ".xml";
            $pdf1 = "Facturas/" . $uuid . '.pdf';
            $mail->AddAttachment($archivo);
            $mail->AddAttachment($pdf1);
            $mail->IsHTML(true);
            $mail->Send();
            unlink($archivo);
            unlink($pdf1);
            unlink($ubiQr);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
