<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoriaTicketRequest;
use App\Http\Requests\UpdateCategoriaTicketRequest;
use App\Repositories\CategoriaTicketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoriaTicketController extends AppBaseController
{
    /** @var  CategoriaTicketRepository */
    private $categoriaTicketRepository;

    public function __construct(CategoriaTicketRepository $categoriaTicketRepo)
    {
        $this->categoriaTicketRepository = $categoriaTicketRepo;
    }

    /**
     * Display a listing of the CategoriaTicket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoriaTickets = $this->categoriaTicketRepository->all();

        return view('categoria_tickets.index')
            ->with('categoriaTickets', $categoriaTickets);
    }

    /**
     * Show the form for creating a new CategoriaTicket.
     *
     * @return Response
     */
    public function create()
    {
        return view('categoria_tickets.create');
    }

    /**
     * Store a newly created CategoriaTicket in storage.
     *
     * @param CreateCategoriaTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoriaTicketRequest $request)
    {
        $input = $request->all();

        $categoriaTicket = $this->categoriaTicketRepository->create($input);

        flash('Message')->success('Categoria Ticket saved successfully.');

        return redirect(route('categoriaTickets.index'));
    }

    /**
     * Display the specified CategoriaTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoriaTicket = $this->categoriaTicketRepository->find($id);

        if (empty($categoriaTicket)) {
            flash('Message')->error('Categoria Ticket not found');

            return redirect(route('categoriaTickets.index'));
        }

        return view('categoria_tickets.show')->with('categoriaTicket', $categoriaTicket);
    }

    /**
     * Show the form for editing the specified CategoriaTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoriaTicket = $this->categoriaTicketRepository->find($id);

        if (empty($categoriaTicket)) {
            flash('Message')->error('Categoria Ticket not found');

            return redirect(route('categoriaTickets.index'));
        }

        return view('categoria_tickets.edit')->with('categoriaTicket', $categoriaTicket);
    }

    /**
     * Update the specified CategoriaTicket in storage.
     *
     * @param int $id
     * @param UpdateCategoriaTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoriaTicketRequest $request)
    {
        $categoriaTicket = $this->categoriaTicketRepository->find($id);

        if (empty($categoriaTicket)) {
            flash('Message')->error('Categoria Ticket not found');

            return redirect(route('categoriaTickets.index'));
        }

        $categoriaTicket = $this->categoriaTicketRepository->update($request->all(), $id);

        flash('Message')->success('Categoria Ticket updated successfully.');

        return redirect(route('categoriaTickets.index'));
    }

    /**
     * Remove the specified CategoriaTicket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoriaTicket = $this->categoriaTicketRepository->find($id);

        if (empty($categoriaTicket)) {
            flash('Message')->error('Categoria Ticket not found');

            return redirect(route('categoriaTickets.index'));
        }

        $this->categoriaTicketRepository->delete($id);

        flash('Message')->success('Categoria Ticket deleted successfully.');

        return redirect(route('categoriaTickets.index'));
    }
}
