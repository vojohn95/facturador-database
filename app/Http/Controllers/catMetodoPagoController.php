<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecatMetodoPagoRequest;
use App\Http\Requests\UpdatecatMetodoPagoRequest;
use App\Repositories\catMetodoPagoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class catMetodoPagoController extends AppBaseController
{
    /** @var  catMetodoPagoRepository */
    private $catMetodoPagoRepository;

    public function __construct(catMetodoPagoRepository $catMetodoPagoRepo)
    {
        $this->catMetodoPagoRepository = $catMetodoPagoRepo;
    }

    /**
     * Display a listing of the catMetodoPago.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $catMetodoPagos = $this->catMetodoPagoRepository->all();

        return view('cat_metodo_pagos.index')
            ->with('catMetodoPagos', $catMetodoPagos);
    }

    /**
     * Show the form for creating a new catMetodoPago.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_metodo_pagos.create');
    }

    /**
     * Store a newly created catMetodoPago in storage.
     *
     * @param CreatecatMetodoPagoRequest $request
     *
     * @return Response
     */
    public function store(CreatecatMetodoPagoRequest $request)
    {
        $input = $request->all();

        $catMetodoPago = $this->catMetodoPagoRepository->create($input);

        flash('Message')->success('Cat Metodo Pago añadido.');

        return redirect(route('catMetodoPagos.index'));
    }

    /**
     * Display the specified catMetodoPago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catMetodoPago = $this->catMetodoPagoRepository->find($id);

        if (empty($catMetodoPago)) {
            flash('Message')->error('Cat Metodo Pago not found');

            return redirect(route('catMetodoPagos.index'));
        }

        return view('cat_metodo_pagos.show')->with('catMetodoPago', $catMetodoPago);
    }

    /**
     * Show the form for editing the specified catMetodoPago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catMetodoPago = $this->catMetodoPagoRepository->find($id);

        if (empty($catMetodoPago)) {
            flash('Message')->error('Cat Metodo Pago not found');

            return redirect(route('catMetodoPagos.index'));
        }

        return view('cat_metodo_pagos.edit')->with('catMetodoPago', $catMetodoPago);
    }

    /**
     * Update the specified catMetodoPago in storage.
     *
     * @param int $id
     * @param UpdatecatMetodoPagoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecatMetodoPagoRequest $request)
    {
        $catMetodoPago = $this->catMetodoPagoRepository->find($id);

        if (empty($catMetodoPago)) {
            flash('Message')->error('Cat Metodo Pago not found');

            return redirect(route('catMetodoPagos.index'));
        }

        $catMetodoPago = $this->catMetodoPagoRepository->update($request->all(), $id);

        flash('Message')->success('Cat Metodo Pago actualizado.');

        return redirect(route('catMetodoPagos.index'));
    }

    /**
     * Remove the specified catMetodoPago from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catMetodoPago = $this->catMetodoPagoRepository->find($id);

        if (empty($catMetodoPago)) {
            flash('Message')->error('Cat Metodo Pago not found');

            return redirect(route('catMetodoPagos.index'));
        }

        $this->catMetodoPagoRepository->delete($id);

        flash('Message')->success('Cat Metodo Pago eliminado.');

        return redirect(route('catMetodoPagos.index'));
    }
}
