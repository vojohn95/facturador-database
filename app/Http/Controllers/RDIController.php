<?php

namespace App\Http\Controllers;

use App\RDI;
use App\Exports\RdiExport;
use App\Models\Estacionamiento;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class RDIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $est = Estacionamiento::select('no_est', 'nombre')->get();

        return view('rdi.index')->with('estacionamientos', $est);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return (new RdiExport($request->estacionamiento, $request->fecha))->download('RDI.xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RDI  $rDI
     * @return \Illuminate\Http\Response
     */
    public function show(RDI $rDI)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RDI  $rDI
     * @return \Illuminate\Http\Response
     */
    public function edit(RDI $rDI)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RDI  $rDI
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RDI $rDI)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RDI  $rDI
     * @return \Illuminate\Http\Response
     */
    public function destroy(RDI $rDI)
    {
        //
    }
}
