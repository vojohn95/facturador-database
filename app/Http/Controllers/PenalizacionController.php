<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use Illuminate\Http\Request;
use App\Models\Estacionamiento;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PenalizacionRepository;
use App\Http\Requests\CreatePenalizacionRequest;
use App\Http\Requests\UpdatePenalizacionRequest;

class PenalizacionController extends AppBaseController
{
    /** @var  PenalizacionRepository */
    private $penalizacionRepository;

    public function __construct(PenalizacionRepository $penalizacionRepo)
    {
        $this->penalizacionRepository = $penalizacionRepo;
    }

    /**
     * Display a listing of the Penalizacion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $penalizacions = $this->penalizacionRepository->all();

        return view('penalizacions.index')
            ->with('penalizacions', $penalizacions);
    }

    /**
     * Show the form for creating a new Penalizacion.
     *
     * @return Response
     */
    public function create()
    {
        $est = Estacionamiento::all();

        return view('penalizacions.create')->with('ests', $est);
    }

    /**
     * Store a newly created Penalizacion in storage.
     *
     * @param CreatePenalizacionRequest $request
     *
     * @return Response
     */
    public function store(CreatePenalizacionRequest $request)
    {
        $input = $request->all();

        $penalizacion = $this->penalizacionRepository->create($input);

        flash('Message')->success('Penalizacion añadida.');

        return redirect(route('penalizacions.index'));
    }

    /**
     * Display the specified Penalizacion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $penalizacion = $this->penalizacionRepository->find($id);

        if (empty($penalizacion)) {
            flash('Message')->error('Penalizacion not found');

            return redirect(route('penalizacions.index'));
        }

        return view('penalizacions.show')->with('penalizacion', $penalizacion);
    }

    /**
     * Show the form for editing the specified Penalizacion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $penalizacion = $this->penalizacionRepository->find($id);

        if (empty($penalizacion)) {
            flash('Message')->error('Penalizacion not found');

            return redirect(route('penalizacions.index'));
        }

        $est = Estacionamiento::all();

        return view('penalizacions.edit')->with('penalizacion', $penalizacion)->with('ests', $est);
    }

    /**
     * Update the specified Penalizacion in storage.
     *
     * @param int $id
     * @param UpdatePenalizacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePenalizacionRequest $request)
    {
        $penalizacion = $this->penalizacionRepository->find($id);

        if (empty($penalizacion)) {
            flash('Message')->error('Penalizacion not found');

            return redirect(route('penalizacions.index'));
        }

        $penalizacion = $this->penalizacionRepository->update($request->all(), $id);

        flash('Message')->success('Penalizacion actualizada.');

        return redirect(route('penalizacions.index'));
    }

    /**
     * Remove the specified Penalizacion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $penalizacion = $this->penalizacionRepository->find($id);

        if (empty($penalizacion)) {
            flash('Message')->error('Penalizacion not found');

            return redirect(route('penalizacions.index'));
        }

        $this->penalizacionRepository->delete($id);

        flash('Message')->success('Penalizacion eliminada.');

        return redirect(route('penalizacions.index'));
    }
}
