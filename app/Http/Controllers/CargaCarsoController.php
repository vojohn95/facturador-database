<?php

namespace App\Http\Controllers;

use App\CargaCarso;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use App\Models\ReporteIngreso;

class CargaCarsoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargaCarso = ReporteIngreso::orderBy('id', 'DESC')->paginate(30);
        return view('cargacarso.index')->with('cargaCarsos', $cargaCarso);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cargacarso.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $cargaCarso = ReporteIngreso::create($request->all());


        flash('Message')->success('Se cargo correctamente la información.');

        return redirect(route('cargacarso.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CargaCarso  $cargaCarso
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        dd($request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CargaCarso  $cargaCarso
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $reporte = ReporteIngreso::find($request->id);

        if (empty($reporte)) {
            flash('Message')->error('Reporte not found');

            return redirect(route('cargacarso.index'));
        }

        return view('cargacarso.edit')->with('cargocarsos', $reporte);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CargaCarso  $cargaCarso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CargaCarso $cargaCarso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CargaCarso  $cargaCarso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        dd($request->all());
        $cargaCarso = ReporteIngreso::find($request->id);
        $cargaCarso->delete();
        flash('Message')->error('Se elimino correctamente la información.');
        return redirect(route('cargacarso.index'));
    }
}
