<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCatConceptoRequest;
use App\Http\Requests\UpdateCatConceptoRequest;
use App\Repositories\CatConceptoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CatConceptoController extends AppBaseController
{
    /** @var  CatConceptoRepository */
    private $catConceptoRepository;

    public function __construct(CatConceptoRepository $catConceptoRepo)
    {
        $this->catConceptoRepository = $catConceptoRepo;
    }

    /**
     * Display a listing of the CatConcepto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $catConceptos = $this->catConceptoRepository->all();

        return view('cat_conceptos.index')
            ->with('catConceptos', $catConceptos);
    }

    /**
     * Show the form for creating a new CatConcepto.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_conceptos.create');
    }

    /**
     * Store a newly created CatConcepto in storage.
     *
     * @param CreateCatConceptoRequest $request
     *
     * @return Response
     */
    public function store(CreateCatConceptoRequest $request)
    {
        $input = $request->all();

        $catConcepto = $this->catConceptoRepository->create($input);

        flash('Message')->success('Cat Concepto añadido.');

        return redirect(route('catConceptos.index'));
    }

    /**
     * Display the specified CatConcepto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catConcepto = $this->catConceptoRepository->find($id);

        if (empty($catConcepto)) {
            flash('Message')->error('Cat Concepto not found');

            return redirect(route('catConceptos.index'));
        }

        return view('cat_conceptos.show')->with('catConcepto', $catConcepto);
    }

    /**
     * Show the form for editing the specified CatConcepto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catConcepto = $this->catConceptoRepository->find($id);

        if (empty($catConcepto)) {
            flash('Message')->error('Cat Concepto not found');

            return redirect(route('catConceptos.index'));
        }

        return view('cat_conceptos.edit')->with('catConcepto', $catConcepto);
    }

    /**
     * Update the specified CatConcepto in storage.
     *
     * @param int $id
     * @param UpdateCatConceptoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCatConceptoRequest $request)
    {
        $catConcepto = $this->catConceptoRepository->find($id);

        if (empty($catConcepto)) {
            flash('Message')->error('Cat Concepto not found');

            return redirect(route('catConceptos.index'));
        }

        $catConcepto = $this->catConceptoRepository->update($request->all(), $id);

        flash('Message')->success('Cat Concepto actualizado.');

        return redirect(route('catConceptos.index'));
    }

    /**
     * Remove the specified CatConcepto from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catConcepto = $this->catConceptoRepository->find($id);

        if (empty($catConcepto)) {
            flash('Message')->error('Cat Concepto not found');

            return redirect(route('catConceptos.index'));
        }

        $this->catConceptoRepository->delete($id);

        flash('Message')->success('Cat Concepto eliminado.');

        return redirect(route('catConceptos.index'));
    }
}
