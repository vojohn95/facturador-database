<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Pago;
use App\Models\Pension;
use App\Models\catBancos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\PagoRepository;
use App\Http\Requests\CreatePagoRequest;
use App\Http\Requests\UpdatePagoRequest;
use App\Http\Controllers\AppBaseController;

class PagoController extends AppBaseController
{
    /** @var  PagoRepository */
    private $pagoRepository;

    public function __construct(PagoRepository $pagoRepo)
    {
        $this->pagoRepository = $pagoRepo;
    }

    /**
     * Display a listing of the Pago.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pagos = $this->pagoRepository->all();

        return view('pagos.index')
            ->with('pagos', $pagos);
    }

    /**
     * Show the form for creating a new Pago.
     *
     * @return Response
     */
    public function create()
    {
        $pensiones = Pension::get();
        $bancos = catBancos::get();
        $formapagos = DB::connection('mysql2')->table('cat_formaPago')
        ->select('*')
        ->get();

        $metodopagos = DB::connection('mysql2')->table('cat_Metodopago')
        ->select('*')
        ->get();

        return view('pagos.create')->with('pensiones', $pensiones)->with('bancos', $bancos)
        ->with('formaPagos', $formapagos)
        ->with('metodoPagos', $metodopagos);
    }

    /**
     * Store a newly created Pago in storage.
     *
     * @param CreatePagoRequest $request
     *
     * @return Response
     */
    public function store(CreatePagoRequest $request)
    {
        $input = $request->all();

        $comprobante_pago = '';
        $comprobante_pago = request()->file('comprobante_pago');
        if ($comprobante_pago != null) {
            $datacont = $comprobante_pago->get();
            $nombre_contrato = $comprobante_pago->getBasename();
            $cont = file_get_contents($comprobante_pago);
            $contdata = base64_encode($cont);
            $input['comprobante_pago'] = $contdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pago = $this->pagoRepository->create($input);

        flash('Message')->success('Pago añadido satisfactoriamente.');

        return redirect(route('pagos.index'));
    }

    /**
     * Display the specified Pago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            flash('Message')->error('Pago not found');

            return redirect(route('pagos.index'));
        }

        return view('pagos.show')->with('pago', $pago);
    }

    /**
     * Show the form for editing the specified Pago.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            flash('Message')->error('Pago not found');

            return redirect(route('pagos.index'));
        }

        $pensiones = Pension::get();
        $bancos = catBancos::get();
        $formapagos = DB::connection('mysql2')->table('cat_formaPago')
        ->select('*')
        ->get();

        $metodopagos = DB::connection('mysql2')->table('cat_Metodopago')
        ->select('*')
        ->get();

        return view('pagos.edit')->with('pago', $pago)->with('pensiones', $pensiones)->with('bancos', $bancos)
        ->with('idPension', $pago->idPension)->with('id_bancos', $pago->id_bancos)->with('formaPagos', $formapagos)
        ->with('metodoPagos', $metodopagos)
        ->with('id_metodoPago', $pago->id_metodoPago)
        ->with('id_formaPago', $pago->id_formaPago);
    }

    /**
     * Update the specified Pago in storage.
     *
     * @param int $id
     * @param UpdatePagoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePagoRequest $request)
    {
        $input = request()->all();
        $pago = Pago::find($id);

        if (empty($pago)) {
            flash('Message')->error('Pago not found');

            return redirect(route('pagos.index'));
        }

        if($input['total']){
            $pago->total = $input['total'];
        }

        /*if($input['subtotal']){
            $pago->subtotal = $input['subtotal'];
        }

        if($input['iva']){
            $pago->iva = $input['iva'];
        }*/

        if($input['id_formaPago']){
            $pago->id_formaPago = $input['id_formaPago'];
        }

        if($input['tipoPago']){
            $pago->tipoPago = $input['tipoPago'];
        }

        if($input['idPension']){
            $pago->idPension = $input['idPension'];
        }

        if($input['fechaPago']){
            $pago->fechaPago = $input['fechaPago'];
        }

        if($input['id_bancos']){
            $pago->id_bancos = $input['id_bancos'];
        }

        if($input['mes']){
            $pago->mes = $input['mes'];
        }

        if($input['año']){
            $pago->año = $input['año'];
        }

        $comprobantePago = '';
        $comprobantePago = request()->file('comprobante_pago');
        if ($comprobantePago != null) {
            $datascomp = $comprobantePago->get();
            $nombre_cdomici = $comprobantePago->getBasename();
            $cdom = file_get_contents($comprobantePago);
            $cdomdata = base64_encode($cdom);
            $pago->comprobante_pago = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pago->save();

        flash('Message')->success('Pago actualizado satisfactoriamente.');

        return redirect(route('pagos.index'));
    }

    /**
     * Remove the specified Pago from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pago = $this->pagoRepository->find($id);

        if (empty($pago)) {
            flash('Message')->error('Pago not found');

            return redirect(route('pagos.index'));
        }

        $this->pagoRepository->delete($id);

        flash('Message')->success('Pago eliminado satisfactoriamente.');

        return redirect(route('pagos.index'));
    }

    public function downloadcompPago($id){
        $pdf = Pago::find($id);
        $archivo = $pdf->comprobante_pago;
        $decoded = base64_decode($archivo);
        $path = "pdfs/comprobante_pago".$pdf->id.".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=comprobante_pago-$pdf->id.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }
}
