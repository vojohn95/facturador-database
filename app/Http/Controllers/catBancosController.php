<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecatBancosRequest;
use App\Http\Requests\UpdatecatBancosRequest;
use App\Repositories\catBancosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class catBancosController extends AppBaseController
{
    /** @var  catBancosRepository */
    private $catBancosRepository;

    public function __construct(catBancosRepository $catBancosRepo)
    {
        $this->catBancosRepository = $catBancosRepo;
    }

    /**
     * Display a listing of the catBancos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $catBancos = $this->catBancosRepository->all();

        return view('cat_bancos.index')
            ->with('catBancos', $catBancos);
    }

    /**
     * Show the form for creating a new catBancos.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_bancos.create');
    }

    /**
     * Store a newly created catBancos in storage.
     *
     * @param CreatecatBancosRequest $request
     *
     * @return Response
     */
    public function store(CreatecatBancosRequest $request)
    {
        $input = $request->all();

        $catBancos = $this->catBancosRepository->create($input);

        flash('Message')->success('Banco añadido satisfatoriamente.');

        return redirect(route('catBancos.index'));
    }

    /**
     * Display the specified catBancos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catBancos = $this->catBancosRepository->find($id);

        if (empty($catBancos)) {
            flash('Message')->error('Cat Bancos not found');

            return redirect(route('catBancos.index'));
        }

        return view('cat_bancos.show')->with('catBancos', $catBancos);
    }

    /**
     * Show the form for editing the specified catBancos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catBancos = $this->catBancosRepository->find($id);

        if (empty($catBancos)) {
            flash('Message')->error('Cat Bancos not found');

            return redirect(route('catBancos.index'));
        }

        return view('cat_bancos.edit')->with('catBancos', $catBancos);
    }

    /**
     * Update the specified catBancos in storage.
     *
     * @param int $id
     * @param UpdatecatBancosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecatBancosRequest $request)
    {
        $catBancos = $this->catBancosRepository->find($id);

        if (empty($catBancos)) {
            flash('Message')->error('Cat Bancos not found');

            return redirect(route('catBancos.index'));
        }

        $catBancos = $this->catBancosRepository->update($request->all(), $id);

        flash('Message')->success('Banco actualizado.');

        return redirect(route('catBancos.index'));
    }

    /**
     * Remove the specified catBancos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catBancos = $this->catBancosRepository->find($id);

        if (empty($catBancos)) {
            flash('Message')->error('Cat Bancos not found');

            return redirect(route('catBancos.index'));
        }

        $this->catBancosRepository->delete($id);

        flash('Message')->success('Banco eliminado.');

        return redirect(route('catBancos.index'));
    }
}
