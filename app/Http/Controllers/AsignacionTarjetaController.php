<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAsignacionTarjetaRequest;
use App\Http\Requests\UpdateAsignacionTarjetaRequest;
use App\Models\EstacionamientoPension;
use App\Repositories\AsignacionTarjetaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AsignacionTarjetaController extends AppBaseController
{
    /** @var  AsignacionTarjetaRepository */
    private $asignacionTarjetaRepository;

    public function __construct(AsignacionTarjetaRepository $asignacionTarjetaRepo)
    {
        $this->asignacionTarjetaRepository = $asignacionTarjetaRepo;
    }

    /**
     * Display a listing of the AsignacionTarjeta.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $asignacionTarjetas = $this->asignacionTarjetaRepository->all();

        return view('asignacion_tarjetas.index')
            ->with('asignacionTarjetas', $asignacionTarjetas);
    }

    /**
     * Show the form for creating a new AsignacionTarjeta.
     *
     * @return Response
     */
    public function create()
    {
        $proyecto = EstacionamientoPension::all();
        //dd($proyecto);

        return view('asignacion_tarjetas.create')->with('proyectos', $proyecto);
    }

    /**
     * Store a newly created AsignacionTarjeta in storage.
     *
     * @param CreateAsignacionTarjetaRequest $request
     *
     * @return Response
     */
    public function store(CreateAsignacionTarjetaRequest $request)
    {
        $input = $request->all();

        $asignacionTarjeta = $this->asignacionTarjetaRepository->create($input);

        flash('Message')->success('Asignacion Tarjeta añadido.');

        return redirect(route('asignacionTarjetas.index'));
    }

    /**
     * Display the specified AsignacionTarjeta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $asignacionTarjeta = $this->asignacionTarjetaRepository->find($id);

        if (empty($asignacionTarjeta)) {
            flash('Message')->error('Asignacion Tarjeta not found');

            return redirect(route('asignacionTarjetas.index'));
        }

        return view('asignacion_tarjetas.show')->with('asignacionTarjeta', $asignacionTarjeta);
    }

    /**
     * Show the form for editing the specified AsignacionTarjeta.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $asignacionTarjeta = $this->asignacionTarjetaRepository->find($id);
        //dd($asignacionTarjeta);

        if (empty($asignacionTarjeta)) {
            flash('Message')::error('Asignacion Tarjeta not found');

            return redirect(route('asignacionTarjetas.index'));
        }

        $proyecto = EstacionamientoPension::all();

        return view('asignacion_tarjetas.edit')->with('asignacionTarjeta', $asignacionTarjeta)->with('proyectos', $proyecto);
    }

    /**
     * Update the specified AsignacionTarjeta in storage.
     *
     * @param int $id
     * @param UpdateAsignacionTarjetaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAsignacionTarjetaRequest $request)
    {
        $asignacionTarjeta = $this->asignacionTarjetaRepository->find($id);

        if (empty($asignacionTarjeta)) {
            flash('Message')::error('Asignacion Tarjeta not found');

            return redirect(route('asignacionTarjetas.index'));
        }

        $asignacionTarjeta = $this->asignacionTarjetaRepository->update($request->all(), $id);

        flash('Message')::success('Asignacion Tarjeta actualizado.');

        return redirect(route('asignacionTarjetas.index'));
    }

    /**
     * Remove the specified AsignacionTarjeta from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $asignacionTarjeta = $this->asignacionTarjetaRepository->find($id);

        if (empty($asignacionTarjeta)) {
            flash('Message')::error('Asignacion Tarjeta not found');

            return redirect(route('asignacionTarjetas.index'));
        }

        $this->asignacionTarjetaRepository->delete($id);

        flash('Message')::success('Asignacion Tarjeta eliminado.');

        return redirect(route('asignacionTarjetas.index'));
    }
}
