<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\Models\Permiso;

class DatatableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function centralusertable()
    {
        if (request()->ajax()) {
            $centralUsers = DB::table('central_users')
                ->join('roles', 'central_users.id', '=', 'roles.id_central_user')
                ->join('permisos', 'roles.id_permiso', '=', 'permisos.id')
                ->select('central_users.id', 'central_users.name', 'central_users.email', 'permisos.nombre')
                ->get();

            return DataTables()::of($centralUsers)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . route('central_users.edit', [$row->id]) . '" class="btn btn-default btn-sm white-text" onclick="return confirm(\'¿Estas seguro?\')">visualizar</a>';
                    $btn .= '<form action="' . route('central_users.destroy', $row->id) . '" method="POST" style="display: inline">';
                    $btn .= '<input type="hidden" name="_method" value="DELETE">';
                    $btn .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
                    $btn .= '<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(\'¿Estas seguro?\')">Eliminar</button>';
                    $btn .= '</form>';
                    $btn .= ' &nbsp;&nbsp;';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function permisostable()
    {
        if (request()->ajax()) {
            $permisos = Permiso::all();

            return DataTables()::of($permisos)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . route('central_users.edit', [$row->id]) . '" class="btn btn-default btn-sm white-text" onclick="return confirm(\'¿Estas seguro?\')">visualizar</a>';
                    $btn .= '<form action="' . route('central_users.destroy', $row->id) . '" method="POST" style="display: inline">';
                    $btn .= '<input type="hidden" name="_method" value="DELETE">';
                    $btn .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
                    $btn .= '<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(\'¿Estas seguro?\')">Eliminar</button>';
                    $btn .= '</form>';
                    $btn .= ' &nbsp;&nbsp;';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function estacionamientotable()
    {
        if (request()->ajax()) {
            $estacionamientos = DB::table('parks')
                ->join('marcas', 'parks.id_marca', '=', 'marcas.id')
                ->select('parks.no_est', 'parks.nombre', 'parks.dist_regio', 'parks.cp', 'parks.escuela', 'marcas.marca', 'parks.Facturable', 'parks.Automatico')
                ->where('no_est', '!=', 1)
                ->paginate(250);

            return DataTables()::of($estacionamientos)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . route('central_users.edit', [$row->id]) . '" class="btn btn-default btn-sm white-text" onclick="return confirm(\'¿Estas seguro?\')">visualizar</a>';
                    $btn .= '<form action="' . route('central_users.destroy', $row->id) . '" method="POST" style="display: inline">';
                    $btn .= '<input type="hidden" name="_method" value="DELETE">';
                    $btn .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
                    $btn .= '<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(\'¿Estas seguro?\')">Eliminar</button>';
                    $btn .= '</form>';
                    $btn .= ' &nbsp;&nbsp;';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
