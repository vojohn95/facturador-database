<?php

namespace App\Http\Controllers;

use Excel;
use App\Reporte;
use App\ReportePension;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\PensionActivasExport;
use App\Exports\PensionInActivasExport;
use App\Exports\PensionPagosAplicados;

class ReportePensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reportepension.index');
    }

    public function pagosMesAplicados(Request $request){
        $input = request()->all();
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
        $date = $date->format('d-m-Y');

        $file_name = "Reporte pagos aplicados-" . $date . ".xlsx";
        return (new PensionPagosAplicados($input['mes'], $input['año']))->download($file_name);
    }

    public function ReporteActivos(Request $request){
        $input = request()->all();
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
        $date = $date->format('d-m-Y');

        $file_name = "Reporte activos-" . $date . ".xlsx";
        return (new PensionActivasExport())->download($file_name);
    }

    public function ReporteInActivos(Request $request){
        $input = request()->all();
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
        $date = $date->format('d-m-Y');

        $file_name = "Reporte inactivos-" . $date . ".xlsx";
        return (new PensionInActivasExport())->download($file_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReportePension  $reportePension
     * @return \Illuminate\Http\Response
     */
    public function show(ReportePension $reportePension)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReportePension  $reportePension
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportePension $reportePension)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportePension  $reportePension
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportePension $reportePension)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReportePension  $reportePension
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportePension $reportePension)
    {
        //
    }
}
