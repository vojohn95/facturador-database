<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Pension;
use App\Models\CatConcepto;
use Illuminate\Http\Request;
use App\Models\EstacionamientoPension;
use App\Repositories\PensionRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreatePensionRequest;
use App\Http\Requests\UpdatePensionRequest;

class PensionController extends AppBaseController
{
    /** @var  PensionRepository */
    private $pensionRepository;

    public function __construct(PensionRepository $pensionRepo)
    {
        $this->pensionRepository = $pensionRepo;
    }

    /**
     * Display a listing of the Pension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pensions = $this->pensionRepository->all();

        return view('pensions.index')
            ->with('pensions', $pensions);
    }

    /**
     * Show the form for creating a new Pension.
     *
     * @return Response
     */
    public function create()
    {
        $tipopen = CatConcepto::all();
        $proyecto = EstacionamientoPension::all();

        return view('pensions.create')->with('proyectos', $proyecto);
    }

    /**
     * Store a newly created Pension in storage.
     *
     * @param CreatePensionRequest $request
     *
     * @return Response
     */
    public function store(CreatePensionRequest $request)
    {
        $input = $request->all();

        $contrato = '';
        $Solicitudcontrato = '';
        $comprobanteDomicilio = '';
        $ine = '';
        $licencia = '';
        $tarjetaCirculacion = '';

        $contrato = request()->file('contrato');
        if ($contrato != null) {
            $datacont = $contrato->get();
            $nombre_contrato = $contrato->getBasename();
            $cont = file_get_contents($contrato);
            $contdata = base64_encode($cont);
            $input['contrato'] = $contdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $Solicitudcontrato = request()->file('solicitudContrato');
        if ($Solicitudcontrato != null) {
            $datascont = $Solicitudcontrato->get();
            $nombre_scontrato = $Solicitudcontrato->getBasename();
            $scont = file_get_contents($Solicitudcontrato);
            $scontdata = base64_encode($scont);
            $input['solicitudContrato'] = $scontdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $comprobanteDomicilio = request()->file('comprobanteDomicilio');
        if ($comprobanteDomicilio != null) {
            $datascomp = $comprobanteDomicilio->get();
            $nombre_cdomici = $comprobanteDomicilio->getBasename();
            $cdom = file_get_contents($comprobanteDomicilio);
            $cdomdata = base64_encode($cdom);
            $input['comprobanteDomicilio'] = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $ine = request()->file('ine');
        if ($ine != null) {
            $datasine = $ine->get();
            $nombre_ine = $ine->getBasename();
            $cine = file_get_contents($ine);
            $cinedata = base64_encode($cine);
            $input['ine'] = $cinedata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $licencia = request()->file('licencia');
        if ($licencia != null) {
            $dataslic = $licencia->get();
            $nombre_lic = $licencia->getBasename();
            $line = file_get_contents($licencia);
            $linedata = base64_encode($line);
            $input['licencia'] = $linedata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $tarjetaCirculacion = request()->file('tarjetaCirculacion');
        if ($tarjetaCirculacion != null) {
            $datastar = $tarjetaCirculacion->get();
            $nombre_tar = $tarjetaCirculacion->getBasename();
            $tarin = file_get_contents($tarjetaCirculacion);
            $tarindata = base64_encode($tarin);
            $input['tarjetaCirculacion'] = $tarindata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pension = $this->pensionRepository->create($input);

        //dd("contrato");
        flash('Message')->success('Pensión añadida satisfactoriamente.');

        return redirect(route('pensions.index'));
    }

    /**
     * Display the specified Pension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            flash('Message')->error('Pension not found');

            return redirect(route('pensions.index'));
        }

        return view('pensions.show')->with('pension', $pension);
    }

    /**
     * Show the form for editing the specified Pension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            flash('Message')->error('Pension not found');

            return redirect(route('pensions.index'));
        }

        $tipopen = CatConcepto::all();
        $proyecto = EstacionamientoPension::all();

        return view('pensions.edit')->with('pension', $pension)->with('proyectos', $proyecto);
    }

    /**
     * Update the specified Pension in storage.
     *
     * @param int $id
     * @param UpdatePensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePensionRequest $request)
    {
        $input = request()->all();
        $pension = Pension::find($id);

        if (empty($pension)) {
            flash('Message')->error('Pension not found');

            return redirect(route('pensions.index'));
        }

        if($input['montoPension']){
            $pension->montoPension = $input['montoPension'];
        }

        if($input['tipoPension']){
            $pension->tipoPension = $input['tipoPension'];
        }

        if($input['fecha_limite']){
            $pension->fecha_limite = $input['fecha_limite'];
        }

        if($input['id_estacionamiento']){
            $pension->id_estacionamiento = $input['id_estacionamiento'];
        }

        if($input['rfc']){
            $pension->rfc = $input['rfc'];
        }

        if($input['status']){
            $pension->status = $input['status'];
        }

        if($input['noTarjeta']){
            $pension->noTarjeta = $input['noTarjeta'];
        }

        $contrato = '';
        $Solicitudcontrato = '';
        $comprobanteDomicilio = '';
        $ine = '';
        $licencia = '';
        $tarjetaCirculacion = '';

        $contrato = request()->file('contrato');
        if ($contrato != null) {
            $datacont = $contrato->get();
            $nombre_contrato = $contrato->getBasename();
            $cont = file_get_contents($contrato);
            $contdata = base64_encode($cont);
            $pension->contrato = $contdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $Solicitudcontrato = request()->file('solicitudContrato');
        if ($Solicitudcontrato != null) {
            $datascont = $Solicitudcontrato->get();
            $nombre_scontrato = $Solicitudcontrato->getBasename();
            $scont = file_get_contents($Solicitudcontrato);
            $scontdata = base64_encode($scont);
            $pension->solicitudContrato = $scontdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $comprobanteDomicilio = request()->file('comprobanteDomicilio');
        if ($comprobanteDomicilio != null) {
            $datascomp = $comprobanteDomicilio->get();
            $nombre_cdomici = $comprobanteDomicilio->getBasename();
            $cdom = file_get_contents($comprobanteDomicilio);
            $cdomdata = base64_encode($cdom);
            $pension->comprobanteDomicilio = $cdomdata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $ine = request()->file('ine');
        if ($ine != null) {
            $datasine = $ine->get();
            $nombre_ine = $ine->getBasename();
            $cine = file_get_contents($ine);
            $cinedata = base64_encode($cine);
            $pension->ine = $cinedata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $licencia = request()->file('licencia');
        if ($licencia != null) {
            $dataslic = $licencia->get();
            $nombre_lic = $licencia->getBasename();
            $line = file_get_contents($licencia);
            $linedata = base64_encode($line);
            $pension->licencia = $linedata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $tarjetaCirculacion = request()->file('tarjetaCirculacion');
        if ($tarjetaCirculacion != null) {
            $datastar = $tarjetaCirculacion->get();
            $nombre_tar = $tarjetaCirculacion->getBasename();
            $tarin = file_get_contents($tarjetaCirculacion);
            $tarindata = base64_encode($tarin);
            $pension->tarjetaCirculacion = $tarindata;
            //$contrato = $this->pensionRepository->create($input);
        }

        $pension->save();

        flash('Message')->success('Pensión actualizada satisfactoriamente.');

        return redirect(route('pensions.index'));
    }

    /**
     * Remove the specified Pension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pension = $this->pensionRepository->find($id);

        if (empty($pension)) {
            flash('Message')->error('Pension not found');

            return redirect(route('pensions.index'));
        }

        $this->pensionRepository->delete($id);

        flash('Message')->success('Pensión eliminada satisfactoriamente.');

        return redirect(route('pensions.index'));
    }

    public function downloadContPdf($id){
        $pdf = Pension::find($id);
        $archivo = $pdf->contrato;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=contrato-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadSContPdf($id){
        $pdf = Pension::find($id);
        $archivo = $pdf->solicitudContrato;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=solicitudcontrato-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadComprobantedomPdf($id){
        $pdf = Pension::find($id);
        $archivo = $pdf->comprobanteDomicilio;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=comprobantedomicilio-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadIne($id){
        $pdf = Pension::find($id);
        $archivo = $pdf->ine;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=ine-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadLic($id){
        $pdf = Pension::find($id);
        $archivo = $pdf->licencia;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=licencia-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

    public function downloadTarC($id){
        $pdf = Pension::find($id);
        $archivo = $pdf->tarjetaCirculacion;
        $notarjeta = $pdf->noTarjeta;
        $decoded = base64_decode($archivo);
        $path = "pdfs/" . $notarjeta . ".pdf";
        file_put_contents($path, $decoded);
        header("Content-disposition: attachment; filename=tarjetacirculacion-$notarjeta.pdf");
        header("Content-type: application/pdf");
        readfile($path);
        unlink($path);
    }

}
