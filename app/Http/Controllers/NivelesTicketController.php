<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNivelesTicketRequest;
use App\Http\Requests\UpdateNivelesTicketRequest;
use App\Repositories\NivelesTicketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class NivelesTicketController extends AppBaseController
{
    /** @var  NivelesTicketRepository */
    private $nivelesTicketRepository;

    public function __construct(NivelesTicketRepository $nivelesTicketRepo)
    {
        $this->nivelesTicketRepository = $nivelesTicketRepo;
    }

    /**
     * Display a listing of the NivelesTicket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $nivelesTickets = $this->nivelesTicketRepository->all();

        return view('niveles_tickets.index')
            ->with('nivelesTickets', $nivelesTickets);
    }

    /**
     * Show the form for creating a new NivelesTicket.
     *
     * @return Response
     */
    public function create()
    {
        return view('niveles_tickets.create');
    }

    /**
     * Store a newly created NivelesTicket in storage.
     *
     * @param CreateNivelesTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateNivelesTicketRequest $request)
    {
        $input = $request->all();

        $nivelesTicket = $this->nivelesTicketRepository->create($input);

        flash('Message')->success('Niveles Ticket saved successfully.');

        return redirect(route('nivelesTickets.index'));
    }

    /**
     * Display the specified NivelesTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nivelesTicket = $this->nivelesTicketRepository->find($id);

        if (empty($nivelesTicket)) {
            flash('Message')->error('Niveles Ticket not found');

            return redirect(route('nivelesTickets.index'));
        }

        return view('niveles_tickets.show')->with('nivelesTicket', $nivelesTicket);
    }

    /**
     * Show the form for editing the specified NivelesTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nivelesTicket = $this->nivelesTicketRepository->find($id);

        if (empty($nivelesTicket)) {
            flash('Message')->error('Niveles Ticket not found');

            return redirect(route('nivelesTickets.index'));
        }

        return view('niveles_tickets.edit')->with('nivelesTicket', $nivelesTicket);
    }

    /**
     * Update the specified NivelesTicket in storage.
     *
     * @param int $id
     * @param UpdateNivelesTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNivelesTicketRequest $request)
    {
        $nivelesTicket = $this->nivelesTicketRepository->find($id);

        if (empty($nivelesTicket)) {
            flash('Message')->error('Niveles Ticket not found');

            return redirect(route('nivelesTickets.index'));
        }

        $nivelesTicket = $this->nivelesTicketRepository->update($request->all(), $id);

        flash('Message')->success('Niveles Ticket updated successfully.');

        return redirect(route('nivelesTickets.index'));
    }

    /**
     * Remove the specified NivelesTicket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $nivelesTicket = $this->nivelesTicketRepository->find($id);

        if (empty($nivelesTicket)) {
            flash('Message')->error('Niveles Ticket not found');

            return redirect(route('nivelesTickets.index'));
        }

        $this->nivelesTicketRepository->delete($id);

        flash('Message')->success('Niveles Ticket deleted successfully.');

        return redirect(route('nivelesTickets.index'));
    }
}
