<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstacionamientoPensionRequest;
use App\Http\Requests\UpdateEstacionamientoPensionRequest;
use App\Repositories\EstacionamientoPensionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EstacionamientoPensionController extends AppBaseController
{
    /** @var  EstacionamientoPensionRepository */
    private $estacionamientoPensionRepository;

    public function __construct(EstacionamientoPensionRepository $estacionamientoPensionRepo)
    {
        $this->estacionamientoPensionRepository = $estacionamientoPensionRepo;
    }

    /**
     * Display a listing of the EstacionamientoPension.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $estacionamientoPensions = $this->estacionamientoPensionRepository->all();

        return view('estacionamiento_pensions.index')
            ->with('estacionamientoPensions', $estacionamientoPensions);
    }

    /**
     * Show the form for creating a new EstacionamientoPension.
     *
     * @return Response
     */
    public function create()
    {
        return view('estacionamiento_pensions.create');
    }

    /**
     * Store a newly created EstacionamientoPension in storage.
     *
     * @param CreateEstacionamientoPensionRequest $request
     *
     * @return Response
     */
    public function store(CreateEstacionamientoPensionRequest $request)
    {
        $input = $request->all();

        $estacionamientoPension = $this->estacionamientoPensionRepository->create($input);

        flash('Message')->success('Estacionamiento Pension saved successfully.');

        return redirect(route('estacionamientoPensions.index'));
    }

    /**
     * Display the specified EstacionamientoPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estacionamientoPension = $this->estacionamientoPensionRepository->find($id);

        if (empty($estacionamientoPension)) {
            flash('Message')->error('Estacionamiento Pension not found');

            return redirect(route('estacionamientoPensions.index'));
        }

        return view('estacionamiento_pensions.show')->with('estacionamientoPension', $estacionamientoPension);
    }

    /**
     * Show the form for editing the specified EstacionamientoPension.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estacionamientoPension = $this->estacionamientoPensionRepository->find($id);

        if (empty($estacionamientoPension)) {
            flash('Message')->error('Estacionamiento Pension not found');

            return redirect(route('estacionamientoPensions.index'));
        }

        return view('estacionamiento_pensions.edit')->with('estacionamientoPension', $estacionamientoPension);
    }

    /**
     * Update the specified EstacionamientoPension in storage.
     *
     * @param int $id
     * @param UpdateEstacionamientoPensionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstacionamientoPensionRequest $request)
    {
        $estacionamientoPension = $this->estacionamientoPensionRepository->find($id);

        if (empty($estacionamientoPension)) {
            flash('Message')->error('Estacionamiento Pension not found');

            return redirect(route('estacionamientoPensions.index'));
        }

        $estacionamientoPension = $this->estacionamientoPensionRepository->update($request->all(), $id);

        flash('Message')->success('Estacionamiento Pension updated successfully.');

        return redirect(route('estacionamientoPensions.index'));
    }

    /**
     * Remove the specified EstacionamientoPension from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estacionamientoPension = $this->estacionamientoPensionRepository->find($id);

        if (empty($estacionamientoPension)) {
            flash('Message')->error('Estacionamiento Pension not found');

            return redirect(route('estacionamientoPensions.index'));
        }

        $this->estacionamientoPensionRepository->delete($id);

        flash('Message')->success('Estacionamiento Pension deleted successfully.');

        return redirect(route('estacionamientoPensions.index'));
    }
}
