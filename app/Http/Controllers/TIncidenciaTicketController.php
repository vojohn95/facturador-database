<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTIncidenciaTicketRequest;
use App\Http\Requests\UpdateTIncidenciaTicketRequest;
use App\Repositories\TIncidenciaTicketRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TIncidenciaTicketController extends AppBaseController
{
    /** @var  TIncidenciaTicketRepository */
    private $tIncidenciaTicketRepository;

    public function __construct(TIncidenciaTicketRepository $tIncidenciaTicketRepo)
    {
        $this->tIncidenciaTicketRepository = $tIncidenciaTicketRepo;
    }

    /**
     * Display a listing of the TIncidenciaTicket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tIncidenciaTickets = $this->tIncidenciaTicketRepository->all();

        return view('t_incidencia_tickets.index')
            ->with('tIncidenciaTickets', $tIncidenciaTickets);
    }

    /**
     * Show the form for creating a new TIncidenciaTicket.
     *
     * @return Response
     */
    public function create()
    {
        return view('t_incidencia_tickets.create');
    }

    /**
     * Store a newly created TIncidenciaTicket in storage.
     *
     * @param CreateTIncidenciaTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTIncidenciaTicketRequest $request)
    {
        $input = $request->all();

        $tIncidenciaTicket = $this->tIncidenciaTicketRepository->create($input);

        flash('Message')->success('T Incidencia Ticket saved successfully.');

        return redirect(route('tIncidenciaTickets.index'));
    }

    /**
     * Display the specified TIncidenciaTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tIncidenciaTicket = $this->tIncidenciaTicketRepository->find($id);

        if (empty($tIncidenciaTicket)) {
            flash('Message')->error('T Incidencia Ticket not found');

            return redirect(route('tIncidenciaTickets.index'));
        }

        return view('t_incidencia_tickets.show')->with('tIncidenciaTicket', $tIncidenciaTicket);
    }

    /**
     * Show the form for editing the specified TIncidenciaTicket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tIncidenciaTicket = $this->tIncidenciaTicketRepository->find($id);

        if (empty($tIncidenciaTicket)) {
            flash('Message')->error('T Incidencia Ticket not found');

            return redirect(route('tIncidenciaTickets.index'));
        }

        return view('t_incidencia_tickets.edit')->with('tIncidenciaTicket', $tIncidenciaTicket);
    }

    /**
     * Update the specified TIncidenciaTicket in storage.
     *
     * @param int $id
     * @param UpdateTIncidenciaTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTIncidenciaTicketRequest $request)
    {
        $tIncidenciaTicket = $this->tIncidenciaTicketRepository->find($id);

        if (empty($tIncidenciaTicket)) {
            flash('Message')->error('T Incidencia Ticket not found');

            return redirect(route('tIncidenciaTickets.index'));
        }

        $tIncidenciaTicket = $this->tIncidenciaTicketRepository->update($request->all(), $id);

        flash('Message')->success('T Incidencia Ticket updated successfully.');

        return redirect(route('tIncidenciaTickets.index'));
    }

    /**
     * Remove the specified TIncidenciaTicket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tIncidenciaTicket = $this->tIncidenciaTicketRepository->find($id);

        if (empty($tIncidenciaTicket)) {
            flash('Message')->error('T Incidencia Ticket not found');

            return redirect(route('tIncidenciaTickets.index'));
        }

        $this->tIncidenciaTicketRepository->delete($id);

        flash('Message')->success('T Incidencia Ticket deleted successfully.');

        return redirect(route('tIncidenciaTickets.index'));
    }
}
