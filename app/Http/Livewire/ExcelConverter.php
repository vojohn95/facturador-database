<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Exports\FacturaExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Svg\Gradient\Stop;

class ExcelConverter extends Component
{
    use WithFileUploads, LivewireAlert;

    public $files = [];
    public $totalfiles = 0, $iteration;

    protected $listeners = ['refrescar' => 'refresh'];

    public function refresh()
    {
        $this->files = [];
        $this->reset(['files']);
    }

    public function submit()
    {
        $validatedData = $this->validate([
            'files.*' => 'required|mimes:xml',
        ]);

        foreach ($this->files as $file) {
            $file->store('public');
        }
        $this->alert('success', 'Archivos cargados correctamente');
    }

    public function generarExcel()
    {
        $path = Storage::allFiles('public');


        if (count($path) < 1) {
            $this->alert('warning', 'No se encontraron xml cargados');
        } else {
            $array_r = [];
            $array_sumado = [];
            for ($i = 0; $i < count($path); $i++) {
                $factura_file = public_path('storage/' . substr($path[$i], 7));

                // crear el objeto CFDI
                $cfdi = \CfdiUtils\Cfdi::newFromString(
                    file_get_contents($factura_file)
                );
                // obtener el QuickReader con el método dedicado
                $comprobante = $cfdi->getQuickReader();
                $serie = $comprobante['serie'];
                $folio = $comprobante['folio'];
                $uuid = $comprobante->complemento->timbreFiscalDigital['uuid'];
                $fecha_timbrado = $comprobante->complemento->timbreFiscalDigital['fechatimbrado'];

                $hijos = ($comprobante->conceptos)('concepto');

                foreach ($hijos as $key => $hijo) {
                    array_push($array_r, [
                        'Importe' => $hijo['Importe'],
                        'Descripcion' => $hijo['Descripcion'],
                        'NoIdentificacion' => $hijo['NoIdentificacion'],
                        'ClaveProdServ' => $hijo['ClaveProdServ'],
                        'Serie' => $serie,
                        'Folio' => $folio,
                        'UUID' => $uuid,
                        'Fecha_timbrado' => $fecha_timbrado,
                    ]);
                }
            }
            $date = Carbon::now();
            Excel::store(new FacturaExport($array_r), $date->format('Y-m-d') . '-report.xlsx', 'excel');
            $this->alert('success', 'Excel generado correctamente');
            $this->emit('refrescar');
            Storage::delete($path);
        }
    }

    public function reiniciarCarga()
    {
        $path = Storage::allFiles('public');
        if (count($path) <= 0) {
            Storage::delete($path);
            $this->files = rand();
            $this->alert('success', 'Carga restaurada');
        }
    }

    public function bajarfile($value)
    {
        $this->alert('success', 'Documento generado correctamente');
        return Storage::disk('excel')->download($value);
    }

    public function borrafile($value)
    {
        if (Storage::disk('excel')->exists($value)) {
            Storage::disk('excel')->delete($value);
            $this->alert('success', 'Reporte eliminado correctamente');
        } else {
            $this->alert('error', 'No se encontro archivo');
        }
    }

    public function render()
    {
        $this->totalfiles = count(Storage::allFiles('public'));
        return view('livewire.excel-converter');
    }
}
