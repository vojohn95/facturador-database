<?php

namespace App\Http\Livewire;

use App\Models\Ticket;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Estacionamiento;

class BuscadorTickets extends Component
{
    use WithPagination;

    public $no_ticket = '';
    public $email = '';
    public $RFC = '';
    public $Razon_social = '';
    public $selectedEst = null; // Add this property for filtering by Estacionamiento

    public function render()
    {
        $tickets = $this->search();

        $est = Estacionamiento::select('no_est', 'nombre')->get();

        return view('livewire.buscador-tickets', ['tickets' => $tickets, 'est' => $est]);
    }

    public function search()
    {
        $query = Ticket::select('autof_tickets.id', 'autof_tickets.id_cliente', 'autof_tickets.id_est', 'autof_tickets.id_tipo', 'autof_tickets.id_CentralUser', 'autof_tickets.factura', 'autof_tickets.no_ticket', 'autof_tickets.total_ticket', 'autof_tickets.fecha_emision', 'autof_tickets.estatus', 'autof_tickets.UsoCFDI', 'autof_tickets.metodo_pago', 'autof_tickets.forma_pago', 'autof_tickets.RFC', 'autof_tickets.Razon_social', 'autof_tickets.email', 'parks.nombre as park', 'autof_tickets.cp', 'autof_tickets.id_regimen', 'catalogo_regimen.codigoRegimen', 'catalogo_regimen.descripcion', 'autof_tickets.created_at')
            ->join('parks', 'parks.no_est', '=', 'autof_tickets.id_est')
            ->leftJoin('catalogo_regimen', 'catalogo_regimen.codigoRegimen', '=', 'autof_tickets.id_regimen')
            ->where(function ($q) {
                $q->where('estatus', 'validar')
                    ->orWhere('estatus', 'sin_fact');
            })
            ->whereYear('autof_tickets.created_at', '>=', 2023);

        if ($this->no_ticket) {
            $query->where('no_ticket', 'LIKE', '%' . trim($this->no_ticket) . '%');
        }

        if ($this->Razon_social) {
            $query->where('Razon_social', 'LIKE', '%' . trim($this->Razon_social) . '%');
        }

        if ($this->email) {
            $query->where('email', 'LIKE', '%' . trim($this->email) . '%');
        }

        if ($this->RFC) {
            $query->where('RFC', 'LIKE', '%' . trim($this->RFC) . '%');
        }

        if ($this->selectedEst) {
            $query->where('autof_tickets.id_est', $this->selectedEst);
        }

        return $query->orderBy('id', 'desc')->fastPaginate();
    }

    public function updated()
    {
        $this->resetPage();
    }
}
