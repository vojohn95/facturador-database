<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EstatusComponent extends Component
{
    public function render()
    {
        return view('livewire.estatus-component');
    }
}
