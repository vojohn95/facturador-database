FROM php:8.2-fpm-alpine

# Add docker-php-extension-installer script
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

# Install dependencies
RUN apk add --no-cache \
    bash \
    curl \
    freetype-dev \
    g++ \
    gcc \
    git \
    icu-dev \
    icu-libs \
    libc-dev \
    libzip-dev \
    make \
    mysql-client \
    # nodejs \
    # npm \
    oniguruma-dev \
    yarn \
    openssh-client \
    postgresql-libs \
    rsync \
    zlib-dev \
    sudo \
    zip \
    unzip \
    libsodium-dev \
    dcron

# Install php extensions
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions \
    @composer \
    redis-stable \
    imagick-stable \
    #xdebug-stable \
    bcmath \
    calendar \
    exif \
    gd \
    intl \
    pdo_mysql \
    pdo_pgsql \
    pcntl \
    soap \
    zip \
    apcu \
    opcache \
    sockets \
    sodium \
    xsl

# Copy custom php.ini
COPY php.ini /usr/local/etc/php/php.ini

# Copy custom www.conf
COPY www.conf /usr/local/etc/php-fpm.d/www.conf

RUN chown -R www-data:www-data /var/www \
    && chmod 755 -R /var/www

# # Add cron job to schedule Laravel tasks
# RUN echo "0 * * * * php /var/www/html/artisan schedule:run >> /dev/null 2>&1" | crontab -

# # Link cron log file to stdout
# RUN ln -s /dev/stdout /var/log/cron

# # Run the cron service in the foreground
# CMD [ "crond", "-f", "-d", "8" ]

# Healthcheck
HEALTHCHECK --interval=15m --timeout=3s \
    CMD curl --fail http://localhost/ || exit 1

