@extends('layouts.app')
@section('title', 'Estacionamientos')
@section('content')
    @foreach($estacionamiento as $estacionamientos)
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h4 class="card-header-title">{!! $estacionamientos->nombre !!}</h4>
            </div>
        </div>
        <hr>

        @include('flash::message')
        <section class="content" id="divid">
        @include('layouts.errors')
        @include('flash::message')
        <!--graficas-->
            <div class="container">
                <!--Tabla de estacionamiento-->
                <div class="row">
                    <div class="col">
                        <div class="table-responsive ">
                            <div class="form-group pull-right">
                                <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
                            </div>
                            <table class="table table-hover table-bordered results responsive-table text-center"
                                   id="dtBasicExample">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>RFC</th>
                                    <th>Razón social</th>
                                    <th>Total</th>
                                    <th>Fecha de recepción</th>
                                    <th>Estatus</th>
                                    <th>Foto</th>
                                </tr>
                                <tr class="warning no-result">
                                    <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i>
                                        No se encontro
                                        registro con la información ingresada
                                    </td>
                                </tr>
                                </thead>
                                @foreach($ticket as $tickets)
                                    <tbody>
                                    <tr>
                                        <td>{!! $tickets->id !!}</td>
                                        <td>{!! $tickets->RFC !!}</td>
                                        <td>{!! $tickets->Razon_social !!}</td>
                                        <td>{!! $tickets->total_ticket !!}</td>
                                        <td>{!! $tickets->fecha_emision!!}</td>
                                        <td>
                                            @if($tickets->estatus == "valido")
                                                <a type="button"
                                                   class="btn-floating btn-sm btn-success material-tooltip-main"
                                                   data-toggle="tooltip"
                                                   data-placement="bottom" title="Timbrado"><i class="fas fa-check"></i></a>
                                            @elseif($tickets->estatus == "validar")
                                                <a type="button"
                                                   class="btn-floating btn-sm btn-info material-tooltip-main"
                                                   data-toggle="tooltip"
                                                   data-placement="bottom" title="Pendiente de aprobación"><i
                                                            class="fas fa-exclamation"></i></a>
                                            @else
                                                <a type="button"
                                                   class="btn-floating btn-sm btn-danger material-tooltip-main"
                                                   data-toggle="tooltip"
                                                   data-placement="bottom" title="Rechazado"><i
                                                            class="fas fa-times"></i></a>
                                            @endif
                                        </td>
                                        <td>
                                            <a type="button" class="btn-floating btn-sm default-color"
                                               data-toggle="modal"
                                               data-target="#basicExampleModal{{$tickets->id}}"><i
                                                        class="far fa-file-image"></i></a>

                                            <!-- Modal -->
                                            <div class="modal fade" id="basicExampleModal{{$tickets->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="exampleModalLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body view overlay zoom">
                                                            <img src="data:image/jpg;base64,{{$tickets->imagen}}"
                                                                 style="width: 100%;">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Cerrar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            {{$ticket->links()}}

                        </div>
                        <a type="button" class="btn default-color white-text" href="{{route('estacionamientos.index')}}">Volver</a>
                    </div>

                </div>
            </div>
            <!--graficas-->
            <br><br>
            @endforeach
            <br>
            <br>
        </section>
@endsection


