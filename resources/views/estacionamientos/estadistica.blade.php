@extends('layouts.app')
@section('title', 'Estacionamientos')
@section('content')
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h4 class="card-header-title">{!! $estacionamiento->nombre !!}</h4>
            </div>
        </div>
        <hr>
        @include('flash::message')
        <section class="content" id="divid">
        @include('layouts.errors')
        @include('flash::message')
        <!--graficas-->
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating indigo"><i class="fas fa-ticket-alt"
                                                                                        aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 18px;">Tickets: <span class="badge badge-pill badge-info"
                                                                                   style="font-size: 18px;">{!! $tickets !!}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-check-circle"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Facturado: <span
                                                    class="badge badge-pill badge-secondary"
                                                    style="font-size: 16px;">${!! $facturado !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-barcode"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Facturas: <span
                                                    class="badge badge-pill badge-success"
                                                    style="font-size: 16px;">{!! $facturas !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-window-close"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 18px;">Rechazadas: <span
                                                    class="badge badge-pill badge-danger"
                                                    style="font-size: 18px;">{!! $rechazos !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h5 class="card-header-title">Top 5 clientes</h5>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <ul class="list-group">
                                    @foreach($rfc as $RFC)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            {!! $RFC->RFC !!}
                                            <span class="badge badge-default badge-pill">{!! $RFC->fact !!}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h5 class="card-header-title">Facturado</h5>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="pieChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </section>
        <a type="button" class="btn btn-default" href="{{route('estacionamientos.index')}}">Volver</a>
            <br><br>
            <br>
            <br><br>

@endsection

<script>
    @push('scripts')
    //pie
    var datos = [{!! $rechazo !!},{!! $TotalGrafica !!},{!! $factura !!}];
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Rechazos", "Facturas", "Tickets"],
            datasets: [{
                data: datos,
                backgroundColor: ["#F7464A", "#00C851", "#33b5e5"],
                hoverBackgroundColor: ["#FF5A5E", "#00C851", "#33b5e5"]
            }]
        },
        options: {
            responsive: true
        }
    });

    @endpush
</script>
