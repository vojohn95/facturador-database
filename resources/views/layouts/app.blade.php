<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Desarrollador: Luis Fernando Jonathan Vargas Osornio - vojohn95@gmail.com">
    <meta name="author" content="Desarrollador: Victor Rojas Barrera - victorrb1015@gmail.com">
    <meta name="Version" content="produccion">
    <title>@yield('title', 'OCE')</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('logo/logo/login-logo.png') }}" />
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
    <!--Responsive Extension Datatables CSS-->
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <style>
        #loading {
            position: fixed;
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
            text-align: center;
            opacity: 0.8;
            background-color: #fff;
            z-index: 990000;
        }

        #loading-image {
            position: absolute;
            z-index: 100;
        }
    </style>
    <div id="loading">
        <img id="loading-image" src="https://cdn.dribbble.com/users/778626/screenshots/4339853/car-middle.gif"
            alt="Loading..." />
    </div>
    @livewireStyles

</head>

<style>
    body {
        padding: 20px 20px;
    }

    .results tr[visible='false'],
    .no-result {
        display: none;
    }

    .results tr[visible='true'] {
        display: table-row;
    }

    .counter {
        padding: 8px;
        color: white;
    }
</style>

@guest
    @include('layouts.navs.nav')
@else
    @include('layouts.navs.nav_dev')
@endguest

<body style="padding-top: 90px;">
    <main class="py-4">
        <div class="container">

            @yield('content')

        </div>
    </main>
    <br>
    <footer class="page-footer font-small aqua-gradient  fixed-bottom">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© <?php echo date('Y'); ?> Copyright:
            <a href="https:central-mx.com" target="_blank"> Operadora Central de Estacionamientos SAPI de C.V.</a>
        </div>
        <!-- Copyright -->

    </footer>

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('logo/logo/login-logo.png') }}" />
    <link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fullcalendar.print.min.css') }}" rel="stylesheet" media='print' />

    <!--<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--<script type="text/javascript">
        //script para mensajes falsh
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>-->
    <script>
        $(window).on('load', function() {
            $('#loading').hide();
        })
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(8000).fadeOut(3000);
    </script>
    @stack('maps')
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/lazysizes.min.js') }}" async=""></script>
    <link href="{{ asset('css/yearpicker.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/yearpicker.js') }}"></script>
    <!--Tabla con busqueda-->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>


    {{--  --}}
    <style>
        .dataTables_wrapper .dataTables_filter {
            float: left;
            align: left;
            text-align: left;
        }
    </style>
    @yield('datatable')
    <!--Tabla con busqueda-->
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".search").keyup(function() {
                var searchTerm = $(".search").val();
                var listItem = $('.results tbody').children('tr');
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
                $.extend($.expr[':'], {
                    'containsi': function(elem, i, match, array) {
                        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf(
                            (match[3] || "").toLowerCase()) >= 0;
                    }
                });
                $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e) {
                    $(this).attr('visible', 'false');
                });
                $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e) {
                    $(this).attr('visible', 'true');
                });

                var jobCount = $('.results tbody tr[visible="true"]').length;
                $('.counter').text(jobCount + ' item');

                if (jobCount == '0') {
                    $('.no-result').show();
                } else {
                    $('.no-result').hide();
                }
            });
        });
    </script>
    <script>
        @stack('scripts')
    </script>

    <script>
        // Material Select Initialization
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });
    </script>

    @livewireScripts
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <x-livewire-alert::scripts />
</body>

</html>
