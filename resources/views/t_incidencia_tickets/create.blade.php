@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            T Incidencia Ticket
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tIncidenciaTickets.store']) !!}

                        @include('t_incidencia_tickets.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
