<div class="table-responsive">
    <table class="table" id="tIncidenciaTickets-table">
        <thead>
            <tr>
                <th>Nombre Incidencia</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tIncidenciaTickets as $tIncidenciaTicket)
            <tr>
                <td>{{ $tIncidenciaTicket->nombre_incidencia }}</td>
                <td>
                    {!! Form::open(['route' => ['tIncidenciaTickets.destroy', $tIncidenciaTicket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('tIncidenciaTickets.show', [$tIncidenciaTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('tIncidenciaTickets.edit', [$tIncidenciaTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
