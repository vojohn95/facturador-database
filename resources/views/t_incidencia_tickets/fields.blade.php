<!-- Nombre Incidencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_incidencia', 'Nombre Incidencia:') !!}
    {!! Form::text('nombre_incidencia', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('tIncidenciaTickets.index') }}" class="btn btn-default">Cancel</a>
</div>
