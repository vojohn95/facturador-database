<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Asunto</th>
                <th>Responsable</th>
                <th>Fecha</th>
                <th>Firma</th>
            </tr>
        </thead>
        <tbody>
            @foreach($firmas as $firma)
                <tr>
                    <td>{!! $firma->nombre !!}</td>
                    <td>{!! $firma->tipo !!}</td>
                    <td>{!! $firma->asunto !!}</td>
                    <td>{!! $firma->responsable !!}</td>
                    <td>{!! $firma->created_at !!}</td>
                    <td>
                        <a type="button" class="btn-floating btn-sm default-color" data-toggle="modal"
                           data-target="#basicExampleModal{{$firma->id}}"><i class="far fa-file-image"></i></a>

                        <!-- Modal -->

                        <div class="modal fade" id="basicExampleModal{{$firma->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body view overlay zoom">
                                        <img src="data:image/*;charset=utf-8;base64,{{$firma->imagen}}" style="width: 100%;">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
