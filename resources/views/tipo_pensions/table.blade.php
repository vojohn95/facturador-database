<div class="table-responsive">
    <table class="table" id="tipoPensions-table">
        <thead>
            <tr>
                <th>Tipo</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tipoPensions as $tipoPension)
            <tr>
                <td>{!! $tipoPension->tipo !!}</td>
                <td>
                    {!! Form::open(['route' => ['tipoPensions.destroy', $tipoPension->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('tipoPensions.show', [$tipoPension->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('tipoPensions.edit', [$tipoPension->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
