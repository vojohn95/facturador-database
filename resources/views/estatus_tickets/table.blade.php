<div class="table-responsive">
    <table class="table" id="estatusTickets-table">
        <thead>
            <tr>
                <th>Nombre Estatus</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($estatusTickets as $estatusTicket)
            <tr>
                <td>{{ $estatusTicket->nombre_estatus }}</td>
                <td>
                    {!! Form::open(['route' => ['estatusTickets.destroy', $estatusTicket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('estatusTickets.show', [$estatusTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('estatusTickets.edit', [$estatusTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
