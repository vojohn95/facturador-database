<!-- Nombre Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_estatus', 'Nombre Estatus:') !!}
    {!! Form::text('nombre_estatus', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('estatusTickets.index') }}" class="btn btn-default">Cancel</a>
</div>
