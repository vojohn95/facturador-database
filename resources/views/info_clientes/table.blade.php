<div class="table-responsive">
    <table class="table" id="infoClientes-table">
        <thead>
            <tr>
                <th>Id Cliente</th>
        <th>Rfc</th>
        <th>Razon Social</th>
        <th>Calle</th>
        <th>No Ext</th>
        <th>No Int</th>
        <th>Colonia</th>
        <th>Municipio</th>
        <th>Estado</th>
        <th>Pais</th>
        <th>Cp</th>
        <th>Act Na</th>
        <th>Ine</th>
        <th>Tar Circ</th>
        <th>Placas</th>
        <th>Tel</th>
        <th>Pension</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($infoClientes as $infoCliente)
            <tr>
                <td>{!! $infoCliente->id_cliente !!}</td>
            <td>{!! $infoCliente->RFC !!}</td>
            <td>{!! $infoCliente->Razon_social !!}</td>
            <td>{!! $infoCliente->calle !!}</td>
            <td>{!! $infoCliente->no_ext !!}</td>
            <td>{!! $infoCliente->no_int !!}</td>
            <td>{!! $infoCliente->colonia !!}</td>
            <td>{!! $infoCliente->municipio !!}</td>
            <td>{!! $infoCliente->estado !!}</td>
            <td>{!! $infoCliente->pais !!}</td>
            <td>{!! $infoCliente->cp !!}</td>
            <td>{!! $infoCliente->act_na !!}</td>
            <td>{!! $infoCliente->ine !!}</td>
            <td>{!! $infoCliente->tar_circ !!}</td>
            <td>{!! $infoCliente->placas !!}</td>
            <td>{!! $infoCliente->tel !!}</td>
            <td>{!! $infoCliente->pension !!}</td>
                <td>
                    {!! Form::open(['route' => ['infoClientes.destroy', $infoCliente->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('infoClientes.show', [$infoCliente->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('infoClientes.edit', [$infoCliente->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
