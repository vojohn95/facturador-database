<!-- Id Cliente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_cliente', 'Id Cliente:') !!}
    {!! Form::number('id_cliente', null, ['class' => 'form-control']) !!}
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('RFC', 'Rfc:') !!}
    {!! Form::text('RFC', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Social Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    {!! Form::text('Razon_social', null, ['class' => 'form-control']) !!}
</div>

<!-- Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calle', 'Calle:') !!}
    {!! Form::text('calle', null, ['class' => 'form-control']) !!}
</div>

<!-- No Ext Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_ext', 'No Ext:') !!}
    {!! Form::text('no_ext', null, ['class' => 'form-control']) !!}
</div>

<!-- No Int Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_int', 'No Int:') !!}
    {!! Form::text('no_int', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('municipio', 'Municipio:') !!}
    {!! Form::text('municipio', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Pais Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pais', 'Pais:') !!}
    {!! Form::text('pais', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cp', 'Cp:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Act Na Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('act_na', 'Act Na:') !!}
    {!! Form::textarea('act_na', null, ['class' => 'form-control']) !!}
</div>

<!-- Ine Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ine', 'Ine:') !!}
    {!! Form::textarea('ine', null, ['class' => 'form-control']) !!}
</div>

<!-- Tar Circ Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tar_circ', 'Tar Circ:') !!}
    {!! Form::textarea('tar_circ', null, ['class' => 'form-control']) !!}
</div>

<!-- Placas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placas', 'Placas:') !!}
    {!! Form::text('placas', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel', 'Tel:') !!}
    {!! Form::text('tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Pension Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pension', 'Pension:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('pension', 0) !!}
        {!! Form::checkbox('pension', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('infoClientes.index') !!}" class="btn btn-default">Cancel</a>
</div>
