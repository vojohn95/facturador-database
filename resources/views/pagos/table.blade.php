<div class="table-responsive">
    <table class="table" id="dtBasicExample">
        <thead>
            <tr>
                <th>Total</th>
                <!--<th>Subtotal</th>
                <th>Iva</th>-->
                <th>Tipo pago</th>
                <th>Pension</th>
                <th>Fecha pago</th>
                <th>Banco</th>
                <th>Mes</th>
                <th>Año</th>
                <th>Forma pago</th>
                <th>Comprobante Pago</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pagos as $pago)
                <tr>
                    <td>{{ $pago->total }}</td>
                    <!--<td>{{ $pago->subtotal }}</td>
                    <td>{{ $pago->iva }}</td>-->
                    <td>{{ $pago->tipoPago }}</td>
                    <td>{{ $pago->idPension }}</td>
                    <td>{{ $pago->fechaPago }}</td>
                    <td>{{ $pago->id_bancos }}</td>
                    <td>{{ $pago->mes }}</td>
                    <td>{{ $pago->año }}</td>
                    <td>{{ $pago->id_metodoPago }}</td>
                    <td>
                        @if ($pago->comprobante_pago)
                            <a href="{!! route('compPago', [$pago->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['route' => ['pagos.destroy', $pago->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('pagos.show', [$pago->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('pagos.edit', [$pago->id]) }}"
                                class='btn btn-default btn-xs'>Editar</a>
                            {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
