<!-- Total Field -->
<div class="form-group col-sm-12">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
{{-- <div class="form-group col-sm-12">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::number('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Iva Field -->
<div class="form-group col-sm-12">
    {!! Form::label('iva', 'Iva:') !!}
    {!! Form::number('iva', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Tipopago Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tipoPago', 'Tipo pago:') !!}
    {!! Form::text('tipoPago', null, ['class' => 'form-control', 'maxlength' => 25, 'maxlength' => 25]) !!}
</div>

<!-- Idpension Field -->
<div class="form-group col-sm-12">
    {!! Form::label('idPension', 'Pension:') !!}
    <select class="browser-default custom-select" name="idPension">
        @if ($pensiones)
            @foreach ($pensiones as $pensioness)
                <option value="{{ $pensioness->id }}"
                    {{ $pensioness->id == $idPension ? 'selected' : '' }}>
                    {{ $pensioness->noTarjeta }}</option>
            @endforeach
        @else
            <option value="">No hay pensiones registrados</option>
        @endif
    </select>
</div>

<div class="col-12">
    {!! Form::label('formaPago', 'Forma pago:') !!}
    <select class="mdb-select colorful-select dropdown-default md-form"
            searchable="Buscar aquí.." id="formaPago" name="id_formaPago"
            required>
        <option value="">Elija su forma de pago*</option>
        @foreach($formaPagos as $formaPago)
            <option value="{{$formaPago->id}}" {{ $formaPago->id == $id_formaPago ? 'selected' : '' }}>
                {{$formaPago->claveSat}} / {{$formaPago->descricion}}</option>
        @endforeach
    </select>
</div>

<div class="col-12">
    {!! Form::label('metodoPago', 'Metodo pago:') !!}
    <select class="mdb-select colorful-select dropdown-default md-form"
            searchable="Buscar aquí.." id="metodoPago" name="id_metodoPago"
            required>
        <option value="">Elija su metodo de pago*</option>
        @foreach($metodoPagos as $metodoPago)
            <option value="{{$metodoPago->id}}" {{ $metodoPago->id == $id_metodoPago ? 'selected' : '' }}>
                {{$metodoPago->clave}} / {{$metodoPago->Descripcion}}</option>
        @endforeach
    </select>
</div>

<!-- Fechapago Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fechaPago', 'Fecha pago:') !!}
    {!! Form::date('fechaPago', null, ['class' => 'form-control', 'id' => 'fechaPago']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fechaPago').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })

    </script>
@endpush

<!-- Id Bancos Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_bancos', 'Banco:') !!}
    <select class="browser-default custom-select" name="id_bancos">
        @if ($bancos)
            @foreach ($bancos as $bancoss)
                <option value="{{ $bancoss->id }}"
                    {{ $bancoss->id == $id_bancos ? 'selected' : '' }}>
                    {{ $bancoss->banco }}</option>
            @endforeach
        @else
            <option value="">No hay proveedores registrados</option>
        @endif
    </select>
</div>

<!-- Mes Field -->
<div class="form-group col-sm-12">
    {!! Form::label('mes', 'Mes:') !!}
    <select class="browser-default custom-select" name="mes">
        <option value="">Elija un mes</option>
        <option value="Enero">Enero</option>
        <option value="Febrero">Febrero</option>
        <option value="Marzo">Marzo</option>
        <option value="Abril">Abril</option>
        <option value="Mayo">Mayo</option>
        <option value="Junio">Junio</option>
        <option value="Julio">Julio</option>
        <option value="Agosto">Agosto</option>
        <option value="Septiembre">Septiembre</option>
        <option value="Octubre">Octubre</option>
        <option value="Octubre">Noviembre</option>
        <option value="Octubre">Diciembre</option>
    </select>
</div>

<!-- Año Field -->
<div class="form-group col-sm-12">
    {!! Form::label('año', 'Año:') !!}
    {!! Form::number('año', null, ['class' => 'form-control']) !!}
</div>

<!-- Comprobante Pago Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comprobante_pago', 'Comprobante Pago:') !!}
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Cargar comprobante</span>
            <input type="file" name="comprobante_pago" id="foto" accept="application/pdf">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate" name="comprobante_pago" id="foto" accept="application/pdf" type="text"
                placeholder="Cargar comprobante">
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pagos.index') }}" class="btn btn-default">Cancelar</a>
</div>

<script>
    // Material Select Initialization
    $(document).ready(function() {
        $('.mdb-select').materialSelect();
    });

</script>
