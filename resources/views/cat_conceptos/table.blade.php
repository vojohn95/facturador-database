<div class="table-responsive">
    <table class="table" id="catConceptos-table">
        <thead>
            <tr>
                <th>Descripción</th>
                <th colspan="3">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($catConceptos as $catConcepto)
                <tr>
                    <td>{{ $catConcepto->DESCRIPCION }}</td>
                    <!--<td>{{ $catConcepto->TIPO }}</td>
                    <td>{{ $catConcepto->ID_MAT_ARTICULO }}</td>
                    <td>{{ $catConcepto->CLIENTE_ERP }}</td>
                    <td>{{ $catConcepto->FECHA_ALTA }}</td>
                    <td>{{ $catConcepto->FECHA_BAJA }}</td>
                    <td>{{ $catConcepto->ESTATUS }}</td>-->
                    <td>
                        {!! Form::open(['route' => ['catConceptos.destroy', $catConcepto->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('catConceptos.show', [$catConcepto->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('catConceptos.edit', [$catConcepto->id]) }}"
                                class='btn btn-default btn-xs'>EDITAR</a>
                            {!! Form::button('ELIMINAR', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
