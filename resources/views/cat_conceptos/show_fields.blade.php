<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('DESCRIPCION', 'Descripcion:') !!}
    <p>{{ $catConcepto->DESCRIPCION }}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('TIPO', 'Tipo:') !!}
    <p>{{ $catConcepto->TIPO }}</p>
</div>

<!-- Id Mat Articulo Field -->
<div class="form-group">
    {!! Form::label('ID_MAT_ARTICULO', 'Id Mat Articulo:') !!}
    <p>{{ $catConcepto->ID_MAT_ARTICULO }}</p>
</div>

<!-- Cliente Erp Field -->
<div class="form-group">
    {!! Form::label('CLIENTE_ERP', 'Cliente Erp:') !!}
    <p>{{ $catConcepto->CLIENTE_ERP }}</p>
</div>

<!-- Fecha Alta Field -->
<div class="form-group">
    {!! Form::label('FECHA_ALTA', 'Fecha Alta:') !!}
    <p>{{ $catConcepto->FECHA_ALTA }}</p>
</div>

<!-- Fecha Baja Field -->
<div class="form-group">
    {!! Form::label('FECHA_BAJA', 'Fecha Baja:') !!}
    <p>{{ $catConcepto->FECHA_BAJA }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('ESTATUS', 'Estatus:') !!}
    <p>{{ $catConcepto->ESTATUS }}</p>
</div>

