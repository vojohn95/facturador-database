<!-- Descripcion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DESCRIPCION', 'Descripcion:') !!}
    {!! Form::text('DESCRIPCION', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Tipo Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('TIPO', 'Tipo:') !!}
    {!! Form::text('TIPO', null, ['class' => 'form-control','maxlength' => 3,'maxlength' => 3]) !!}
</div>-->

<!-- Id Mat Articulo Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('ID_MAT_ARTICULO', 'Id Mat Articulo:') !!}
    {!! Form::number('ID_MAT_ARTICULO', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Cliente Erp Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('CLIENTE_ERP', 'Cliente Erp:') !!}
    {!! Form::text('CLIENTE_ERP', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>-->

<!-- Fecha Alta Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('FECHA_ALTA', 'Fecha Alta:') !!}
    {!! Form::date('FECHA_ALTA', null, ['class' => 'form-control','id'=>'FECHA_ALTA']) !!}
</div>-->

@push('scripts')
    <script type="text/javascript">
        $('#FECHA_ALTA').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fecha Baja Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('FECHA_BAJA', 'Fecha Baja:') !!}
    {!! Form::date('FECHA_BAJA', null, ['class' => 'form-control','id'=>'FECHA_BAJA']) !!}
</div>-->

@push('scripts')
    <script type="text/javascript">
        $('#FECHA_BAJA').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Estatus Field -->
<!--<div class="form-group col-sm-6">
    {!! Form::label('ESTATUS', 'Estatus:') !!}
    {!! Form::text('ESTATUS', null, ['class' => 'form-control','maxlength' => 3,'maxlength' => 3]) !!}
</div>-->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('catConceptos.index') }}" class="btn btn-default">Cancelar</a>
</div>
