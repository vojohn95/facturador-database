<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
            <tr>
                <th>ID</th>
                <th>Estacionamiento</th>
                <th>Fecha</th>
                <th>Ingreso Total</th>
                <th>Ingreso TH</th>
                <th>Factura Global</th>
                {{-- <th>Acción</th> --}}
            </tr>
            <tr class="warning no-result">
                <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                    registro con la información ingresada
                </td>
            </tr>
        </thead>
        <tbody>
            @foreach ($cargaCarsos as $cargacarso)
                <tr>
                    <td>{!! $cargacarso->id !!}</td>
                    <td>{!! $cargacarso->id_estacionamiento !!}</td>
                    <td>{!! date_format($cargacarso->fecha, 'd/m/y') !!}</td>
                    <td>{!! $cargacarso->ingreso_total !!}</td>
                    <td>{!! $cargacarso->ingreso_th !!}</td>
                    <td>{!! $cargacarso->factura_global !!}</td>
                    {{-- <td>
                        {!! Form::open(['route' => ['cargacarso.destroy', $cargacarso->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{!! route('cargacarso.edit', [$cargacarso->id]) !!}" class='btn btn-default btn-xs white-text'>Editar</a>
                            {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td> --}}
                </tr>

            @endforeach
        </tbody>
    </table>
</div>

<br>
<br>
