@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Añadir reporte {{ \Carbon\Carbon::now()->format('d-m-Y') }}</h3>
            </div>
        </div>
        <hr>
        @include('flash::message')
        {!! Form::open(['route' => 'cargacarso.store']) !!}
        <div class="md-form input-group">
            <select class="mdb-select colorful-select dropdown-default form-control" searchable="Buscar..."
                name="id_estacionamiento">
                <option value="0" selected disabled>Estacionamiento</option>
                <option value="413">Plaza Loreto</option>
                <option value="414">Plaza Insurgentes</option>
                <option value="938">Nuevo Veracruz</option>
            </select>
            <label class="mdb-main-label">Estacionamiento</label>
            {!! Form::date('fecha', \Carbon\Carbon::now()->format('d-m-Y'), ['class' => 'form-control', 'placeholder' => 'Fecha']) !!}
            {!! Form::number('ingreso_total', null, ['class' => 'form-control', 'placeholder' => 'Ingreso Total']) !!}
            {!! Form::number('ingreso_th', null, ['class' => 'form-control', 'placeholder' => 'Ingreso TH']) !!}
            {!! Form::number('factura_global', null, ['class' => 'form-control', 'placeholder' => 'Factura Global']) !!}
        </div>
        <div class="form-group col-sm-12">
            <button type="submit" value="Cargar" class="btn btn-primary" id="cargar">Registrar</button>
            <a href="{!! route('cargacarso.index') !!}" class="btn btn-default">Cancelar</a>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
