@extends('layouts.app')
@section('title', 'Carso')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Carga de reportes</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-8">

            </div>
            <div class="col-4 text-right">
                <a type="button" class="btn btn-dark btn-rounded" href="{{ route('cargacarso.create') }}">
                    Cargar reporte
                </a>
            </div>
        </div>
        <br>
        @include('cargacarso.table')
    </section>
@endsection
