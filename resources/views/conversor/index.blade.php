@extends('layouts.app')
@section('title', 'Conversor')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Conversor XML</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-8">
                @livewire('excel-converter')
            </div>
        </div>
        <br>
        {{-- @include('facturas.table') --}}
    </section>
@endsection
