<div>
    <table class="table" id="dtBasicExample">
        <thead>
            <tr>
                <th>Nombre</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        {{--  <tbody>
            @foreach ($permisos as $permiso)
                <tr>
                    <td>{!! $permiso->nombre !!}</td>
                    <td>
                        {!! Form::open(['route' => ['permisos.destroy', $permiso->id], 'method' => 'delete']) !!}

                        <a href="{!! route('permisos.edit', [$permiso->id]) !!}" class='btn btn-default btn-xs white-text'>visualizar</a>

                        {!! Form::button('Eliminar', [
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'onclick' => "return confirm('¿Estas seguro?')",
                        ]) !!}

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>  --}}
    </table>
</div>
<br>
@section('datatable')
    <script>
        $(document).ready(function() {
            $('#dtBasicExample').DataTable({
                "ajax" : {
                    "url" : "{{ route('datatable.permisos') }}",
                },
                "columns": [
                    { "data": "nombre" },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false
                    },
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                responsive: true,
                width: '100%',
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
@endsection
