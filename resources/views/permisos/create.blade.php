@extends('layouts.app')

@section('content')
    <section class="content-header">
        <section class="content">
            <div class="card card-cascade wilder">
                <!-- Card image -->
                <div class="view view-cascade gradient-card-header default-color">
                    <!-- Title -->
                    <h3 class="card-header-title">Permisos</h3>
                </div>
            </div>
            <hr>
            @include('layouts.errors')
            @include('flash::message')
            <div class="col-12">

                {!! Form::open(['route' => 'permisos.store']) !!}
                <div class="row justify-content-md-center">
                    <div class="col-md-8">
                        @include('permisos.fields')
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
    </section>
@endsection
