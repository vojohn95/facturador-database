@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h4 class="card-header-title">Estadisticas - {!! date('d/M/Y') !!}</h4>
            </div>
        </div>
        <hr>
        <div id="divid">
            @include('layouts.errors')
            @include('flash::message')
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating indigo"><i class="fas fa-ticket-alt"
                                                                                        aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 18px;">Tickets del dia: <span
                                                    class="badge badge-pill badge-info"
                                                    style="font-size: 18px;">{!! $tickets !!}</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-check-circle"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Facturado: <span
                                                    class="badge badge-pill badge-secondary"
                                                    style="font-size: 16px;">${!! $facturado !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-barcode"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Facturas del dia: <span
                                                    class="badge badge-pill badge-success"
                                                    style="font-size: 16px;">{!! $facturas !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-window-close"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 18px;">Rechazadas: <span
                                                    class="badge badge-pill badge-danger"
                                                    style="font-size: 18px;">{!! $rechazos !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--graficas-->
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Grafica del día</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="pieChart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con mas facturas</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                    <canvas id="barChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--graficas-->
            <br><br>
        </div>
        <hr>
        <!--Graficas del mes-->

        <!--Graficas-->
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <a type="button" class="btn-floating indigo"><i class="fas fa-ticket-alt"
                                                                                    aria-hidden="true"></i></a>
                                </div>
                                <div class="col">
                                    <p style="font-size: 18px;">Tickets del mes: <span
                                                class="badge badge-pill badge-info"
                                                style="font-size: 18px;">{!! $tickets !!}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <!-- Card Data -->
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <a type="button" class="btn-floating deep-purple"><i class="fas fa-check-circle"
                                                                                         aria-hidden="true"></i></a>
                                </div>
                                <div class="col">
                                    <p style="font-size: 16px;">Facturado: <span
                                                class="badge badge-pill badge-secondary"
                                                style="font-size: 16px;">${!! $facturado !!}</span></p>
                                </div>
                            </div>
                        </div>
                        <!-- Card Data -->
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <!-- Card Data -->
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <a type="button" class="btn-floating deep-purple"><i class="fas fa-barcode"
                                                                                         aria-hidden="true"></i></a>
                                </div>
                                <div class="col">
                                    <p style="font-size: 16px;">Facturas del dia: <span
                                                class="badge badge-pill badge-success"
                                                style="font-size: 16px;">{!! $facturas !!}</span></p>
                                </div>
                            </div>
                        </div>
                        <!-- Card Data -->
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <!-- Card Data -->
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <a type="button" class="btn-floating deep-purple"><i class="fas fa-window-close"
                                                                                         aria-hidden="true"></i></a>
                                </div>
                                <div class="col">
                                    <p style="font-size: 18px;">Rechazadas: <span
                                                class="badge badge-pill badge-danger"
                                                style="font-size: 18px;">{!! $rechazos !!}</span></p>
                                </div>
                            </div>
                        </div>
                        <!-- Card Data -->
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <div class="card card-cascade narrower">
                        <!-- Card image -->
                        <div class="view view-cascade gradient-card-header aqua-gradient">
                            <!-- Title -->
                            <h4 class="card-header-title">FACTURAS {!! date('Y') !!}</h4>
                        </div>
                        <!-- Card content -->
                        <div class="card-body card-body-cascade text-center">
                            <!-- Text -->
                            <canvas id="lineChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Graficas-->
        <br>
        <br>
    </section>
@endsection

<script>

    @push('scripts')
    //recarga de div
    function loadlink() {
        $('#divid').load('dashboard');
        console.log('TESTING!!!!');
    }

    setInterval(function () {
        loadlink()
    }, 1500000);

    //pie
    var datos = [{!! $rechazos !!},{!! $facturas !!},{!! $graf !!}];
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Rechazos", "Facturas", "Tickets"],
            datasets: [{
                data: datos,
                backgroundColor: ["#F7464A", "#00C851", "#33b5e5"],
                hoverBackgroundColor: ["#FF5A5E", "#00C851", "#33b5e5"]
            }]
        },
        options: {
            responsive: true
        }
    });

    //bar
    new Chart(document.getElementById("barChart"), {
        "type": "horizontalBar",
        "data": {
            "labels": [
                @php
                    if($est->count()){
                        foreach($est as $item){
                            echo '"'.$item->nombre.'",';
                        }
                    }
                @endphp
            ],
            "datasets": [{
                "label": "Facturas",
                "data": [
                    @php
                        if($est->count()){
                            foreach($est as $item){
                                echo $item->fact.',';
                            }
                        }
                    @endphp
                ],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });

    //line
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"],
            datasets: [{
                label: "Cantidad de tickets ingresados",
                data: [65, 59, 80, 81, 56, 55, 40],
                backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                    'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
                {
                    label: "Total de facturas timbradas",
                    data: [28, 48, 40, 19, 86, 27, 90],
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                }
            ]
        },
        options: {
            responsive: true
        }
    });
    @endpush
</script>
