@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h4 class="card-header-title">Facturación OCE</h4>
            </div>
        </div>
        <hr>
    </section>

    <ul class="nav md-pills nav-justified pills-rounded pills-default movimiento">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#dia" role="tab">Facturas del Día</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#mes" role="tab">Facturacion del Mes</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#fact" role="tab">Facturas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#est" role="tab">Estacionamientos</a>
        </li>
    </ul>

    <!-- Tab panels -->
    <div class="tab-content pt-0">

        <!--Dia-->
        <div class="tab-pane fade in show active" id="dia" role="tabpanel">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating indigo"><i class="fas fa-ticket-alt"
                                                                                        aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Tickets del dia: <span
                                                class="badge badge-pill badge-info"
                                                style="font-size: 16px;">{!! $tickets !!}</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col text-center">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-check-circle"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Facturado: <span
                                                class="badge badge-pill badge-secondary"
                                                style="font-size: 16px;">${!! number_format($facturado,2) !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-barcode"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Facturas: <span
                                                class="badge badge-pill badge-success"
                                                style="font-size: 16px;">{!! $facturas !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-window-close"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Rechazadas: <span
                                                class="badge badge-pill badge-danger"
                                                style="font-size: 16px;">{!! $rechazos !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--graficas-->
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Gráfica del día</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="pieChart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con mas facturas</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="barChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--graficas-->
            <br>
            <br>

        </div>
        <!--/.Dia-->

        <!--Mes-->
        <div class="tab-pane fade" id="mes" role="tabpanel">
            <br>

            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating indigo"><i class="fas fa-ticket-alt"
                                                                                        aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Tickets del mes: <span
                                                class="badge badge-pill badge-info"
                                                style="font-size: 16px;">{!! $tickets_mes !!}</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col text-center">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-check-circle"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col">
                                        <p style="font-size: 16px;">Facturado: <span
                                                class="badge badge-pill badge-secondary"
                                                style="font-size: 16px;">${!! number_format($facturado_mes,2) !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-barcode"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Facturas: <span
                                                class="badge badge-pill badge-success"
                                                style="font-size: 16px;">{!! $facturas_mes !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- Card Data -->
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a type="button" class="btn-floating deep-purple"><i class="fas fa-window-close"
                                                                                             aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col text-center">
                                        <p style="font-size: 16px;">Rechazadas: <span
                                                class="badge badge-pill badge-danger"
                                                style="font-size: 16px;">{!! $rechazo_mes !!}</span></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!--graficas-->
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Gráfica del Mes</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="pieChart_mes"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con mas facturas</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="barChart_mes"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--graficas-->
            <br>
            <br>
        </div>
        <!--/.Mes-->

        <!--Facturas-->
        <div class="tab-pane fade" id="fact" role="tabpanel">
            <br>

            <div class="row">
                <div class="col">
                    <div class="card card-cascade narrower">
                        <!-- Card image -->
                        <div class="view view-cascade gradient-card-header aqua-gradient">
                            <!-- Title -->
                            <h4 class="card-header-title">FACTURAS {!! date('Y') !!}</h4>
                        </div>
                        <!-- Card content -->
                        <div class="card-body card-body-cascade text-center">
                            <!-- Text -->
                            <canvas id="lineChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <br>

        </div>
        <!--/.Facturas-->

        <!--Estacionamientos-->
        <div class="tab-pane fade" id="est" role="tabpanel">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con mas facturas</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="barChart_est_mas"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card card-cascade narrower">
                            <!-- Card image -->
                            <div class="view view-cascade gradient-card-header aqua-gradient">
                                <!-- Title -->
                                <h4 class="card-header-title">Estacionamientos con menos facturas</h4>
                            </div>
                            <!-- Card content -->
                            <div class="card-body card-body-cascade text-center">
                                <!-- Text -->
                                <canvas id="barChart_est_menos"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <!--/.Estacionamientos-->

    </div>
@endsection

<script>
    @push('scripts')
    //recarga de div
    function loadlink() {
        $('#divid').load('dashboard');
        //console.log('TESTING!!!!');
    }

    setInterval(function () {
        loadlink()
    }, 1000000);

    //Grafica de Tickets pastel dia
    var datos = [{!! $rechazos !!},{!! $facturas !!},{!! $graf !!}];
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Rechazos", "Facturas", "Tickets"],
            datasets: [{
                data: datos,
                backgroundColor: ["#F7464A", "#00C851", "#33b5e5"],
                hoverBackgroundColor: ["#FF5A5E", "#00C851", "#33b5e5"]
            }]
        },
        options: {
            responsive: true
        }
    });

    //Grafica de estacionamientos barras laterales dia
    new Chart(document.getElementById("barChart"), {
        "type": "horizontalBar",
        "data": {
            "labels": [
                @php
                    if(count($est)){
                        foreach($est as $item){
                            echo '"'.$item->nombre.'",';
                        }
                    }
                @endphp
            ],
            "datasets": [{
                "label": "Facturas",
                "data": [
                    @php
                        if($est->count()){
                            foreach($est as $item){
                                echo $item->fact.',';
                            }
                        }
                    @endphp
                ],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });

    //Grafica de Tickets pastel mes
    var datos_mes = [{!! $rechazo_mes !!},{!! $facturas_mes !!},{!! $graf_mes !!}];
    var ctxP = document.getElementById("pieChart_mes").getContext('2d');
    var myPieChart = new Chart(ctxP, {
        type: 'pie',
        data: {
            labels: ["Rechazos", "Facturas", "Tickets"],
            datasets: [{
                data: datos_mes,
                backgroundColor: ["#F7464A", "#00C851", "#33b5e5"],
                hoverBackgroundColor: ["#FF5A5E", "#00C851", "#33b5e5"]
            }]
        },
        options: {
            responsive: true
        }
    });
    //Graficas de tickets barras mes
    new Chart(document.getElementById("barChart_mes"), {
        "type": "horizontalBar",
        "data": {
            "labels": [
                @php
                    if($est_mes->count()){
                        foreach($est_mes as $item){
                            echo '"'.$item->nombre.'",';
                        }
                    }
                @endphp
            ],
            "datasets": [{
                "label": "Facturas",
                "data": [
                    @php
                        if($est_mes->count()){
                            foreach($est_mes as $item){
                                echo $item->fact.',';
                            }
                        }
                    @endphp
                ],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });

    //Grafica de facturas
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: [
                @php
                    if(count($ticket_graf)){
                        foreach($ticket_graf as $item){
                            switch($item->MES){
                                case(1):
                                    echo '"Enero",';
                                    break;
                                case(2):
                                    echo '"Ferbrero",';
                                    break;
                                case(3):
                                    echo '"Marzo",';
                                    break;
                                case(4):
                                    echo '"Abril",';
                                    break;
                                case(5):
                                    echo '"Mayo",';
                                    break;
                                case(6):
                                    echo '"Junio",';
                                    break;
                                case(7):
                                    echo '"Julio",';
                                    break;
                                case(8):
                                    echo '"Agosto",';
                                    break;
                                case(9):
                                    echo '"Septiembre",';
                                    break;
                                case(10):
                                    echo '"Octubre",';
                                    break;
                                case(11):
                                    echo '"Noviembre",';
                                    break;
                                case(12):
                                    echo '"Diciembre",';
                                    break;
                            }
                        }
                    }
                @endphp
            ],
            datasets: [{
                label: "Cantidad de tickets ingresados",
                data: [
                    @php
                        if(count($ticket_graf)){
                foreach($ticket_graf as $item){
                    echo $item->Total_Tickets.',';
                }
            }
                    @endphp
                ],
                backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                    'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
                {
                    label: "Total de facturas timbradas",
                    data: [
                        @php
                            if(count($ticket_graf)){
                    foreach($fact_graf as $item){
                        echo $item->Total_Tickets.',';
                    }
                }
                        @endphp
                    ],
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                }
            ]
        },
        options: {
            responsive: true
        }
    });

    // grafica de estacionamientos mas
    new Chart(document.getElementById("barChart_est_mas"), {
        "type": "horizontalBar",
        "data": {
            "labels": [
                @php
                    if($est_mes->count()){
                        foreach($est_mes as $item){
                            echo '"'.$item->nombre.'",';
                        }
                    }
                @endphp
            ],
            "datasets": [{
                "label": "Facturas",
                "data": [
                    @php
                        if($est_mes->count()){
                            foreach($est_mes as $item){
                                echo $item->fact.',';
                            }
                        }
                    @endphp
                ],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });
    // grafica de estacionamientos menos
    new Chart(document.getElementById("barChart_est_menos"), {
        "type": "horizontalBar",
        "data": {
            "labels": [
                @php
                    if($est_menos->count()){
                        foreach($est_menos as $item){
                            echo '"'.$item->nombre.'",';
                        }
                    }
                @endphp
            ],
            "datasets": [{
                "label": "Facturas",
                "data": [
                    @php
                        if($est_menos->count()){
                            foreach($est_menos as $item){
                                echo $item->fact.',';
                            }
                        }
                    @endphp
                ],
                "fill": false,
                "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                    "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                    "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                ],
                "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                    "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                ],
                "borderWidth": 1
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }]
            }
        }
    });

    //ciclo
    var tabChange = function () {
        var tabs = $(".movimiento > li");
        var active = tabs.filter(".active").removeClass("active");
        var next = active.next("li").length
            ? active.next("li")
            : tabs.filter(":first-child")
        // Bootsrap tab show, para ativar a tab
        next.addClass("active").find("a").tab("show")
    };
    // Tab Cycle function
    var tabCycle = setInterval(tabChange, 3500);
    // Tab click event handler
    $(function () {
        $(".movimiento a").click(function (e) {
            e.preventDefault();
            $(".movimiento .active").removeClass("active")
            // Stop the cycle
            clearInterval(tabCycle);
            // Show the clicked tabs associated tab-pane
            $(this).tab("show");
            $(this).parent().addClass("active");
            // Start the cycle again in a predefined amount of time
            setTimeout(function () {
                tabCycle = setInterval(tabChange, 5000);
            }, 10000);
        });
    });

    @endpush
</script>
