<div>
    <br>
    <h3>Reporte de ingresos (RDI)</h3>
    <br>
    <h2>{{ $nombre_proyecto }}</h2>
    <br>
    <table>
        <thead>
            <tr>
                <th><strong>ESTACIONAMIENTO N°:</strong></th>
                <th><strong>898</strong></th>
            </tr>
        </thead>
    </table>
    <table>
        <thead>
            <tr>
                <th><strong>PREPARADO POR:</strong></th>
                <th><strong>SAUL ARAUJO GUIA</strong></th>
            </tr>
        </thead>
    </table>
    <table>
        <thead>
            <tr>
                <th><strong>VERIFICADO POR:</strong></th>
                <th><strong>ADRIANA OCHOA</strong></th>
            </tr>
        </thead>
    </table>
    <table>
        <thead>
            <tr>
                <th><strong>FECHA INGRESO:</strong></th>
                <th><strong>{{ $fecha_contable }}</strong></th>
            </tr>
        </thead>
    </table>
    <table class="default">
        <thead>
            <tr>
                <th></th>
                <th><strong>IMPORTES CON IVA</strong></th>
                <th><strong>IMPORTES SIN IVA</strong></th>
                <th></th>
            </tr>
            <tr>
                <th scope="col"></th>
                <th>Proyecto</th>
                <th>Corporativo</th>
                <th>Proyecto</th>
                <th>Corporativo</th>
                <th scope="col">Validaciones</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($invoices as $invoice)
                @if ($invoice->concepto == 'Autos')
                    <tr>
                        <th>Autos</th>
                        <td>
                            @if ($invoice->numero_autos)
                                $ {{ $invoice->numero_autos }}
                            @else
                                -
                            @endif
                        </td>
                        <td> - </td>
                        <td>
                            @if ($invoice->numero_autos)
                               $ {{ $invoice->numero_autos }}
                            @else
                                -
                            @endif
                        </td>
                        <td> - </td>
                    </tr>
                @endif
                @if ($invoice->concepto == 'TARIFA HORARIA')
                    <tr>
                        <th>Tarifa Horaria</th>
                        <td>
                            @if ($invoice->monto_iva)
                                $ {{ $invoice->monto_iva }}
                            @else
                                -
                            @endif
                        </td>
                        <td> - </td>
                        <td>
                            @if ($invoice->monto_sin_iva)
                                $ {{ $invoice->monto_sin_iva }}
                            @else
                                -
                            @endif
                        </td>
                        <td> - </td>
                    </tr>
                @endif
            @endforeach

            {{-- <tr>
                <th>Etiquetas</th>
                <td>$0,00</td>
                <td>$100,00</td>
                <td>$100,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Tarjetones</th>
                <td>$0,00</td>
                <td>$50,00</td>
                <td>$0,00</td>
                <td>$80,00</td>
            </tr>

            <tr>
                <th>Pensiones</th>
                <td>$40,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
                <td>$110,00</td>
            </tr>

            <tr>
                <th>Tarifa Horaria</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Reposicion de tarjeta</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Recargos</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Otros Ingresos</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Igualas</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Valet</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Valet Magneticos</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Activaciones</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Car Wash</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Impresiones de Factura</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Ingreso Bruto</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr>

            <tr>
                <th>Faltantes Sobrantes</th>
                <td>$0,00</td>
                <td>$20,00</td>
                <td>$0,00</td>
                <td>$0,00</td>
            </tr> --}}

        </tbody>
        <!--<tfoot>
            <tr>
                <th>Balance</th>
                <td colspan="2">$20,00</td>
                <td colspan="2">$10,00</td>
            </tr>
        </tfoot>-->
    </table>
    <table>
        <thead>
            <tr>
                <th></th>
                <th><strong>DEPOSITO EN ESTACIONAMIENTO</strong></th>
            </tr>
        </thead>
    </table>
    <table class="default">
        <thead>
            <tr>
                <th></th>
                <th><strong>CONCEPTO</strong></th>
                <th><strong>IMPORTE</strong></th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <th>4100.01</th>
                <th>Tarifa Horaria</th>
                <th>$6000</th>
            </tr>
            <tr>
                <th>4100.02</th>
                <th>Valet</th>
                <th>$6000</th>
            </tr>
            <tr>
                <th>4100.03</th>
                <th>Faltantes Sobrantes</th>
                <th>$6000</th>
            </tr>
            <tr>
                <th>4100.04</th>
                <th>Pensiones</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.05</th>
                <th>Etiquetas</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.06</th>
                <th>Tarjetones</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.07</th>
                <th>Igualas</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.08</th>
                <th>Otros Ingresos</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.09</th>
                <th>Recargos</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.10</th>
                <th>Impresión de factura</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.11</th>
                <th>Activaciones</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.12</th>
                <th>Reposición de Tarjeta</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th>4100.13</th>
                <th>Vales Magneticos</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th></th>
                <th>Car Wash</th>
                <th>$ -</th>
            </tr>
            <tr>
                <th></th>
                <th><strong>SUMA DE FICHAS:</strong></th>
                <th>$ 6,468.97</th>
            </tr>
            <tr>
                <th></th>
                <th><strong>INGRESO TOTAL:</strong></th>
                <th>$ 6,468.97</th>
            </tr>
            <tr>
                <th></th>
                <th><strong>DIFERENCIA:</strong></th>
                <th>$ -</th>
            </tr>
        </tbody>
        <!--<tfoot>
            <tr>
                <th>Balance</th>
                <td colspan="2">$20,00</td>
                <td colspan="2">$10,00</td>
            </tr>
        </tfoot>-->
    </table>
    <table class="default">
        <tr>
            <th></th>
            <td colspan="3"><strong>RELACION DE MEMOS DE DESCUENTO</strong></td>
            <td></td>
        </tr>

        <tr>
            <th>#Numero de empleado</th>
            <th colspan="3">Nombre de empleado</th>
            <th></th>
        </tr>

        <tr>
            <td></td>
            <td colspan="3"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3">Total:</td>
            <td>$ - </td>
        </tr>
    </table>
    <table>
        <thead>
            <tr>
                <th></th>
                <th><strong>Nota: Asegurate de imprimir esta hoja, anexar fichas de deposito y adjuntar pars y
                        memos</strong></th>
            </tr>
        </thead>
    </table>
    <table>
        <thead>
            <tr>
                <th></th>
                <th>Notas de estacionamiento</th>
            </tr>
        </thead>
    </table>
</div>
