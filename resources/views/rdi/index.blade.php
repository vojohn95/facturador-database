@extends('layouts.app')
@section('title', 'RDI')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">RDI</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <h5>Generar</h5>
        {{ Form::open(['action' => ['RDIController@store'], 'method' => 'POST']) }}
        <div class="row">
            <div class="col-4">
                <select name="estacionamiento" class="browser-default custom-select" required>
                    <option value="" selected>Seleccione un estacionamiento</option>
                    @foreach ($estacionamientos as $est)
                        <option value="{{ $est->no_est }}">({{ $est->no_est }}) / {{ $est->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="text-right col-4">
                <div class="form-group col-sm-12">
                    {!! Form::date('fecha', null, ['class' => 'form-control', 'id' => 'fecha', 'required']) !!}
                </div>
                @push('scripts')
                    <script type="text/javascript">
                        $('#fecha').datetimepicker({
                            format: 'YYYY-MM-DD',
                            useCurrent: false
                        })
                    </script>
                @endpush
            </div>
            <div class="text-left col-4">
                <button type="submit" class="btn btn-sm btn-default">Consultar</button>
            </div>
        </div>
        {!! Form::close() !!}


        <br>

    </section>
@endsection
