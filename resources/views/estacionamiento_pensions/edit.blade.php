@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Estacionamiento Pension
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($estacionamientoPension, ['route' => ['estacionamientoPensions.update', $estacionamientoPension->id], 'method' => 'patch']) !!}

                        @include('estacionamiento_pensions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection