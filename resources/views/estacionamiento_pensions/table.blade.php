<div class="table-responsive">
    <table class="table" id="estacionamientoPensions-table">
        <thead>
            <tr>
                <th>No Estacionamiento</th>
        <th>Nombre Proyecto</th>
        <th>Direccion</th>
        <th>Calle</th>
        <th>No Ext</th>
        <th>Colonia</th>
        <th>Municipio</th>
        <th>Estado</th>
        <th>Pais</th>
        <th>Cp</th>
        <th>Tipo Estacionamiento</th>
        <th>Id Gerentes</th>
        <th>Estatus</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($estacionamientoPensions as $estacionamientoPension)
            <tr>
                <td>{{ $estacionamientoPension->no_estacionamiento }}</td>
            <td>{{ $estacionamientoPension->nombre_proyecto }}</td>
            <td>{{ $estacionamientoPension->direccion }}</td>
            <td>{{ $estacionamientoPension->calle }}</td>
            <td>{{ $estacionamientoPension->no_ext }}</td>
            <td>{{ $estacionamientoPension->colonia }}</td>
            <td>{{ $estacionamientoPension->municipio }}</td>
            <td>{{ $estacionamientoPension->estado }}</td>
            <td>{{ $estacionamientoPension->pais }}</td>
            <td>{{ $estacionamientoPension->cp }}</td>
            <td>{{ $estacionamientoPension->tipo_estacionamiento }}</td>
            <td>{{ $estacionamientoPension->id_gerentes }}</td>
            <td>{{ $estacionamientoPension->estatus }}</td>
                <td>
                    {!! Form::open(['route' => ['estacionamientoPensions.destroy', $estacionamientoPension->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('estacionamientoPensions.show', [$estacionamientoPension->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('estacionamientoPensions.edit', [$estacionamientoPension->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
