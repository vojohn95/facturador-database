<!-- No Estacionamiento Field -->
<div class="form-group">
    {!! Form::label('no_estacionamiento', 'No Estacionamiento:') !!}
    <p>{{ $estacionamientoPension->no_estacionamiento }}</p>
</div>

<!-- Nombre Proyecto Field -->
<div class="form-group">
    {!! Form::label('nombre_proyecto', 'Nombre Proyecto:') !!}
    <p>{{ $estacionamientoPension->nombre_proyecto }}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{{ $estacionamientoPension->direccion }}</p>
</div>

<!-- Calle Field -->
<div class="form-group">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{{ $estacionamientoPension->calle }}</p>
</div>

<!-- No Ext Field -->
<div class="form-group">
    {!! Form::label('no_ext', 'No Ext:') !!}
    <p>{{ $estacionamientoPension->no_ext }}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{{ $estacionamientoPension->colonia }}</p>
</div>

<!-- Municipio Field -->
<div class="form-group">
    {!! Form::label('municipio', 'Municipio:') !!}
    <p>{{ $estacionamientoPension->municipio }}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{{ $estacionamientoPension->estado }}</p>
</div>

<!-- Pais Field -->
<div class="form-group">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{{ $estacionamientoPension->pais }}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{{ $estacionamientoPension->cp }}</p>
</div>

<!-- Tipo Estacionamiento Field -->
<div class="form-group">
    {!! Form::label('tipo_estacionamiento', 'Tipo Estacionamiento:') !!}
    <p>{{ $estacionamientoPension->tipo_estacionamiento }}</p>
</div>

<!-- Id Gerentes Field -->
<div class="form-group">
    {!! Form::label('id_gerentes', 'Id Gerentes:') !!}
    <p>{{ $estacionamientoPension->id_gerentes }}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{{ $estacionamientoPension->estatus }}</p>
</div>

