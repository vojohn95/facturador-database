@extends('layouts.app')
@section('title', 'Tickets')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Tickets</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-6">
                <a href="{{ url('buscador-tickets') }}" class="btn btn-default btn-lg">
                    Buscador Tickets: <span class="counter badge badge-danger ml-4"></span>
                </a>
            </div>
            {{--  <div class="col-2">
                <a type="button" class="btn btn-info btn-rounded" href="{{ url('buscador-tickets') }}">
                    Buscar ticket
                </a>
            </div>  --}}
            <div class="col-4 text-right">
                <a type="button" class="btn btn-dark btn-rounded" href="{{ route('list_rechazo') }}">
                    Tickets rechazados
                </a>
            </div>
        </div>
        <br>
        {{--  @include('tickets.table')  --}}
        @livewire('buscador-tickets')
    </section>
@endsection
