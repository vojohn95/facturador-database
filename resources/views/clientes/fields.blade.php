<!-- Apellido Paterno Field -->
<div class="form-group col-sm-12">
    {!! Form::label('apellido_paterno', 'Apellido Paterno:') !!}
    {!! Form::text('apellido_paterno', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Apellido Materno Field -->
<div class="form-group col-sm-12">
    {!! Form::label('apellido_materno', 'Apellido Materno:') !!}
    {!! Form::text('apellido_materno', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Nombres Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombres', 'Nombres:') !!}
    {!! Form::text('nombres', null, ['class' => 'form-control','maxlength' => 200,'maxlength' => 200]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 150,'maxlength' => 150]) !!}
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-12">
    {!! Form::label('RFC', 'Rfc:') !!}
    {!! Form::text('RFC', null, ['class' => 'form-control','maxlength' => 14,'maxlength' => 14]) !!}
</div>

<!-- Id Pension Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_pension', 'Pensión:') !!}
    <select class="browser-default custom-select" name="id_pension">
        <option value="" disabled selected>Escoja una opción</option>
        @foreach($pensiones as $pension)
            <option value="{{$pension->id}}">{{$pension->noTarjeta}}</option>
        @endforeach
    </select>
</div>

<!-- Estatus Field -->
<!--<div class="form-group col-sm-12">
    {!! Form::label('estatus', 'Estatus:') !!}
    {!! Form::text('estatus', null, ['class' => 'form-control','maxlength' => 1,'maxlength' => 1]) !!}
</div>-->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clientes.index') }}" class="btn btn-default">Cancelar</a>
</div>
