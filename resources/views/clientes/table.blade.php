<div class="table-responsive">
    <table class="table" id="dtBasicExample">
        <thead>
        <tr>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Nombre</th>
            <th>Email</th>
            <th>Rfc</th>
            <th>Pension</th>
            <!--<th>Estatus</th>-->
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clientes as $cliente)
            <tr>
                <td>{{ $cliente->apellido_paterno }}</td>
                <td>{{ $cliente->apellido_materno }}</td>
                <td>{{ $cliente->nombres }}</td>
                <td>{{ $cliente->email }}</td>
                <td>{{ $cliente->RFC }}</td>
                <td>{{ $cliente->id_pension }}</td>
                <!--<td>{{ $cliente->estatus }}</td>-->
                <td>
                    {!! Form::open(['route' => ['clientes.destroy', $cliente->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{{ route('clientes.show', [$cliente->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{{ route('clientes.edit', [$cliente->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
