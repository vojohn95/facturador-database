@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Empresa Collect
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($empresaCollect, ['route' => ['empresaCollects.update', $empresaCollect->id], 'method' => 'patch']) !!}

                        @include('empresa_collects.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection