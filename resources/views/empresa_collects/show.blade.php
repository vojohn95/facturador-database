@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Empresa Collect
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('empresa_collects.show_fields')
                    <a href="{!! route('empresaCollects.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
