@extends('layouts.app')
@section('title', 'Pruebas')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Facturas Publico en General</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-8">
                @if($bus)
                    <a type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#buscar_factura">
                        Buscar factura
                    </a>
                    <!-- Modal -->
                    <div class="modal fade" id="buscar_factura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Buscar factura</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                {!! Form::open(['route' => 'busqueda', 'method' => 'post']) !!}
                                <div class="modal-body">
                                @include('layouts.errors')
                                <!-- Text -->
                                    <div class="row">
                                        <div class="col-md-9 col-lg-10">
                                            <div class="md-form">
                                                <i class="fas fa-user prefix"></i>
                                                <input type="text" id="rs" name="rs" class="form-control">
                                                <label for="rs">Razon Social</label>
                                            </div>
                                            <div class="md-form">
                                                <i class="fas fa-address-card prefix"></i>
                                                <input type="text" id="rfc" name="rfc" class="form-control">
                                                <label for="rfc">RFC</label>
                                            </div>
                                            <div class="md-form">
                                                <i class="fas fa-hand-holding-usd prefix"></i>
                                                <input type="number" step=".01" id="total" name="total" class="form-control">
                                                <label for="total">Total</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'id' => 'enviar']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                @else
                    <a type="button" class="btn btn-default btn-lg" href="{{ url('factura') }}">
                        Facturas
                    </a>
                @endif

            </div>
            <div class="col-4 text-right">
                <a type="button" class="btn btn-dark btn-rounded" href="{{route('publico-general.create')}}">
                    Generar factura
                </a>
            </div>
        </div>
        <br>
        {{-- @include('facturas.table') --}}
    </section>
@endsection

