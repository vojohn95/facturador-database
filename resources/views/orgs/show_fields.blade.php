<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $org->id !!}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('RFC', 'Rfc:') !!}
    <p>{!! $org->RFC !!}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('Razon_social', 'Razon Social:') !!}
    <p>{!! $org->Razon_social !!}</p>
</div>

<!-- Regimen Fiscal Field -->
<div class="form-group">
    {!! Form::label('Regimen_fiscal', 'Regimen Fiscal:') !!}
    <p>{!! $org->Regimen_fiscal !!}</p>
</div>

<!-- Url Dev Field -->
<div class="form-group">
    {!! Form::label('URL_dev', 'Url Dev:') !!}
    <p>{!! $org->URL_dev !!}</p>
</div>

<!-- Url Prod Field -->
<div class="form-group">
    {!! Form::label('URL_prod', 'Url Prod:') !!}
    <p>{!! $org->URL_prod !!}</p>
</div>

<!-- Token Dev Field -->
<div class="form-group">
    {!! Form::label('token_dev', 'Token Dev:') !!}
    <p>{!! $org->token_dev !!}</p>
</div>

<!-- Token Prod Field -->
<div class="form-group">
    {!! Form::label('token_prod', 'Token Prod:') !!}
    <p>{!! $org->token_prod !!}</p>
</div>

<!-- Prod Field -->
<div class="form-group">
    {!! Form::label('Prod', 'Prod:') !!}
    <p>{!! $org->Prod !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $org->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $org->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $org->updated_at !!}</p>
</div>

