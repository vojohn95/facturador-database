<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>RFC</th>
            <th>Razón Social</th>
            <th>Regimen Fiscal</th>
            <th>Producción</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($orgs as $org)
            <tr>
                <td>{!! $org->RFC !!}</td>
                <td>{!! $org->Razon_social !!}</td>
                <td>{!! $org->Regimen_fiscal !!}</td>
                <td>
                    @if($org->Prod)
                        <a type="button" class="btn-floating btn-sm success-color" href="{{route('cambio',$org->id)}}"><i class="far fa-check-circle"></i></a>
                        @else
                        <a type="button" class="btn-floating btn-sm danger-color" href="{{route('cambio',$org->id)}}"><i class="fas fa-times-circle"></i></a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<br>
<br>
