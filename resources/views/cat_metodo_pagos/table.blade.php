<div class="table-responsive">
    <table class="table" id="catMetodoPagos-table">
        <thead>
        <tr>
            <th>Clave sat</th>
            <th>Descrición</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($catMetodoPagos as $catMetodoPago)
            <tr>
                <td>{{ $catMetodoPago->claveSat }}</td>
                <td>{{ $catMetodoPago->descricion }}</td>
                <td>
                    {!! Form::open(['route' => ['catMetodoPagos.destroy', $catMetodoPago->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{{ route('catMetodoPagos.show', [$catMetodoPago->id]) }}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{{ route('catMetodoPagos.edit', [$catMetodoPago->id]) }}"
                           class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
