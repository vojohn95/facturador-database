<div class="table-responsive">
    <table class="table" id="catBancos-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Banco</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($catBancos as $catBancos)
            <tr>
                <td>{{ $catBancos->id }}</td>
                <td>{{ $catBancos->banco }}</td>
                <td>
                    {!! Form::open(['route' => ['catBancos.destroy', $catBancos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{{ route('catBancos.show', [$catBancos->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{{ route('catBancos.edit', [$catBancos->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
