@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Img Ingreso
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($imgIngreso, ['route' => ['imgIngresos.update', $imgIngreso->id], 'method' => 'patch']) !!}

                        @include('img_ingresos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection