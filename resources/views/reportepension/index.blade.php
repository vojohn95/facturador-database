@extends('layouts.app')
@section('title', 'Reporte Pensiones')
@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Reportes Pensiones</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <h5>Buscar pagos del mes aplicados</h5>
        {{Form::open(['action' => ['ReportePensionController@pagosMesAplicados'], 'method' => 'POST']) }}
        <div class="row">
            <div class="col-4">
                <select name="mes" class="browser-default custom-select" required>
                    <option selected>Seleccione un mes</option>
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                </select>
            </div>
            <div class="text-right col-4">
                <input type="number" class="form-control" name="año" maxlenght="4" placeholder="Ingrese año" required>
            </div>
            <div class="text-left col-4">
                <button type="submit" class="btn btn-sm btn-default">Consultar</button>
            </div>
        </div>
        {!! Form::close() !!}

        <h5>Buscar pensiones activas</h5>
        {{Form::open(['action' => ['ReportePensionController@ReporteActivos'], 'method' => 'POST']) }}
        <div class="row">
            <div class="col-4">
                <select name="mes" class="browser-default custom-select" required>
                    <option selected>Seleccione un mes</option>
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                </select>
            </div>
            <div class="text-right col-4">
                <input type="number" class="form-control" name="año" maxlenght="4" placeholder="Ingrese año" required>
            </div>
            <div class="text-left col-4">
                <button type="submit" class="btn btn-sm btn-default">Consultar</button>
            </div>
        </div>
        {!! Form::close() !!}

        <h5>Buscar pensiones inactivas</h5>
        {{Form::open(['action' => ['ReportePensionController@ReporteInActivos'], 'method' => 'POST']) }}
        <div class="row">
            <div class="col-4">
                <select name="mes" class="browser-default custom-select" required>
                    <option selected>Seleccione un mes</option>
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                </select>
            </div>
            <div class="text-right col-4">
                <input type="number" class="form-control" name="año" maxlenght="4" placeholder="Ingrese año" required>
            </div>
            <div class="text-left col-4">
                <button type="submit" class="btn btn-sm btn-default">Consultar</button>
            </div>
        </div>
        {!! Form::close() !!}

        <br>

    </section>
@endsection
