<div class="container">
    <div>
        <p>Total de xmls en espera: {{ $totalfiles }}</p>
        <form wire:submit.prevent="submit" enctype="multipart/form-data">
            <div>
                <label for="formFileMultiple" class="form-label">Seleccione XML</label>
                <input class="form-control" type="file" id="files" wire:model.defer="files" multiple required />
                @error('files.*')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div wire:loading class="py-2">
                <div class="spinner-grow text-primary" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-secondary" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-success" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-danger" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-info" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-light" role="status">
                    <span class="visually-hidden"></span>
                </div>
                <div class="spinner-grow text-dark" role="status">
                    <span class="visually-hidden"></span>
                </div>
            </div>
            <br>
            <div class="btn-group" role="group" wire:loading.remove class="py-4">
                <button wire:loading.attr="disabled" type="submit" class="btn btn-success">Cargar</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </form>
        <br>
        <div class="btn-group" role="group" wire:loading.remove class="py-4">
            <button class="btn btn-warning" wire:click="generarExcel">Generar excel</button>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn btn-danger" wire:click="reiniciarCarga">Reiniciar carga</button>
        </div>
        <br>
        @php
            $path = Storage::allFiles('excel');
        @endphp
        <table class="table table-hover" align="center">
            <thead>
                <tr>
                    <th scope="col">Reportes</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @for ($i = 0; $i < count($path); $i++)
                        <th scope="row"><button class="btn btn-primary btn-sm"
                                wire:click="bajarfile('{{ substr($path[$i], 6) }}')">{{ substr($path[$i], 6) }}</button>
                        </th>
                        <th>
                            <button type="button" class="btn btn-danger btn-sm"
                                wire:click="borrafile('{{ substr($path[$i], 6) }}')">Borrar</button>
                        </th>
                    @endfor
                </tr>
            </tbody>
        </table>

    </div>
