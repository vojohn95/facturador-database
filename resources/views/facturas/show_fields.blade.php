<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $factura->id !!}</p>
</div>

<!-- Id Ticketaf Field -->
<div class="form-group">
    {!! Form::label('id_ticketAF', 'Id Ticketaf:') !!}
    <p>{!! $factura->id_ticketAF !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $factura->serie !!}</p>
</div>

<!-- Tipo Doc Field -->
<div class="form-group">
    {!! Form::label('tipo_doc', 'Tipo Doc:') !!}
    <p>{!! $factura->tipo_doc !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $factura->folio !!}</p>
</div>

<!-- Fecha Timbrado Field -->
<div class="form-group">
    {!! Form::label('fecha_timbrado', 'Fecha Timbrado:') !!}
    <p>{!! $factura->fecha_timbrado !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $factura->uuid !!}</p>
</div>

<!-- Subtotal Factura Field -->
<div class="form-group">
    {!! Form::label('subtotal_factura', 'Subtotal Factura:') !!}
    <p>{!! $factura->subtotal_factura !!}</p>
</div>

<!-- Iva Factura Field -->
<div class="form-group">
    {!! Form::label('iva_factura', 'Iva Factura:') !!}
    <p>{!! $factura->iva_factura !!}</p>
</div>

<!-- Total Factura Field -->
<div class="form-group">
    {!! Form::label('total_factura', 'Total Factura:') !!}
    <p>{!! $factura->total_factura !!}</p>
</div>

<!-- Xml Field -->
<div class="form-group">
    {!! Form::label('XML', 'Xml:') !!}
    <p>{!! $factura->XML !!}</p>
</div>

<!-- Pdf Field -->
<div class="form-group">
    {!! Form::label('PDF', 'Pdf:') !!}
    <p>{!! $factura->PDF !!}</p>
</div>

<!-- Estatus Field -->
<div class="form-group">
    {!! Form::label('estatus', 'Estatus:') !!}
    <p>{!! $factura->estatus !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $factura->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $factura->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $factura->updated_at !!}</p>
</div>

