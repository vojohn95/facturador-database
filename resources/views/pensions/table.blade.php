<div class="table-responsive">
    <table id="dtBasicExample" class="table" id="pensions-table">
        <thead>
            <tr>
                <th>Monto</th>
                <th>Tipo</th>
                <th>Contrato</th>
                <th>Solicitud contrato</th>
                <th>Comprobante domicilio</th>
                <th>Ine</th>
                <th>Licencia</th>
                <th>Rfc</th>
                <th>Tarjeta circulación</th>
                <th>No. tarjeta</th>
                <th>Estatus</th>
                <th>Fecha Limite</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pensions as $pension)
                <tr>
                    <td>{{ $pension->montoPension }}</td>
                    <td>{{ $pension->tipoPension }}</td>
                    <td>
                        @if ($pension->contrato)
                            <a href="{!! route('contp', [$pension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        @if ($pension->solicitudContrato)
                            <a href="{!! route('scontp', [$pension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        @if ($pension->solicitudContrato)
                            <a href="{!! route('ccontp', [$pension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        @if ($pension->ine)
                            <a href="{!! route('ine', [$pension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        @if ($pension->licencia)
                            <a href="{!! route('licencia', [$pension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>{{ $pension->rfc }}</td>
                    <td>
                        @if ($pension->tarjetaCirculacion)
                            <a href="{!! route('tarjetaC', [$pension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>{{ $pension->noTarjeta }}</td>
                    <td>
                        @php
                            $fechaAntigua = DB::connection('mysql2')
                                ->table('pensionesDet')
                                ->select('created_at')
                                ->where('id', '=', $pension->id)
                                ->get();

                            $dbDate = new DateTime($fechaAntigua[0]->created_at);
                            $hoyDate = new DateTime(date('Y-m-d H:i:s'));
                            $timePassed = $dbDate->diff($hoyDate);
                        @endphp
                        @if ($timePassed->days >= 31)
                            Pensión nueva
                        @else
                            @if ($pension->status == 1)
                                Activo
                            @elseif($pension->status == 0)
                                Inactivo
                            @else
                                Baja temporal
                            @endif
                        @endif
                    </td>
                    <td>{{ $pension->fecha_limite }}</td>
                    <td>
                        {!! Form::open(['route' => ['pensions.destroy', $pension->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('pensions.show', [$pension->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('pensions.edit', [$pension->id]) }}"
                                class='btn btn-default btn-xs'>Editar</a>
                            {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
