<div class="table-responsive">
    <table class="table" id="categoriaTickets-table">
        <thead>
            <tr>
                <th>Nombre Categoria</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($categoriaTickets as $categoriaTicket)
            <tr>
                <td>{{ $categoriaTicket->nombre_categoria }}</td>
                <td>
                    {!! Form::open(['route' => ['categoriaTickets.destroy', $categoriaTicket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('categoriaTickets.show', [$categoriaTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('categoriaTickets.edit', [$categoriaTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
