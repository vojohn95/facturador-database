<div class="table-responsive">
    <table class="table" id="ticketsTickets-table">
        <thead>
            <tr>
                <th>Nombre Ticket</th>
        <th>Descripcion</th>
        <th>Id Proyecto</th>
        <th>Id Tipo</th>
        <th>Id Asignacion</th>
        <th>Id Categoria</th>
        <th>Id Nivel</th>
        <th>Id Estatus</th>
        <th>Id Usuario</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($ticketsTickets as $ticketsTicket)
            <tr>
                <td>{{ $ticketsTicket->nombre_ticket }}</td>
            <td>{{ $ticketsTicket->descripcion }}</td>
            <td>{{ $ticketsTicket->id_proyecto }}</td>
            <td>{{ $ticketsTicket->id_tipo }}</td>
            <td>{{ $ticketsTicket->id_asignacion }}</td>
            <td>{{ $ticketsTicket->id_categoria }}</td>
            <td>{{ $ticketsTicket->id_nivel }}</td>
            <td>{{ $ticketsTicket->id_estatus }}</td>
            <td>{{ $ticketsTicket->id_usuario }}</td>
                <td>
                    {!! Form::open(['route' => ['ticketsTickets.destroy', $ticketsTicket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('ticketsTickets.show', [$ticketsTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('ticketsTickets.edit', [$ticketsTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
