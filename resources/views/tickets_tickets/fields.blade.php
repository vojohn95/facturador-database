<!-- Nombre Ticket Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_ticket', 'Nombre Ticket:') !!}
    {!! Form::text('nombre_ticket', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Proyecto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    {!! Form::number('id_proyecto', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tipo', 'Id Tipo:') !!}
    {!! Form::number('id_tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Asignacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_asignacion', 'Id Asignacion:') !!}
    {!! Form::number('id_asignacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Categoria Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_categoria', 'Id Categoria:') !!}
    {!! Form::number('id_categoria', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Nivel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_nivel', 'Id Nivel:') !!}
    {!! Form::number('id_nivel', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Estatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estatus', 'Id Estatus:') !!}
    {!! Form::number('id_estatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Usuario Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_usuario', 'Id Usuario:') !!}
    {!! Form::number('id_usuario', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ticketsTickets.index') }}" class="btn btn-default">Cancel</a>
</div>
