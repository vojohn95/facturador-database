@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tickets Ticket
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ticketsTicket, ['route' => ['ticketsTickets.update', $ticketsTicket->id], 'method' => 'patch']) !!}

                        @include('tickets_tickets.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection