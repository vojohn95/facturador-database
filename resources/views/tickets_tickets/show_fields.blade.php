<!-- Nombre Ticket Field -->
<div class="form-group">
    {!! Form::label('nombre_ticket', 'Nombre Ticket:') !!}
    <p>{{ $ticketsTicket->nombre_ticket }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $ticketsTicket->descripcion }}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{{ $ticketsTicket->id_proyecto }}</p>
</div>

<!-- Id Tipo Field -->
<div class="form-group">
    {!! Form::label('id_tipo', 'Id Tipo:') !!}
    <p>{{ $ticketsTicket->id_tipo }}</p>
</div>

<!-- Id Asignacion Field -->
<div class="form-group">
    {!! Form::label('id_asignacion', 'Id Asignacion:') !!}
    <p>{{ $ticketsTicket->id_asignacion }}</p>
</div>

<!-- Id Categoria Field -->
<div class="form-group">
    {!! Form::label('id_categoria', 'Id Categoria:') !!}
    <p>{{ $ticketsTicket->id_categoria }}</p>
</div>

<!-- Id Nivel Field -->
<div class="form-group">
    {!! Form::label('id_nivel', 'Id Nivel:') !!}
    <p>{{ $ticketsTicket->id_nivel }}</p>
</div>

<!-- Id Estatus Field -->
<div class="form-group">
    {!! Form::label('id_estatus', 'Id Estatus:') !!}
    <p>{{ $ticketsTicket->id_estatus }}</p>
</div>

<!-- Id Usuario Field -->
<div class="form-group">
    {!! Form::label('id_usuario', 'Id Usuario:') !!}
    <p>{{ $ticketsTicket->id_usuario }}</p>
</div>

