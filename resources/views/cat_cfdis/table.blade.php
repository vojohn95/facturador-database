<div class="table-responsive">
    <table class="table" id="catCfdis-table">
        <thead>
        <tr>
            <th>Clave sat</th>
            <th>Descrición</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($catCfdis as $catCfdi)
            <tr>
                <td>{{ $catCfdi->claveSat }}</td>
                <td>{{ $catCfdi->descricion }}</td>
                <td>
                    {!! Form::open(['route' => ['catCfdis.destroy', $catCfdi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                    <!--<a href="{{ route('catCfdis.show', [$catCfdi->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{{ route('catCfdis.edit', [$catCfdi->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
