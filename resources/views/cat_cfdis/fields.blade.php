<!-- Clavesat Field -->
<div class="form-group col-sm-12">
    {!! Form::label('claveSat', 'Clave sat:') !!}
    {!! Form::text('claveSat', null, ['class' => 'form-control','maxlength' => 10,'maxlength' => 10]) !!}
</div>

<!-- Descricion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('descricion', 'Descripción:') !!}
    {!! Form::text('descricion', null, ['class' => 'form-control','maxlength' => 30,'maxlength' => 30]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('catCfdis.index') }}" class="btn btn-default">Cancelar</a>
</div>
