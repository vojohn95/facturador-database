<div class="table-responsive">
    <table class="table" id="gerentePensions-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Email</th>
        <th>Password</th>
        <th>Remember Token</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($gerentePensions as $gerentePension)
            <tr>
                <td>{{ $gerentePension->name }}</td>
            <td>{{ $gerentePension->email }}</td>
            <td>{{ $gerentePension->password }}</td>
            <td>{{ $gerentePension->remember_token }}</td>
                <td>
                    {!! Form::open(['route' => ['gerentePensions.destroy', $gerentePension->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('gerentePensions.show', [$gerentePension->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('gerentePensions.edit', [$gerentePension->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
