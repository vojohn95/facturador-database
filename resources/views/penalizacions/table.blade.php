<div class="table-responsive">
    <table class="table" id="penalizacions-table">
        <thead>
        <tr>
            <th>Monto</th>
            <th>Concepto</th>
            <th>Estacionamiento</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($penalizacions as $penalizacion)
            <tr>
                <td>{{ $penalizacion->monto }}</td>
                <td>{{ $penalizacion->concepto }}</td>
                <td>{{ $penalizacion->id_estacionamiento }}</td>
                <td>
                    {!! Form::open(['route' => ['penalizacions.destroy', $penalizacion->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                    <!--<a href="{{ route('penalizacions.show', [$penalizacion->id]) }}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>-->
                        <a href="{{ route('penalizacions.edit', [$penalizacion->id]) }}" class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
