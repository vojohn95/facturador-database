<!-- Monto Field -->
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{{ $penalizacion->monto }}</p>
</div>

<!-- Concepto Field -->
<div class="form-group">
    {!! Form::label('concepto', 'Concepto:') !!}
    <p>{{ $penalizacion->concepto }}</p>
</div>

<!-- Id Estacionamiento Field -->
<div class="form-group">
    {!! Form::label('id_estacionamiento', 'Id Estacionamiento:') !!}
    <p>{{ $penalizacion->id_estacionamiento }}</p>
</div>

