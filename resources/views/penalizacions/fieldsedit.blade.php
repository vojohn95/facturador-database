<!-- Monto Field -->
<div class="form-group col-sm-12">
    {!! Form::label('monto', 'Monto:') !!}
    {!! Form::number('monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Concepto Field -->
<div class="form-group col-sm-12">
    {!! Form::label('concepto', 'Concepto:') !!}
    {!! Form::text('concepto', null, ['class' => 'form-control', 'maxlength' => 45, 'maxlength' => 45]) !!}
</div>

<!-- Id Estacionamiento Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_estacionamiento', 'Estacionamiento:') !!}
    <select class="browser-default custom-select" name="id_estacionamiento" id="id_estacionamiento">
        <option selected>Selecciona un estacionamiento</option>
        @foreach($ests as $est)
        <option value="{{ $est->no_est }}"
            {{ $est->no_est == $penalizacion->id_estacionamiento ? 'selected' : '' }}>
            ({{ $est->no_est}})/{{$est->nombre}}</option>
        @endforeach
      </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('penalizacions.index') }}" class="btn btn-default">Cancelar</a>
</div>

<script>
    // Material Select Initialization
    $(document).ready(function() {
        $('.mdb-select').materialSelect();
    });

</script>
