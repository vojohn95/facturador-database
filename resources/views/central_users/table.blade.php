<div>
    <table class="table table-hover" id="dtBasicExample">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Rol</th>
                <th>Acción</th>
            </tr>
        </thead>
    </table>
</div>
<br>
@section('datatable')
    <script>
        $(document).ready(function() {
            $('#dtBasicExample').DataTable({
                "ajax" : {
                    "url" : "{{ route('datatable.central_users') }}",
                },
                "columns": [
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "nombre" },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: false
                    },
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                responsive: true,
                width: '100%',
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
@endsection
