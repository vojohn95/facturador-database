@extends('layouts.app')
@section('title', 'Ingresos')
@section('content')


    <section class="content">

        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Calendario</h3>
            </div>
        </div>
        <hr>

        @include('layouts.errors')
        @include('flash::message')

        <div class="row">

            <div class="col-1"></div>

            <div class="col-md-10">
                <div class="card card-cascade wider">
                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center">
                        <div id="calendar"></div>

                    </div>
                </div>
            </div>
            <div class="col-1"></div>

            <!-- Card Wider -->
        </div>
        <br>
        <br>
    </section>



@endsection


<script>
    @push('scripts')
    $(document).ready(function () {

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today,addEventButton',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: '{!! date('Y-m-d') !!}',
            navLinks: true,
            editable: true,
            eventLimit: true,
            events: [{
                title: 'Simple static event',
                start: '2018-11-16',
                description: 'Super cool event'
            },

            ],
            customButtons: {
                addEventButton: {
                    text: 'Add new event',
                    click: function () {
                        var dateStr = prompt('Enter date in YYYY-MM-DD format');
                        var date = moment(dateStr);

                        if (date.isValid()) {
                            $('#calendar').fullCalendar('renderEvent', {
                                title: 'Dynamic event',
                                start: date,
                                allDay: true
                            });
                        } else {
                            alert('Invalid Date');
                        }

                    }
                }
            },
            dayClick: function (date, jsEvent, view) {
                var date = moment(date);

                if (date.isValid()) {
                    $('#calendar').fullCalendar('renderEvent', {
                        title: 'Dynamic event from date click',
                        start: date,
                        allDay: true
                    });
                } else {
                    alert('Invalid');
                }
            },
        });
    });
    @endpush()
</script>
