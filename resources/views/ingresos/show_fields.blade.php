<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ingreso->id !!}</p>
</div>

<!-- Id Est Field -->
<div class="form-group">
    {!! Form::label('id_est', 'Id Est:') !!}
    <p>{!! $ingreso->id_est !!}</p>
</div>

<!-- Id Empresa Field -->
<div class="form-group">
    {!! Form::label('id_empresa', 'Id Empresa:') !!}
    <p>{!! $ingreso->id_empresa !!}</p>
</div>

<!-- Id Banco Field -->
<div class="form-group">
    {!! Form::label('id_banco', 'Id Banco:') !!}
    <p>{!! $ingreso->id_banco !!}</p>
</div>

<!-- Numero Field -->
<div class="form-group">
    {!! Form::label('numero', 'Numero:') !!}
    <p>{!! $ingreso->numero !!}</p>
</div>

<!-- Venta Field -->
<div class="form-group">
    {!! Form::label('venta', 'Venta:') !!}
    <p>{!! $ingreso->venta !!}</p>
</div>

<!-- Fecha Recoleccion Field -->
<div class="form-group">
    {!! Form::label('fecha_recoleccion', 'Fecha Recoleccion:') !!}
    <p>{!! $ingreso->fecha_recoleccion !!}</p>
</div>

<!-- Importe Field -->
<div class="form-group">
    {!! Form::label('importe', 'Importe:') !!}
    <p>{!! $ingreso->importe !!}</p>
</div>

<!-- Valido Field -->
<div class="form-group">
    {!! Form::label('valido', 'Valido:') !!}
    <p>{!! $ingreso->valido !!}</p>
</div>

<!-- Coment Field -->
<div class="form-group">
    {!! Form::label('coment', 'Coment:') !!}
    <p>{!! $ingreso->coment !!}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{!! $ingreso->tipo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ingreso->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ingreso->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ingreso->deleted_at !!}</p>
</div>

