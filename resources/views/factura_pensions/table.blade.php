<div class="table-responsive">
    <table class="table" id="dtBasicExample">
        <thead>
            <tr>
                <th>Pension</th>
                <th>Serie</th>
                <th>Folio</th>
                <th>Fecha Timbrado</th>
                <th>UUID</th>
                <th>Subtotal Factura</th>
                <th>Iva Factura</th>
                <th>Total Factura</th>
                <th>Xml</th>
                <th>Pdf</th>
                <th style="margin-right: 0px;">Estatus</th>
                <!-- <th>Uso cfdi</th>
                <th>Metodo pago</th> -->
                <th style="margin-left: 100px;">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($facturaPensions as $facturaPension)
                <tr>
                    <td>{{ $facturaPension->id_pen }}</td>
                    <td>
                        @if ($facturaPension->serie)
                            {{ $facturaPension->serie }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->folio)
                            {{ $facturaPension->folio }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->fecha_timbrado)
                            {{ $facturaPension->fecha_timbrado }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->uuid)
                            {{ $facturaPension->uuid }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->subtotal_factura)
                            {{ $facturaPension->subtotal_factura }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->iva_factura)
                            {{ $facturaPension->iva_factura }}
                        @else
                            En espera
                        @endif
                    </td>
                    <td>{{ $facturaPension->total_factura }}</td>
                    <td>
                        @if ($facturaPension->XML)
                            <a href="{!! route('xmlPen', [$facturaPension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->PDF)
                            <a href="{!! route('pdfPen', [$facturaPension->id]) !!}" class='btn-floating btn-sm btn-blue-grey'><i
                                    class="fas fa-download"></i></a>
                        @else
                            Sin archivo
                        @endif
                    </td>
                    <td>
                        @if ($facturaPension->estatus == 1)
                            Timbrada
                        @elseif($facturaPension->estatus == 2)
                            <div class="row">
                                <div class="col-6">
                                    <a type="button" class="btn-floating btn-sm success-color" href="{{route('validopension',$facturaPension->id)}}" id="btn-one"><i
                                            class="far fa-check-circle"></i></a>
                                </div>
                                <div class="col-6">
                                    <a type="button" class="btn-floating btn-sm danger-color" href="{{route('rechazopension',$facturaPension->id)}}" id="btn-one"><i
                                            class="far fa-times-circle"></i></a>
                                </div>
                            </div>
                        @else
                            Rechazada
                        @endif
                    </td>
                    {{-- <td>{{ $facturaPension->id_usoCFDI }}</td>
                    <td>{{ $facturaPension->id_metodoPago }}</td> --}}
                    <td>
                        {!! Form::open(['route' => ['facturaPensions.destroy', $facturaPension->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <!--<a href="{{ route('facturaPensions.show', [$facturaPension->id]) }}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                            <a href="{{ route('facturaPensions.edit', [$facturaPension->id]) }}"
                                class='btn btn-default btn-sm'>Editar</a>
                            {{-- {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} --}}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
