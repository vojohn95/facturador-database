@extends('layouts.app')
@section('title','Marcas')
@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Marcas</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')


    <div class="row">
        <div class="col-6">
            <button type="button" class="btn btn-default btn-lg">
                Marcas encontradas: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-primary btn-rounded" href="{!! route('estacionamientos.index') !!}">
                Estacionamientos
            </a>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-dark btn-rounded" href="{!! route('marcas.create') !!}">
                Agregar Marca
            </a>
        </div>

    </div>
    <br>
    <!-- Empieza tabla -->
    @include('marcas.table')
    <!--Termina tabla-->


    <br><br>
@endsection
