<div class="table-responsive">
    <table class="table" id="nivelesTickets-table">
        <thead>
            <tr>
                <th>Nombre Nivel</th>
        <th>Tiempo Respuesta</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($nivelesTickets as $nivelesTicket)
            <tr>
                <td>{{ $nivelesTicket->nombre_nivel }}</td>
            <td>{{ $nivelesTicket->tiempo_respuesta }}</td>
                <td>
                    {!! Form::open(['route' => ['nivelesTickets.destroy', $nivelesTicket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('nivelesTickets.show', [$nivelesTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('nivelesTickets.edit', [$nivelesTicket->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
