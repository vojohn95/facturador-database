<!-- Nombre Nivel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_nivel', 'Nombre Nivel:') !!}
    {!! Form::text('nombre_nivel', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Tiempo Respuesta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tiempo_respuesta', 'Tiempo Respuesta:') !!}
    {!! Form::text('tiempo_respuesta', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('nivelesTickets.index') }}" class="btn btn-default">Cancel</a>
</div>
