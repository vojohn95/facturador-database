@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Niveles Ticket
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($nivelesTicket, ['route' => ['nivelesTickets.update', $nivelesTicket->id], 'method' => 'patch']) !!}

                        @include('niveles_tickets.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection