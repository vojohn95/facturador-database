<!-- Folio Inicial Field -->
<div class="form-group col-sm-12">
    {!! Form::label('folio_inicial', 'Folio Inicial:') !!}
    {!! Form::number('folio_inicial', null, ['class' => 'form-control']) !!}
</div>

<!-- Folio Final Field -->
<div class="form-group col-sm-12">
    {!! Form::label('folio_final', 'Folio Final:') !!}
    {!! Form::number('folio_final', null, ['class' => 'form-control']) !!}
</div>



<!-- Id Proyecto Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_estacionamiento', 'Proyecto:') !!}
    <select class="browser-default custom-select" name="id_estacionamiento">
        <option value="" disabled selected>Escoja una opción</option>
        @foreach($proyectos as $proyecto)
            <option value="{{$proyecto->id}}">({{$proyecto->id}})/{{$proyecto->nombre_proyecto}}</option>
        @endforeach
    </select>
</div>

<!-- Fecha Asignacion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fecha_asignacion', 'Fecha Asignacion:') !!}
    {!! Form::date('fecha_asignacion', null, ['class' => 'form-control','id'=>'fecha_asignacion']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_asignacion').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Fecha Finalizacion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fecha_finalizacion', 'Fecha Finalizacion:') !!}
    {!! Form::date('fecha_finalizacion', null, ['class' => 'form-control','id'=>'fecha_finalizacion']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_finalizacion').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('asignacionTarjetas.index') }}" class="btn btn-default">Cancelar</a>
</div>
