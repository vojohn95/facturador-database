<!-- Folio Inicial Field -->
<div class="form-group">
    {!! Form::label('folio_inicial', 'Folio Inicial:') !!}
    <p>{{ $asignacionTarjeta->folio_inicial }}</p>
</div>

<!-- Folio Final Field -->
<div class="form-group">
    {!! Form::label('folio_final', 'Folio Final:') !!}
    <p>{{ $asignacionTarjeta->folio_final }}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{{ $asignacionTarjeta->id_proyecto }}</p>
</div>

<!-- Fecha Asignacion Field -->
<div class="form-group">
    {!! Form::label('fecha_asignacion', 'Fecha Asignacion:') !!}
    <p>{{ $asignacionTarjeta->fecha_asignacion }}</p>
</div>

<!-- Fecha Finalizacion Field -->
<div class="form-group">
    {!! Form::label('fecha_finalizacion', 'Fecha Finalizacion:') !!}
    <p>{{ $asignacionTarjeta->fecha_finalizacion }}</p>
</div>

