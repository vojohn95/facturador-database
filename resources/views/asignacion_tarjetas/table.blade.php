<div class="table-responsive">
    <table class="table" id="dtBasicExample">
        <thead>
        <tr>
            <th>Folio Inicial</th>
            <th>Folio Final</th>
            <th>Proyecto</th>
            <th>Fecha Asignación</th>
            <th>Fecha Finalización</th>
            <th>Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($asignacionTarjetas as $asignacionTarjeta)
            <tr>
                <td>{{ $asignacionTarjeta->folio_inicial }}</td>
                <td>{{ $asignacionTarjeta->folio_final }}</td>
                <td>{{ $asignacionTarjeta->id_proyecto }}</td>
                <td>{{ $asignacionTarjeta->fecha_asignacion }}</td>
                <td>{{ $asignacionTarjeta->fecha_finalizacion }}</td>
                <td>
                    {!! Form::open(['route' => ['asignacionTarjetas.destroy', $asignacionTarjeta->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('asignacionTarjetas.edit', [$asignacionTarjeta->id]) }}"
                           class='btn btn-default btn-xs'>Editar</a>
                        {!! Form::button('Eliminar', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
