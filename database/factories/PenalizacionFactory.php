<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Penalizacion;
use Faker\Generator as Faker;

$factory->define(Penalizacion::class, function (Faker $faker) {

    return [
        'monto' => $faker->randomDigitNotNull,
        'concepto' => $faker->word,
        'id_estacionamiento' => $faker->randomDigitNotNull
    ];
});
