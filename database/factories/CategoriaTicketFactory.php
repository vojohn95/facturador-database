<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CategoriaTicket;
use Faker\Generator as Faker;

$factory->define(CategoriaTicket::class, function (Faker $faker) {

    return [
        'nombre_categoria' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
