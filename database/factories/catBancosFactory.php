<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\catBancos;
use Faker\Generator as Faker;

$factory->define(catBancos::class, function (Faker $faker) {

    return [
        'id' => $faker->randomDigitNotNull,
        'banco' => $faker->word
    ];
});
