<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\catMetodoPago;
use Faker\Generator as Faker;

$factory->define(catMetodoPago::class, function (Faker $faker) {

    return [
        'claveSat' => $faker->randomDigitNotNull,
        'descricion' => $faker->word
    ];
});
