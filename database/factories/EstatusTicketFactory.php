<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EstatusTicket;
use Faker\Generator as Faker;

$factory->define(EstatusTicket::class, function (Faker $faker) {

    return [
        'nombre_estatus' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
