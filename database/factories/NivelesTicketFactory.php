<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\NivelesTicket;
use Faker\Generator as Faker;

$factory->define(NivelesTicket::class, function (Faker $faker) {

    return [
        'nombre_nivel' => $faker->word,
        'tiempo_respuesta' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
