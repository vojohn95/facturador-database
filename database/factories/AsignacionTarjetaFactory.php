<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AsignacionTarjeta;
use Faker\Generator as Faker;

$factory->define(AsignacionTarjeta::class, function (Faker $faker) {

    return [
        'folio_inicial' => $faker->word,
        'folio_final' => $faker->word,
        'id_proyecto' => $faker->randomDigitNotNull,
        'fecha_asignacion' => $faker->word,
        'fecha_finalizacion' => $faker->word
    ];
});
