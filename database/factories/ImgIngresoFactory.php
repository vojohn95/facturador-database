<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ImgIngreso;
use Faker\Generator as Faker;

$factory->define(ImgIngreso::class, function (Faker $faker) {

    return [
        'id_ingreso' => $faker->word,
        'foto' => $faker->text,
        'firma' => $faker->text
    ];
});
