<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\GerentePension;
use Faker\Generator as Faker;

$factory->define(GerentePension::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'remember_token' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
