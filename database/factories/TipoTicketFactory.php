<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TipoTicket;
use Faker\Generator as Faker;

$factory->define(TipoTicket::class, function (Faker $faker) {

    return [
        'tipo' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
