<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Ticket;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {

    return [
        'id_cliente' => $faker->word,
        'id_est' => $faker->randomDigitNotNull,
        'id_tipo' => $faker->word,
        'id_CentralUser' => $faker->word,
        'factura' => $faker->randomDigitNotNull,
        'no_ticket' => $faker->word,
        'total_ticket' => $faker->word,
        'fecha_emision' => $faker->word,
        'imagen' => $faker->text,
        'estatus' => $faker->word,
        'UsoCFDI' => $faker->word,
        'metodo_pago' => $faker->word,
        'forma_pago' => $faker->word,
        'RFC' => $faker->word,
        'Razon_social' => $faker->word,
        'email' => $faker->word,
        'calle' => $faker->word,
        'no_ext' => $faker->word,
        'no_int' => $faker->word,
        'colonia' => $faker->word,
        'municipio' => $faker->word,
        'estado' => $faker->word,
        'cp' => $faker->word,
        'coment' => $faker->text,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
