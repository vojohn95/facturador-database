<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CatConcepto;
use Faker\Generator as Faker;

$factory->define(CatConcepto::class, function (Faker $faker) {

    return [
        'DESCRIPCION' => $faker->word,
        'TIPO' => $faker->word,
        'ID_MAT_ARTICULO' => $faker->randomDigitNotNull,
        'CLIENTE_ERP' => $faker->word,
        'FECHA_ALTA' => $faker->date('Y-m-d H:i:s'),
        'FECHA_BAJA' => $faker->date('Y-m-d H:i:s'),
        'ESTATUS' => $faker->word
    ];
});
