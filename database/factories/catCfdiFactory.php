<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\catCfdi;
use Faker\Generator as Faker;

$factory->define(catCfdi::class, function (Faker $faker) {

    return [
        'claveSat' => $faker->word,
        'descricion' => $faker->word
    ];
});
