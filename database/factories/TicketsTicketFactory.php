<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TicketsTicket;
use Faker\Generator as Faker;

$factory->define(TicketsTicket::class, function (Faker $faker) {

    return [
        'nombre_ticket' => $faker->word,
        'descripcion' => $faker->text,
        'id_proyecto' => $faker->randomDigitNotNull,
        'id_tipo' => $faker->randomDigitNotNull,
        'id_asignacion' => $faker->word,
        'id_categoria' => $faker->randomDigitNotNull,
        'id_nivel' => $faker->randomDigitNotNull,
        'id_estatus' => $faker->randomDigitNotNull,
        'id_usuario' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
