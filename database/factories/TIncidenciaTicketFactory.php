<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TIncidenciaTicket;
use Faker\Generator as Faker;

$factory->define(TIncidenciaTicket::class, function (Faker $faker) {

    return [
        'nombre_incidencia' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
